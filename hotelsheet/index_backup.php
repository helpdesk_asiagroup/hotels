<?php

    include '../include/include.php';

    $type   = Request::getInt('type', 1);
    $action = Request::getVar('action', $default = "get_list");
    $year   = Request::getInt('year', $default = 2018);

    switch($type) {
        case 1:
            $tablename = "catalogue_$year";
            $cat_col = 'general';
            $template = 'hotelsheet';
            break;
        case 2:
            $tablename = "luxury_$year";
            $cat_col = 'luxury';
            $template = 'hotelsheet_luxury';
            $css_file = 'hotelsheet_luxury.css';
            break;
        case 3:
            $tablename = "mice_$year";
            $cat_col = 'mice';
            $template = 'hotelsheet_mice';
            $css_file = 'hotelsheet_mice.css';
            break;
    }

    switch ($action){
        case 'get_list':
            $regions_obj = Region::load();
            $regions[0] = 'ALL';
            foreach ($regions_obj as $reg_obj) {
                $regions[$reg_obj->get('id')] = $reg_obj->get('name');
            }

            $db = db::instance();
            $sql = "SELECT h.id, h.name, r.name AS block, gen_cat.block AS block_id, h.region_id, cat.exported AS exported,
                           cat.changed_after_export, cat.changed_page, cat.changed_position, cat.approved,
                           cat.notes, r.tom, cat.finished, cc.luxury, cc.mice,
                           cr.status AS corr_status, cr.text AS corr_text 
                        FROM catalogue_contents cc
                           LEFT JOIN hotels h ON cc.hotel_id = h.id
                           LEFT JOIN $tablename cat ON cat.hotel_id = h.id
                           LEFT JOIN catalogue_$year gen_cat ON gen_cat.hotel_id = h.id
                           LEFT JOIN regions r ON r.id = cat.block
                           LEFT JOIN corrections cr ON (cr.hotel_id = h.id AND cr.year = $year AND cr.type = $type)
                        WHERE cc.$cat_col = 1 AND cc.year = $year
                        ORDER BY r.tom ASC, cat.block ASC, cat.order ASC";

            $db->query($sql);

            $list = array();
            $progress = array();
            $toms = array();

            while ($row = $db->fetch_row()) {
                //Initialize array
                if (!isset($progress['TOTAL'])) {
                    $progress['TOTAL']['all'] = 0;
                    $progress['TOTAL']['finished'] = 0;
                    $progress['TOTAL']['exported'] = 0;
                }

                if (!isset($progress[$row['block']])) {
                    $progress[$row['block']]['all'] = 0;
                    $progress[$row['block']]['finished'] = 0;
                    $progress[$row['block']]['exported'] = 0;
                }

                //Increment all counter for both groups (total and block)
                $progress['TOTAL']['all']++;
                $progress[$row['block']]['all']++;
                $progress[$row['block']]['tom'] = $row['tom'];
                $progress[$row['block']]['block_id'] = $row['block_id'];

                //Increment finished counter for both groups (total and block)
                if ($row['finished']) {
                    $progress['TOTAL']['finished']++;
                    $progress[$row['block']]['finished']++;
                }

                if ($row['exported']) {
                    $progress['TOTAL']['exported']++;
                    $progress[$row['block']]['exported']++;
                }

                $row['notes'] = trim($row['notes']) ? 1:0;
                $list[] = $row;
            }

            foreach ($progress as $block => &$data) {
                $data['percent'] = round($data['finished']/$data['all']*100);
                if ($block == 'TOTAL') {
                    $toms["TOTAL"]['TOTAL'] = $data;
                }
                else if ($data['tom']) {
                    $toms[$data['tom']][$block] = $data;
                }
            }

            $vars = compact('engine', 'list', 'toms', 'regions', 'year', 'type');

            $view = new View();
            $view->setTemplate('get_list', $vars);
            $view->addAppsCss('get_list.css');
            $view->addAppsJs('get_list.js');
            $view->display();
            break;

        case 'edit':
            define('CAT_IMG', '../images/catalogue');
            $airport_map = array(
                1 =>  'BKK', 2 => 'BKK', 8 => 'HKT', 17 => 'HKT', 13 => 'KBV', 18 => 'USM', 19 => 'CNX', 20 => 'CEI'
            );

            if (!$hotel_id = Request::getInt('hotel_id')) {
                exit('Wrong hotel id');
            }

            $regions_obj = Region::load();

            foreach ($regions_obj as $reg_obj) {
                $reg_obj->loadSubregions();
                $regions[$reg_obj->get('id')] = $reg_obj->get('name');
            }

            $hotel = Hotel::load($hotel_id);
            $hotel->loadCatalogueInfo($year);

            if ($type == 2) {
                $hotel->loadLuxuryInfo($year);
            } else if ($type == 3) {
                $hotel->loadMiceInfo($year);
            }

            $cat_info = $hotel->catalogue_info;
            $cat_info_luxury = $hotel->luxury_info;
            $cat_info_mice   = $hotel->mice_info;

            $corrections = Corrections::load($hotel_id, $type, $year);

            $vars = compact('engine', 'hotel', 'cat_info', 'corrections', 'cat_info_luxury', 'cat_info_mice', 'regions', 'subregions', 'regions_obj', 'type' ,'airport_map');

            $view = new View();
            $view->setTemplate($template, $vars);
            $view->addAppsCss('hotelsheet.css');

            if (isset($css_file)) {
                $view->addAppsCss($css_file);
            }

            $view->addGlobalJs('autosize.min.js');
            $view->addAppsJs('hotelsheet.js');
            $view->display();
    }
