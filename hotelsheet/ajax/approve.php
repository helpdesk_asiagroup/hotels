<?php
    include '../../include/include.php';

    $type = Request::getInt('type');
    $year = Request::getInt('year');
    $hotel_id = Request::getInt('id');
    $action   = Request::getVar('action');

    switch ($type){
        case 1:
            $class = "Catalogue$year";
            break;
        case 2:
            $class = "Luxury$year";
            break;
        case 1:
            $class = "Mice$year";
            break;
    }

    $catObj = $class::load($hotel_id, $year);
    $catObj->approved = $action == "approve" ? 1 : 0;
    $catObj->save();

    $json['status'] = 1;
    $json['action'] = $action;
    echo json_encode($json);
