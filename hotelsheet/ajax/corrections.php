<?php
    include '../../include/include.php';

    $hotel_id = Request::getInt('hotel_id');
    $type = Request::getInt('type');
    $year = Request::getInt('year');
    $action = Request::getVar('action');

    if (!$hotel_id || !$type || !$year) {
        die("U R BAD!");
    }

    $corr = Corrections::load($hotel_id, $type, $year);
    $text = trim(Request::getVar('text'));
    $reason = trim(Request::getVar('reason'));

    switch ($action) {
        case 'save':
            if (!$engine->isUserInGroup('reservation')) {
                die('Forbidden');
            }
            $corr->text = $text;
            $corr->save();
            break;

        case 'accept':
            if (!$engine->isUserInGroup('editor')) {
                die('Forbidden');
            }
            $corr->status = 1;
            $corr->save();
            break;

        case 'decline':
            if (!$engine->isUserInGroup('editor')) {
                die('Forbidden');
            }
            $corr->status = 2;
            $corr->reason = $reason;
            $corr->save();
            break;

        case 'topending':
            if (!$engine->isUserInGroup('editor')) {
                die('Forbidden');
            }
            $corr->status = 0;
            $corr->save();
            break;

        case 'checked_ok':
            $corr->status = 3;
            $corr->save();
            break;

        default:
            die("U R BAD");
    }

    $answer = array(
            'status' => 'OK',
            'new_status_int' => $corr->status,
            'new_status_text' => $corr->getStatusText(),
            'new_text' => $corr->text,
            'new_reason' => $corr->reason
        );
    echo json_encode($answer);
    die();
