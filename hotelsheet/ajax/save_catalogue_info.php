<?php
    include '../../include/include.php';

    $engine->requireGroup('editor');

    $db = db::instance();

    $json = array();

    $request = $engine->input;

    $hotel_id  = intval($request['hotel_id']);
    $name      = $request['name'];
    $region_id = $request['block'];
    $year      = intval($request['year']);
    $type      = intval($request['type']);

    if ($hotel_id == 99999) {
        $json['error'] = "Sorry, You can not edit Photo page.";
        echo json_encode($json);
        exit();
    }

    unset($request['hotel_id'], $request['name'], $request['type']);

    $hotel = Hotel::load($hotel_id);

    switch ($type)
    {
        case 1:
            $hotel->loadCatalogueInfo($year);
            $target = $hotel->catalogue_info;
            $save_catalogue_info = true;
            $save_luxury_info = false;
            $save_mice_info = false;
            break;
        case 2:
            $hotel->loadLuxuryInfo($year);
            $target = $hotel->luxury_info;
            $save_catalogue_info = false;
            $save_luxury_info = true;
            $save_mice_info = false;
            break;
        case 3:
            $hotel->loadMiceInfo($year);
            $target = $hotel->mice_info;
            $save_catalogue_info = false;
            $save_luxury_info = false;
            $save_mice_info = true;
            break;
       default:
            die("YRBAD");
            break;
    }

    try {
        $hotel->set('name', $name);
        $hotel->set('region_id', $region_id);
        
        foreach ($request as $prop => $val) {
            $json[$prop] = $val;
            $res = $target->set($prop, $val);
        }

	    if ($target->get('exported')) {
    	    $target->set('changed_after_export',1);
        }

        $hotel->save($save_comp_info = false, $save_catalogue_info, $save_luxury_info, $save_mice_info);

    } catch (Exception $e) {
        $json['error'] = $e->getMessage();
        echo json_encode($json);
        die();
    }

    $json['status'] = 'OK';
    //echo json_encode('test');
    echo json_encode($json);
