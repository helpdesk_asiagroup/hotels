<?php
    include '../../include/include.php';

    $engine->requireGroup('designer');

    $request = $engine->input;

    $year      = intval($request['year']);
    $type      = intval($request['type']);
    $block_id  = intval($request['block_id']);

    switch ($type) {
        case '1':
            $cat_name = "GENERAL";
            $tablename = "catalogue_$year";
            $cc_field = "general";
            break;
        case '2':
            $cat_name = "LUXURY";
            $tablename = "luxury_$year";
            $cc_field = "luxury";
            break;
        case '3':
            $cat_name = "MICE";
            $tablename = "mice_$year";
            $cc_field = "mice";
            break;
 }

    $db = db::instance();

    $sql = "SELECT h.id FROM hotels h
                LEFT JOIN $tablename c ON h.id = c.hotel_id
                LEFT JOIN catalogue_contents cc ON h.id = cc.hotel_id
                WHERE c.block = $block_id AND cc.$cc_field = 1 AND cc.year = $year AND c.approved = 1
            ORDER BY c.order ASC";
    $db->query($sql);

    $output = '';
    while ($row = $db->fetch_row()) {
        $output .= "{$row['id']}\t";
    }

    $regions_obj = Region::load();
    foreach ($regions_obj as $reg_obj) {
        $regions[$reg_obj->get('id')] = $reg_obj->get('name');
    }

    $block_name = $regions[$block_id];
    $filename = $cat_name.'_'.$block_name.date("_d_m_H_i").".txt";

    header('Content-type: text/plain; Charset: UTF-8');
    header('Content-Disposition: attachment; filename="'.$filename.'"');

    echo(trim($output));
