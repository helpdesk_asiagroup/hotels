<?php
    include '../../include/include.php';

    $engine->requireGroup('designer');

    $request = $engine->input;

    $hotel_id  = intval($request['hotel_id']);
    $year      = intval($request['year']);
    $type      = intval($request['type']);

    if ($hotel_id == 99999) {
        $json['error'] = "Sorry, You can not edit Photo page.";
        echo json_encode($json);
        exit();
    }

    unset($request['hotel_id'], $request['name'], $request['type']);

    $hotel = Hotel::load($hotel_id);

    $hotel->loadCatalogueInfo($year);
    $general_data = $hotel->catalogue_info->getExportData('l');

    switch ($type) {
        case 2:
            $hotel->loadLuxuryInfo($year);

            $luxury_data  = $hotel->luxury_info->getExportData();

            unset($general_data['exported']);
            unset($general_data['hotel_id']);
            unset($general_data['area']);
            unset($general_data['basic_info']);
            unset($general_data['table_rooms']);
            unset($general_data['site_services']);
            unset($general_data['room_services']);
            unset($general_data['table_mice']);
            unset($general_data['pool_head']);
            unset($general_data['pool_body']);
            unset($general_data['@icon_vip']);
            $mice_icon = $general_data['@icon_mice'];
            unset($general_data['@icon_mice']);

            unset($general_data['children_services']);
            unset($general_data['table_restaurants']);
            unset($general_data['sports_ents']);
            unset($general_data['beach_head']);
            unset($general_data['beach_body']);
            unset($general_data['cards']);
            unset($general_data['deposit']);
            unset($general_data['comments_sayama']);

            $region = Region::load($hotel->luxury_info->get('block'));
            $output = $general_data + $luxury_data;
            break;

        case 3:
            $hotel->loadMiceInfo($year);
            $mice_data = $hotel->mice_info->getExportData();

            unset($general_data['exported']);
            unset($general_data['table_mice']);
            unset($general_data['@icon_mice']);

            $region = Region::load($hotel->mice_info->get('block'));
            $output = array_merge($general_data, $mice_data);
            break;
    }

    $region_id = $region->get('id');

    $output['name']  = $hotel->get('name');
    $output['block'] = $region->get('name');

    //Dirty Hack to save legacy csv order (in Indesign data fetched by column index, NOT COLUMN NAME)
    $ap_distance = $output['ap_distance'];
    unset($output['ap_distance']);
    $output['ap_distance'] = $ap_distance;

    switch ($type) {
        case 2:
            $output['@icon_mice'] = $mice_icon;
            $hotel->luxury_info->set('exported', 1);
            $hotel->luxury_info->set('changed_after_export', 0);
            $hotel->luxury_info->save();
            break;
        case 3:
            $hotel->mice_info->set('exported', 1);
            $hotel->mice_info->set('changed_after_export', 0);
            $hotel->mice_info->save();
            break;
    }

    $airport_map = array(
        1 =>  'BKK', 8 =>  'HKT', 13 => 'KBV', 18 => 'USM',19 => 'CNX', 20 => 'CEI'
    );

    $output['airport'] = $airport_map[$region_id];

    $filename = "{$region->get('name')}_{$output['name']}_#{$output['hotel_id']}" . date("_d_m_H_i") . ".csv";
    header('Content-type: text/plain; Charset: WINDOWS-1251');
    header('Content-Disposition: attachment; filename="'.$filename.'"');

    echo iconv('UTF-8', 'CP1251//TRANSLIT', implode("\t", array_keys($output)) . "\r\n" .  implode("\t", array_values($output)));
    die();
