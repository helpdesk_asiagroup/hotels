<?php $available_years = array(2015 => 2015, 2016 => 2016, 2017 => 2017, 2018=>2018, 2019=>2019); ?>
<?php $available_types = array(1 => "GENERAL", 2 => "LUXURY", 3 => "MICE");?>
<div class="container"> 

<form class="form-horizontal">
  <div class="form-group">
    <div class="col-sm-3">
    <h3>
        <?= HtmlSnippets::select($available_types, 'type', 'form-control input-lg js-type-select-redirect', $type); ?>
    </h3>
    </div>

    <label class="control-label col-xs-7">
    <h2> catalogue year:</h2></label>
    <div class="col-sm-2">
    <h3>
        <?= HtmlSnippets::select($available_years, 'year', 'form-control input-lg js-year-select-redirect', $year); ?>
    </h3>
    </div>
  </div>
</form>

  <div class="row form-group">
  </div>
 <?php if (!$engine->isUserInGroup('reservation')): ?>
  <div class="row progress-wrapper fade-in-loading"> 
  <?php foreach ($toms as $tom_name => $blocks): 
        $block_width = $tom_name == 'TOTAL' ? "col-xs-12" : 'col-xs-6';
        $bar_style   = $tom_name == 'TOTAL' ? "progress-bar-success" : 'progress-bar-info';
        $bar_width   = $tom_name == 'TOTAL' ? "col-xs-10" : 'col-xs-9';
        $vals_width  = $tom_name == 'TOTAL' ? 'col-xs-2'  : 'col-xs-3'; 
        $class       = $tom_name == 'TOTAL' ? 'total'  : 'tom'; ?>
    <div class="<?=$block_width.' '.$class?>">
      <?php foreach($blocks as $block_name => $data): 
            $percentage = $data['percent'];
            $finished   = $data['finished'];
            $exported   = $data['exported'];
            $total      = $data['all'];
            $group      = $block_name == 'TOTAL' ? '' : "<p>$block_name</p>";
            $block_id   = $block_name == 'TOTAL' ? 0  : $data['block_id'];
      ?>

      <div class="<?=$bar_width?>">
          <div class="progress progress-striped" data-block-id="<?=$block_id?>">
              <?= $group ?> 
              <div class="progress-bar <?=$bar_style?>" 
                    role="progressbar" 
                    aria-valuenow="<?=$percentage;?>"
                    aria-value-min="0"
                    aria-value-max="100"
                    style="width: <?= $percentage; ?>%;"> <?= $percentage . "%" ?> 
              </div>
          </div>
      </div>
    
      <div class="<?=$vals_width?>">
          <?php echo "$finished / $total | ".intval($exported); ?>
      </div>
      <?php endforeach;?>
    </div>
  <?php endforeach; ?>

  </div>
 <?php endif; ?>
  <div class="row control fade-in-loading">
    <div class="col-xs-12">
        <input id="name-filter" placeholder="Enter Hotel name" class="form-control" type="text">
    </div>
        <div class="col-xs-2">
          <?php if (!$engine->isUserInGroup('reservation')): ?>
            <label>
                <input type="checkbox" id="not-finished-filter"> Только НЕ заполненные
            </label>
          <?php endif; ?>
        </div>
        <div class="col-xs-2">
          <?php if (!$engine->isUserInGroup('reservation')): ?>
            <label>
                <input type="checkbox" id="finished-filter"> Только заполненные
            </label>
          <?php endif; ?>
        </div>
        <div class="col-xs-2">
          <?php if (!$engine->isUserInGroup('reservation')): ?>
            <label>
                <input type="checkbox" id="with-notes"> Только с заметками 
            </label>
          <?php endif; ?>
        </div>
        <div class="col-xs-1">
        <?php if ($type != 3): ?>
            <label>
                <input type="checkbox" id="mice-filter"> Mice 
            </label>
        <?php endif; ?>
        </div>
        <div class="col-xs-1">
        <?php if ($type != 2): ?>
            <label>
                <input type="checkbox" id="luxury-filter"> luxury 
            </label>
        <?php endif; ?>
        </div>

        <div class="col-xs-2">
           <?php $volumes = array('0' => 'ALL', '1' => "MAINLAND", '2' => "ISLANDS", '3' => "EXOTIC"); ?>
           <?php if ($type == 1): ?>
           <?= HtmlSnippets::select($volumes, 'volume_id', $class='form-control input-sm', "0"); ?>
           <?php endif; ?>
        </div>

        <div class="col-xs-2">
           <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-sm js-block-select', 0); ?>
           <br>
        </div>
        <?php if(!$engine->isUserInGroup('reservation')): ?>
        <br><br>
        <br><br>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="exported-filter"> Выгруженные
            </label>
        </div>

        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="not-exported-filter"> НЕ выгруженные
            </label>
        </div>

        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="changed-after-export-filter"> Сменилось содержание
            </label>
        </div>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="changed-page-filter"> Сменилась страница
            </label>
        </div>
        <div class="col-xs-2">
            <?php if ($type == 1): ?>
            <label>
                <input type="checkbox" id="changed-position-filter"> Сменилось положение
            </label>
            <?php endif ?>
        </div>
        <div class="col-xs-2">
        </div>
        <?php endif;?>
        <br><br>
        <br><br>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="corr-unchecked">Требует проверки
            </label>
        </div>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="corr-checked">Проверен
            </label>
        </div>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="corr-declined">Требует перепроверки
            </label>
        </div>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="corr-checked-ok">Одобрено
            </label>
        </div>
        <?php if (!$engine->isUserInGroup('reservation')): ?>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="approved">Утвержден
		<span class="js-count"></span>
            </label>
        </div>
        <div class="col-xs-2">
            <label>
                <input type="checkbox" id="not-approved">НЕ утвержден
		<span class="js-count"></span>
            </label>
        </div>
        <?php endif; ?>

  </div>

  <div class="row fade-in-loading">
      <div class="list-group">
      <?php foreach ($list as $hot): ?> 
          <?php $cls = $hot['approved'] ? 'list-group-item-info' : ($hot['finished'] ? 'list-group-item-success' : 'list-group-item-danger') ?>
	  <?php $block_id = $hot['block_id']; ?>
          <a href="index.php?action=edit&hotel_id=<?= $hot['id'] ?>&year=<?=$year?>&type=<?=$type?>" 
             class="list-group-item <?= $cls; ?> col-xs-11"  
             data-hotel-id="<?=$hot['id']?>"
             data-finished="<?=(int) !empty($hot['finished'])?>"
             data-changed-after-export="<?=(int) !empty($hot['changed_after_export'])?>"
             data-changed-page="<?=(int) !empty($hot['changed_page'])?>"
             data-changed-position="<?=(int) !empty($hot['changed_position'])?>"
             data-block-id="<?=$block_id?>"
             data-tom="<?=$hot['tom']?>"
             data-notes="<?=$hot['notes']?>"
             data-luxury="<?=$hot['luxury']?>"
             data-corr-unchecked="<?= (int) $hot['corr_status'] == 0 && !$hot['corr_text'] ? 1 : 0 ?>"
             data-corr-checked="<?= (int) $hot['corr_status'] == 0 && $hot['corr_text'] ? 1 : 0 ?>"
             data-corr-status-declined="<?= (int) $hot['corr_status'] == 2 ? 1 : 0 ?>"
             data-corr-status-checked-ok="<?= (int) $hot['corr_status'] == 3 || $hot['corr_status'] == 1 ? 1: 0?>"
             data-mice="<?=$hot['mice']?>"
             data-exported="<?=(int) !empty($hot['exported'])?>"
             data-approved="<?=$hot['approved']?>"
             data-not-approved="<?= 1 - $hot['approved']?>"
          ><?= $hot['name'] ?>
                <p><?= $hot['block'] ?></p>
                <?php if ($engine->isUserInGroup('editor') || $engine->isUserInGroup('designer')): ?>
                    <p class="hotel-id">ID <?=$hot['id']?> </p>
                <?php endif ?>
                <?php if ($hot['notes']): ?>
                    <p class="notes-icon"><span class="glyphicon glyphicon-pencil"></span></p>    
                <?php endif;?>
             </a> 
             <?php if (!empty($hot['finished']) && $engine->isUserInGroup('editor')): ?>
             <?php $checked = $hot['approved'] ? 'checked="true"' : ''; ?>
             <input type="checkbox" 
                    id="js-approved-checkbox-hotel-id-<?=$hot['id']?>"
                    class="col-xs-1 approved-checkbox js-approved-checkbox"
                    <?=$checked?>
                    data-hotel-id="<?=$hot['id']?>"></input>
             <?php endif; ?>
      <?php endforeach; ?>
      </div>

      <form id="export-block" action="/export_block/export.php" method="post" target="_blank">
        <input type="hidden" name="block_id"></input>
        <input type="hidden" name="type"></input>
        <input type="hidden" name="ids"></input>
        <input type="hidden" name="year"></input>
        <?php if ($engine->isUserInGroup('designer')): ?>
        <a class="btn btn-primary js-export-btn btn btn-primary" style="display: none">Выгрузить Блок</a>
        <br>
        <br>
        <br>
        <br>
        <?php endif;?>
      </form>
  </div>
</div>
