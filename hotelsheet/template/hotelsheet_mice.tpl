<?php if (!$engine->isUserInGroup('editor')): ?>
    <script> 
    $(function() {
       $('input, textarea:not(.js-corrections-data), select').attr('disabled','disabled')
       $('.edit').hide();
    });
    </script>
<?php endif; ?><div class="container mice">
<div class="row well mice" id="control">

    <div class="overlay">
        <div class="msg">
            <span class="text">
                Please wait <br>
                <img src="../images/ajax_loader.gif">
            </span>
            <button type="button" class="btn btn-block btn-primary">OK</button>
        </div>
    </div>
        <div class="col-xs-9">
            <h4>Year <?=$cat_info->get('year')?></h4>
        </div>
        <div class="col-xs-3"> 
            <?php $disabled = $cat_info->get('ques') ? '' : 'disabled' ;?>
            <a class="btn btn-primary" target="_blank" href="<?=$cat_info->get('ques')?>" <?=$disabled?> > Анкета </a>

            <?php $disabled = $engine->isUserInGroup('designer') ? '' : 'disabled';?>
            <a class="btn btn-primary" target="_blank" href="ajax/export.php?hotel_id=<?=$hotel->get('id')?>&year=<?=$cat_info->get('year')?>&type=3" <?=$disabled?> > Выгрузить </a>
        </div>
    <div class="col-xs-9">
        <?php $region_id = $cat_info->get('block');?>
        <input id="year"         type="hidden" value="<?=$cat_info_mice->get('year')?>">
        <input id="type"         type="hidden" value="<?=$type?>">
        <input id="exported"     type="hidden" value="<?=$cat_info_mice->get('exported')?>">
        <input id="hotel-id"     type="hidden" name="hotel_id"      value="<?=$hotel->get('id')?>">
        <input id="region_id"    type="hidden" name="region_id"     value="<?=$region_id ?>">
        <input id="subregion_id" type="hidden" name="subregion_id"  value="<?=$cat_info_mice->get('area')?>">
        <input id="hotel-name"   disabled type="text"   class="form-control" value="<?=$hotel->get('name')?>">
        <input id="rate"         disabled type="text"   class="form-control" value="<?=$cat_info_mice->get('rate')?>">

        <?php $opacity = $cat_info->get('icon_video') == 99 ? '' : 'opacity1' ?>
        <img class="video-icon <?=$opacity?>" src="<?=CAT_IMG.'/16_video.jpg'?>" data-value="<?=$cat_info->get('icon_video')?>">
        <div class="row"></div>
        <div class="col-xs-1 notes">
          <button class="btn btn-danger" type="button"><span class="glyphicon glyphicon-pencil"></span></button>
          <div class="note-wrapper">
            <div class="note-text"><input type="text" class="form-control" value="<?= $cat_info_mice->get('notes')?>"></div>
            <div class="note-connector"></div>
          </div>
        </div>
        <div class="col-xs-3 site">
            <label for="check-out"> Site</label>
            <input  id="site" class="form-control input-sm" type="text" value="<?=$cat_info->get('site')?>" disabled>
        </div>
        <div class="col-xs-4 ceo">
            <label for="check-out"> CEO</label>
            <input  id="ceo"  class="form-control input-sm" type="text" value="<?=$cat_info->get('ceo')?>" disabled>
        </div>
        <div class="col-xs-2 region">
           <label>Block</label>
           <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-sm', $region_id, true, true); ?>
        </div>
        <div class="col-xs-2 subregion">
           <label>Area</label>

           <?php foreach ($regions_obj as $reg_obj): ?>
             <select name="subregion" class="form-control input-sm parent-id-<?=$reg_obj->get('id')?>" disabled>
               <option data-parent-id="<?=$reg_obj->get('id')?>" value="0">N/A</option>

               <?php foreach ($reg_obj->subregions as $sub_id => $subregion): ?>
                 <?php $selected = $cat_info->get('area') == $sub_id ? 'selected="selected"' : ''; ?>
                 <option data-parent-id=<?=$subregion->get('parent_region')?> value="<?=$sub_id?>" <?=$selected?>>
                     <?=$subregion->get('name')?>
                 </option>
               <?php endforeach; ?>
             </select>
           <?php endforeach; ?>
        </div>
        
    </div>
    <div class="col-xs-3">
      <button type="button" class="btn btn-primary save-btn">Сохранить</button>
      <label class="finished">
      <?php $check = $cat_info_mice->get('finished') ? 'checked=checked' : ''; ?>
        <input id="finished" type="checkbox"<?= $check ?> id="finished"> Заполнен
      </label>
      <div class="check-in-out form-group">
        <label for="check-in"> Check in </label>
        <input type="text" id="check-in" class="form-control input-sm" value="<?=$cat_info->get('check_in')?>" disabled>
      </div>
      <div class="check-in-out form-group">
        <label for="check-out"> Check out </label>
        <input type="text" id="check-out" class="form-control input-sm" value="<?=$cat_info->get('check_out')?>" disabled>
      </div>
    </div>
</div>
<?php include 'reservation_comments.tpl'?>
<!-------------------------------------------------------------------------------------------------------------------- PREVIEW-------------------------------------------->
<div class="preview col-xs-6">
    <div class="icon-tab">
      <div class="icon-placeholder" data-name="icon_0"  data-value="1" ><img class="check     opacity1" src="<?=CAT_IMG.'/01_time.jpg'?>"   data-id="check"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_1"  data-value="2" ><img class="location  opacity1" src="<?=CAT_IMG.'/02_road.jpg'?>"   data-id="location" alt=""></div>
      <div class="icon-placeholder" data-name="icon_2"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/06_center.jpg'?>" data-id="center"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_3"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/07_build.jpg'?>"  data-id="build"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_4"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/08_refresh.jpg'?>"data-id="refresh"  alt=""></div>
      <div class="icon-placeholder" data-name="icon_5"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/09_vip.jpg'?>"    data-id="vip"      alt=""></div> 
      <div class="icon-placeholder" data-name="icon_6"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/10_low.jpg'?>"    data-id="cheap"    alt=""></div>
<!--  <div class="icon-placeholder" data-name="icon_7"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/11_mice.jpg'?>"   data-id="mice"     alt=""></div> -->
      <div class="icon-placeholder" data-name="icon_8"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/12_honey.jpg'?>"  data-id="honey"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_9"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/13_family.jpg'?>" data-id="family"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_10" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/14_active.jpg'?>" data-id="active"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_11" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/15_relax.jpg'?>"  data-id="relax"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_party" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/17_party.jpg'?>"  data-id="party"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_ppl"   data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/18_ppl.jpg'?>"    data-id="ppl"    alt=""></div>
    </div>
  <div id="end-line-luxury-base">
  </div>
  <div id="sayama-comments" class="text block mice comments">
  </div>

  <div id="base-info" class="text block mice">
  </div>
      <div class="block-name mice">Информация об отеле</div>

  <div id="rooms" class="table-block block mice">
      <div class="block-name mice">Номерной фонд
      </div>
      <table>
          <thead class="mice">
              <tr>
                  <th>Типы номеров</th>
                  <th>Кол-во</th>
                  <th>м2</th>
                  <th>Взр+дет.</th>
                  <th>Вид</th>
              </tr>
          </thead>
          <tbody class="mice">
          <tr>
          </tr>
      </table>
  </div>

  <div id="room-services" class="text block list mice">
  <div class="block-name mice">Услуги в номере
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="site-services" class="text block list mice">
  <div class="block-name mice">Услуги и сервис в отеле
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="pool" class="text block list mice">
  <div class="block-name mice">Бассейн
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="children-services" class="text block list mice">
  <div class="block-name mice">Услуги для детей
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>
  
  <div id="restaurants" class="table-block block list mice">
  <div class="block-name mice">Рестораны и бары
  </div>
    <table>
          <thead class="mice">
              <tr>
                  <th>Название</th>
                  <th>Кухня</th>
                  <th>Буф</th>
                  <th>Банк</th>
                  <th>Часы работы</th>
              </tr>
          </thead>
          <tbody class="mice">
          <tr>
          </tr>
    </table>
  </div>


  <div id="buiseness-services" class="text block list mice">
  <div class="block-name mice">Бизнес услуги</div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="other-services" class="text block list mice">
  <div class="block-name mice">Другие услуги</div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>


  <div id="sports-ents" class="text block list mice">
  <div class="block-name mice">Спорт и развлечения
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="beach" class="text block list mice">
  <div class="block-name mice">Пляж
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>
  <div id="cards" class="text block mice">
      <div class="block-name mice">Карты к оплате
      </div>
      <div class="content"></div>
  </div>
  <div id="deposit" class="text block mice">
      <div class="block-name mice">Депозит при заселении
      </div>
      <div class="content"></div>
  </div>
</div>
<!--- EDIT -->
<?php include 'reservation_icons_text.tpl'; ?>
<div class="col-xs-6 edit mice">
    <div class="icon-tab">
    <div class="exported-overlay"></div>
<!----------------------------------------------- LOCATION ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="location"  src="<?=CAT_IMG.'/02_road_r.jpg'?>" data-id="location">
       <input type="text" data-name="icon_1_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_1_text')?>">
        <div class="icon-select">
            <img class="option <?= $cat_info->get('icon_1') == 2 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/02_road_r.jpg'?>"     data-input="1" data-id="location" data-icon-id="2" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 3 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/03_promenad_r.jpg'?>" data-input="1" data-id="location" data-icon-id="3" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 4 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/04_beach_r.jpg'?>"    data-input="0" data-id="location" data-icon-id="4" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 5 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/05_town_r.jpg'?>"     data-input="0" data-id="location" data-icon-id="5" data-beach-icon="0">
        </div>
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_2') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/06_center_r.jpg'?>" data-id="center" data-icon-id="6" ></div>

<!----------------------------------------------- BUILD    ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_3') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/07_build_r.jpg'?>"  data-id="build"    data-icon-id="7" >
        <input type="text" data-name="icon_3_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_3_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>


<!----------------------------------------------- REFRSH   ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_4') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/08_refresh_r.jpg'?>"data-id="refresh"  data-icon-id="8" >
        <input type="text" data-name="icon_4_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_4_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>

      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_5') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/09_vip_r.jpg'?>"    data-id="vip"    data-icon-id="9" ></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_6') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/10_low_r.jpg'?>"    data-id="cheap"  data-icon-id="10"></div>

      <div class="icon-placeholder">
        <h1 class="center"><b><?=array_key_exists($region_id, $airport_map) ? $airport_map[$region_id] : ''?></b></h1>
        <input type="text" data-name="ap_distance" class="form-control icon-text-info" value="<?=$cat_info_mice->get('ap_distance')?>" style="opacity: 1">
      </div>

      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_8') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/12_honey_r.jpg'?>"  data-id="honey"  data-icon-id="12"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_9') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/13_family_r.jpg'?>" data-id="family" data-icon-id="13"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_10') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/14_active_r.jpg'?>" data-id="active" data-icon-id="14"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_11') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/15_relax_r.jpg'?>"  data-id="relax"  data-icon-id="15"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info_mice->get('icon_party') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/17_party_r.jpg'?>" data-id="party" data-icon-id="17"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info_mice->get('icon_ppl') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/18_ppl_r.jpg'?>"  data-id="ppl"  data-icon-id="18"></div>
    </div>
    <div id="base-info-text" data-for="#base-info" data-type="text" class="block mice ">
      <div class="block-name ">Информация об отеле 
      </div>
        <?php $base_info = $cat_info_mice->get('base_info') ? $cat_info_mice->get('base_info') : $cat_info->get('base_info'); ?>
        <textarea data-name="base_info"> <?=$base_info?>
        </textarea>
    </div>

    <?php $source = $cat_info_mice->get('rooms') ? $cat_info_mice : $cat_info; ?>
    <div id="rooms-text" data-for="#rooms" data-type="table" class="block">
      <div class="block-name">Номерной фонд
      </div>
        <textarea data-name="rooms"><?=$source->get('rooms') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_mice->get('room_services_left') ? $cat_info_mice : $cat_info; ?>
    <div id="room-services-text" data-for="#room-services" class="block list">
      <div class="block-name">Услуги в номере
      </div>
      <div class="left-part" data-type="list" data-for="#room-services .left-part">
        <textarea data-name="room_services_left"><?=$source->get('room_services_left') ?>
        </textarea>
      </div>
      <div class="right-part" data-type="list" data-for="#room-services .right-part">
        <textarea data-name="room_services_right"><?=$source->get('room_services_right') ?>
        </textarea>
      </div>
    </div>


    <?php $source = $cat_info_mice->get('site_services_left') ? $cat_info_mice : $cat_info; ?>
    <div id="site-services-text" data-for="#site-services" class="block list mice">
      <div class="block-name">Услуги и сервис в отеле<input title="убрать" type="checkbox"></input>
      </div>
        <div class="left-part" data-type="list" data-for="#site-services .left-part">
          <textarea data-name="site_services_left"><?=$source->get('site_services_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#site-services .right-part">
          <textarea data-name="site_services_right"><?=$source->get('site_services_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_mice->get('pool_top') ? $cat_info_mice : $cat_info; ?>
    <div id="pool-text" data-for="#pool" class="block list mice">
      <div class="block-name">Бассейн <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#pool .top-part">
            <textarea data-name="pool_top"><?= $source->get('pool_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#pool .left-part">
          <textarea data-name="pool_left"><?=$source->get('pool_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#pool .right-part">
          <textarea data-name="pool_right"><?=$source->get('pool_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_mice->get('children_services_left') ? $cat_info_mice : $cat_info; ?>
    <div id="children-services-text" data-for="#children-services" class="block list mice">
      <div class="block-name">Услуги для детей <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#children-services .left-part">
          <textarea data-name="children_services_left"><?=$source->get('children_services_left') ?>

          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#children-services .right-part">
          <textarea data-name="children_services_right"><?=$source->get('children_services_right') ?>
          </textarea>
        </div>
    </div>

    <?php $restaurants_table = $cat_info_mice->get('restaurants');
    if (!$restaurants_table) {
        $restaurants_table = array();
        foreach (explode('#', $cat_info->get('restaurants')) as $rest) {
            $restArr = explode(';', $rest);
            $restaurants_table[] = "{$restArr[0]};{$restArr[2]};\t;\t;{$restArr[1]}";
        }
        $restaurants_table = implode('#', $restaurants_table);
    }
    ?>
    <div id="restaurants-text" data-for="#restaurants" data-type="table" class="block mice">
      <div class="block-name"> Рестораны и бары<input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="restaurants"><?=$restaurants_table?>
        </textarea>
    </div>

    <!-- TODO -->
    <div id="buisiness-services-text" data-for="#buiseness-services" class="block list mice">
      <div class="block-name">Бизнес Услуги<input title="убрать" type="checkbox"></input></div>
         <div class="left-part" data-type="list" data-for="#buiseness-services .left-part">
          <textarea data-name="business_services_left"><?=$cat_info_mice->get('business_services_left') ?>

          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#buiseness-services .right-part">
          <textarea data-name="business_services_right"><?=$cat_info_mice->get('business_services_right') ?>
          </textarea>
        </div>
    </div>

    <div id="other-services-text" data-for="#other-services" class="block list mice">
      <div class="block-name">Другие услуги<input title="убрать" type="checkbox"></input></div>
        <div class="left-part" data-type="list" data-for="#other-services .left-part">
          <textarea data-name="other_services_left"><?=$cat_info_mice->get('other_services_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#other-services .right-part">
          <textarea data-name="other_services_right"><?=$cat_info_mice->get('other_services_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_mice->get('sports_ents_left') ? $cat_info_mice : $cat_info; ?>
    <div id="sports-ents-text" data-for="#sports-ents" class="block list mice">
      <div class="block-name"> Спорт и развлечения <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#sports-ents .left-part">
          <textarea data-name="sports_ents_left"><?=$source->get('sports_ents_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#sports-ents .right-part">
          <textarea data-name="sports_ents_right"><?=$source->get('sports_ents_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_mice->get('beach_top') ? $cat_info_mice : $cat_info; ?>
    <div id="beach-text" data-for="#beach" class="block list mice">
      <div class="block-name"> Пляж <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#beach .top-part">
            <textarea data-name="beach_top"><?= $source->get('beach_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#beach .left-part">
          <textarea data-name="beach_left"><?=$source->get('beach_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#beach .right-part">
          <textarea data-name="beach_right"><?=$source->get('beach_right') ?>
          </textarea>
        </div>
    </div>
    
    <?php $source = $cat_info_mice->get('cards') ? $cat_info_mice : $cat_info; ?>
    <div id="cards-text" data-for="#cards .content" data-type="text" class="block mice">
      <div class="block-name">Карты к оплате <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="cards"><?=$source->get('cards') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_mice->get('deposit') ? $cat_info_mice : $cat_info; ?>
    <div id="deposit-text" data-for="#deposit .content" data-type="text" class="block mice">
      <div class="block-name">Депозит при заселении <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="deposit"><?=$source->get('deposit') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_mice->get('comments_sayama') ? $cat_info_mice : $cat_info; ?>
    <div id="sayama-comments-text" data-for="#sayama-comments" data-type="text" class="block mice">
      <div class="block-name">Комментарии Саямы
      </div>
        <textarea data-name="comments_sayama"><?=$source->get('comments_sayama') ?>
        </textarea>
    </div>
    
    <div id="map-text" data-for="#map .content" data-type="text" class="block">
    	<div class="block-name">Файл с картой</div>
    	<textarea data-name="map" class="no_split"><?=$cat_info_mice->get('map') ?></textarea>
    </div>
    
    <div id="map_index-text" data-for="#map_index .content" data-type="text" class="block">
    	<div class="block-name">Индекс квадрата на карте</div>
    	<textarea data-name="map_index"><?=$cat_info_mice->get('map_index') ?></textarea>
    </div>
    
    <div id="image_1-text" data-for="#image_1 .content" data-type="text" class="block">
    	<div class="block-name">Файл 1-го изображения</div>
    	<textarea data-name="image_1"><?=$cat_info_mice->get('image_1') ?></textarea>
    </div>
    
    <div id="image_2-text" data-for="#image_2 .content" data-type="text" class="block">
    	<div class="block-name">Файл 2-го изображения</div>
    	<textarea data-name="image_2" class="no_split"><?=$cat_info_mice->get('image_2') ?></textarea>
    </div>
    
    <div id="image_2_txt-text" data-for="#image_2_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 2-го изображения</div>
    	<textarea data-name="image_2_txt"><?=$cat_info_mice->get('image_2_txt') ?></textarea>
    </div>
    
    <div id="image_3-text" data-for="#image_3 .content" data-type="text" class="block">
    	<div class="block-name">Файл 3-го изображения</div>
    	<textarea data-name="image_3" class="no_split"><?=$cat_info_mice->get('image_3') ?></textarea>
    </div>
    
    <div id="image_3_txt-text" data-for="#image_3_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 3-го изображения</div>
    	<textarea data-name="image_3_txt"><?=$cat_info_mice->get('image_3_txt') ?></textarea>
    </div>
    
    <div id="image_4-text" data-for="#image_4 .content" data-type="text" class="block">
    	<div class="block-name">Файл 4-го изображения</div>
    	<textarea data-name="image_4" class="no_split"><?=$cat_info_mice->get('image_4') ?></textarea>
    </div>
    
    <div id="image_4_txt-text" data-for="#image_4_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 4-го изображения</div>
    	<textarea data-name="image_4_txt"><?=$cat_info_mice->get('image_4_txt') ?></textarea>
    </div>
    
    <div id="image_5-text" data-for="#image_5 .content" data-type="text" class="block">
    	<div class="block-name">Файл 5-го изображения</div>
    	<textarea data-name="image_5" class="no_split"><?=$cat_info_mice->get('image_5') ?></textarea>
    </div>
    
    <div id="image_5_txt-text" data-for="#image_5_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 5-го изображения</div>
    	<textarea data-name="image_5_txt"><?=$cat_info_mice->get('image_5_txt') ?></textarea>
    </div>
    
    <div id="image_6-text" data-for="#image_6 .content" data-type="text" class="block">
    	<div class="block-name">Файл 6-го изображения</div>
    	<textarea data-name="image_6" class="no_split"><?=($cat_info_mice->get('image_6'))?></textarea>
    </div>
    
    <div id="image_6_txt-text" data-for="#image_6_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 6-го изображения</div>
    	<textarea data-name="image_6_txt"><?=$cat_info_mice->get('image_6_txt') ?></textarea>
    </div>

	<div id="plan-text" data-for="#plan .content" data-type="text" class="block">
    	<div class="block-name">Файл плана</div>
    	<textarea data-name="plan"><?=$cat_info_mice->get('plan') ?></textarea>
    </div>
    
    <div id="vc1txt-text" data-for="#vc1txt .content" data-type="text" class="block">
    	<div class="block-name">Неизвестная иконка 1 (vc1txt)</div>
    	<textarea data-name="vc1txt"><?=$cat_info_mice->get('vc1txt') ?></textarea>
    </div>
    
    <div id="vc2txt-text" data-for="#vc2txt .content" data-type="text" class="block">
    	<div class="block-name">Неизвестная иконка 2 (vc2txt)</div>
    	<textarea data-name="vc2txt"><?=$cat_info_mice->get('vc2txt') ?></textarea>
    </div>
    
    <div id="vc3txt-text" data-for="#vc3txt .content" data-type="text" class="block">
    	<div class="block-name">Неизвестная иконка 3  (vc3txt)</div>
    	<textarea data-name="vc3txt"><?=$cat_info_mice->get('vc3txt') ?></textarea>
    </div>
</div>
</div>
<br><br>

<div class="container mice">
<div class="preview col-xs-12">
  <div class="mice-rooms-errors">
  </div>
  <div id="mice-rooms" class="table-block block mice" style="width: 302mm">
      <div class="block-name mice">Конференц залы
      </div>
      <table>
          <thead class="mice">
              <tr style="height: 150px;">
                  <th>Название</th>
                  <th class="vert">Размер</th>
                  <th class="vert">Площадь м2</th>
                  <th class="vert">Высота потолков</th>
                  <th class="vert">THEATRE</th>
                  <th class="vert">CLASSROOM</th>
                  <th class="vert">U-SHAPE</th>
                  <th class="vert">RECEPTION</th>
                  <th class="vert">BANQUET</th>
                  <th class="vert">COCKTAIL</th>
                  <th class="vert">BOARDROOM</th>
                  <th class="vert">CABARET</th>
              </tr>
          </thead>
          <tbody class="mice">
          <tr>
          </tr>
      </table>
  </div>


</div>

<!--- EDIT -->
<div class="col-xs-12 edit mice" style="border:0px">
    <div id="mice-rooms-text" data-for="#mice-rooms" data-type="table" class="block mice" style="width:297mm">
      <div class="block-name"> Конференц залы<input title="убрать" type="checkbox"></input>
      </div>
        <textarea class="mice-rooms-table" data-name="mice_rooms"><?=$cat_info_mice->get('mice_rooms')?>
        </textarea>
    </div>

</div>

</div>

<script>
    var interval_mice_rooms;
    $('textarea.mice-rooms-table').bind('input propertychange', function() {
        var errors = [];
        clearTimeout(interval_mice_rooms);
        interval_mice_rooms = window.setTimeout(function () {
        $('#mice-rooms tbody.mice tr').each(function(){
            var room_name= $(this).find('td').eq(0).text();
            var size = $(this).find('td').eq(1).text();
            var area = $(this).find('td').eq(2).text();

            if (size && area) {

                var dims = size.match(/^\s*(\d+).(\d+)\s*$/);
                if (!dims) {
                    errors.push("Зал: "+room_name+" Неверный формат размера!")
                } else {
                    var correct_area = dims[1] * dims[2];
                    if (correct_area != area) {
                        errors.push("Зал: "+room_name+ " Ошибка в площади! В таблице: "+area+" а надо: "+correct_area)
                    }
                }

                $('.mice-rooms-errors').html(errors.join('<br>'));
            }
        });
        }, 2000);
    });
</script>
