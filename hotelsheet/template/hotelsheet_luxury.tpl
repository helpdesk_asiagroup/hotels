<?php if (!$engine->isUserInGroup('editor')): ?>
    <script> 
    $(function() {
       $('input, textarea:not(.js-corrections-data), select').attr('disabled','disabled')
       $('.edit').hide();
    });
    </script>
<?php endif; ?>
<div class="container luxury">
<div class="row well luxury" id="control">

    <div class="overlay">
        <div class="msg">
            <span class="text">
                Please wait <br>
                <img src="../images/ajax_loader.gif">
            </span>
            <button type="button" class="btn btn-block btn-primary">OK</button>
        </div>
    </div>
        <div class="col-xs-9">
            <h4>Year <?=$cat_info->get('year')?></h4>
        </div>
        <div class="col-xs-3"> 
            <?php $disabled = $cat_info->get('ques') ? '' : 'disabled' ;?>
            <a class="btn btn-primary" target="_blank" href="<?=$cat_info->get('ques')?>" <?=$disabled?> > Анкета </a>

            <?php $disabled = $engine->isUserInGroup('designer') && $cat_info_luxury->finished ? '' : 'disabled';?>
            <a class="btn btn-primary" target="_blank" href="ajax/export.php?hotel_id=<?=$hotel->get('id')?>&year=<?=$cat_info->get('year')?>&type=2" <?=$disabled?> > Выгрузить </a>
        </div>
    <div class="col-xs-9">
        <?php $region_id = $cat_info->get('block') ? $cat_info->get('block') : $hotel->get('region_id'); ?>
        <input id="year"         type="hidden" value="<?=$cat_info->get('year')?>">
        <input id="type"         type="hidden" value="<?=$type?>">
        <input id="exported"     type="hidden" value="<?=$cat_info->get('exported')?>">
        <input id="hotel-id"     type="hidden" name="hotel_id"      value="<?=$hotel->get('id')?>">
        <input id="region_id"    type="hidden" name="region_id"     value="<?=$region_id ?>">
        <input id="subregion_id" type="hidden" name="subregion_id"  value="<?=$cat_info->get('area')?>">
        <input id="hotel-name"   disabled type="text"   class="form-control" value="<?=$hotel->get('name')?>">
        <input id="rate"         disabled type="text"   class="form-control" value="<?=$cat_info->get('rate')?>">

        <?php $opacity = $cat_info->get('icon_video') == 99 ? '' : 'opacity1' ?>
        <img class="video-icon <?=$opacity?>" src="<?=CAT_IMG.'/16_video.jpg'?>" data-value="<?=$cat_info->get('icon_video')?>">
        <div class="row"></div>
        <div class="col-xs-1 notes">
          <button class="btn btn-danger" type="button"><span class="glyphicon glyphicon-pencil"></span></button>
          <div class="note-wrapper">
            <div class="note-text"><input type="text" class="form-control" value="<?= $cat_info_luxury->get('notes')?>"></div>
            <div class="note-connector"></div>
          </div>
        </div>
        <div class="col-xs-3 site">
            <label for="check-out"> Site</label>
            <input  id="site" class="form-control input-sm" type="text" value="<?=$cat_info->get('site')?>" disabled>
        </div>
        <div class="col-xs-4 ceo">
            <label for="check-out"> CEO</label>
            <input  id="ceo"  class="form-control input-sm" type="text" value="<?=$cat_info->get('ceo')?>" disabled>
        </div>
        <div class="col-xs-2 region">
           <label>Block</label>
           <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-sm', $region_id, true, true); ?>
        </div>
        <div class="col-xs-2 subregion">
           <label>Area</label>

           <?php foreach ($regions_obj as $reg_obj): ?>
             <select name="subregion" class="form-control input-sm parent-id-<?=$reg_obj->get('id')?>" disabled>
               <option data-parent-id="<?=$reg_obj->get('id')?>" value="0">N/A</option>

               <?php foreach ($reg_obj->subregions as $sub_id => $subregion): ?>
                 <?php $selected = $cat_info->get('area') == $sub_id ? 'selected="selected"' : ''; ?>
                 <option data-parent-id=<?=$subregion->get('parent_region')?> value="<?=$sub_id?>" <?=$selected?>>
                     <?=$subregion->get('name')?>
                 </option>
               <?php endforeach; ?>
             </select>
           <?php endforeach; ?>
        </div>
        
    </div>
    <div class="col-xs-3">
      <button type="button" class="btn btn-primary save-btn">Сохранить</button>
      <label class="finished">
      <?php $check = $cat_info_luxury->get('finished') ? 'checked=checked' : ''; ?>
        <input id="finished" type="checkbox"<?= $check ?> id="finished"> Заполнен
      </label>
      <div class="check-in-out form-group">
        <label for="check-in"> Check in </label>
        <input type="text" id="check-in" class="form-control input-sm" value="<?=$cat_info->get('check_in')?>" disabled>
      </div>
      <div class="check-in-out form-group">
        <label for="check-out"> Check out </label>
        <input type="text" id="check-out" class="form-control input-sm" value="<?=$cat_info->get('check_out')?>" disabled>
      </div>
    </div>
</div>
<?php include 'reservation_comments.tpl'?>

<!-------------------------------------------------------------------------------------------------------------------- PREVIEW-------------------------------------------->
<div class="preview col-xs-6">
    <div class="icon-tab">
      <div class="icon-placeholder" data-name="icon_0"  data-value="1" ><img class="check     opacity1" src="<?=CAT_IMG.'/01_time.jpg'?>"   data-id="check"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_1"  data-value="2" ><img class="location  opacity1" src="<?=CAT_IMG.'/02_road.jpg'?>"   data-id="location" alt=""></div>
      <div class="icon-placeholder" data-name="icon_2"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/06_center.jpg'?>" data-id="center"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_3"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/07_build.jpg'?>"  data-id="build"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_4"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/08_refresh.jpg'?>"data-id="refresh"  alt=""></div>
<!--  <div class="icon-placeholder" data-name="icon_5"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/09_vip.jpg'?>"    data-id="vip"      alt=""></div> -->
      <div class="icon-placeholder" data-name="icon_6"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/10_low.jpg'?>"    data-id="cheap"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_7"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/11_mice.jpg'?>"   data-id="mice"     alt=""></div>
      <div class="icon-placeholder" data-name="icon_8"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/12_honey.jpg'?>"  data-id="honey"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_9"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/13_family.jpg'?>" data-id="family"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_10" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/14_active.jpg'?>" data-id="active"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_11" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/15_relax.jpg'?>"  data-id="relax"    alt=""></div>
    </div>
  <div id="end-line-luxury-base">
  </div>
  <div id="sayama-comments" class="text block luxury comments">
  </div>

  <div id="base-info" class="text block luxury">
  </div>
      <div class="block-name luxury">Информация об отеле</div>

  <div id="signature-info" class="text block luxury">
      <div class="block-name luxury">Отличительная черта</div>
      <div class="content"></div>
  </div>


  <div id="site-services" class="text block list luxury">
  <div class="block-name luxury">Услуги и сервис в отеле
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="spa-info" class="text block luxury">
      <div class="block-name luxury">Описание спа</div>
      <div class="content"></div>
  </div>

  <div id="buiseness-services" class="text block list luxury">
  <div class="block-name luxury">Бизнес услуги</div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="other-services" class="text block list luxury">
  <div class="block-name luxury">Другие услуги</div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="children-services" class="text block list luxury">
  <div class="block-name luxury">Услуги для детей
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="restaurants" class="table-block block list luxury">
  <div class="block-name luxury">Рестораны и бары
  </div>
    <table>
          <thead>
              <tr>
                  <th>Название</th>
                  <th>Кухня</th>
                  <th>Вместимость</th>
                  <th>Часы работы</th>
              </tr>
          </thead>
          <tbody class="luxury">
          <tr>
          </tr>
    </table>
  </div>

  <div id="restaurants-signs" class="text block luxury">
      <div class="content"></div>
      <div class="block-name luxury">Особенности ресторанов</div>
  </div>

  <div id="pool" class="text block list luxury">
  <div class="block-name luxury">Бассейн
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="sports-ents" class="text block list luxury">
  <div class="block-name luxury">Спорт и развлечения
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="beach" class="text block list luxury">
  <div class="block-name luxury">Пляж
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>
  <div id="cards" class="text block luxury">
      <div class="block-name luxury">Карты к оплате
      </div>
      <div class="content"></div>
  </div>
  <div id="deposit" class="text block luxury">
      <div class="block-name luxury">Депозит при заселении
      </div>
      <div class="content"></div>
  </div>
</div>

<!--- EDIT -->
<?php include 'reservation_icons_text.tpl'; ?>
<div class="col-xs-6 edit luxury">
    <div class="icon-tab">
    <div class="exported-overlay"></div>
<!----------------------------------------------- LOCATION ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="location"  src="<?=CAT_IMG.'/02_road_r.jpg'?>" data-id="location">
       <input type="text" data-name="icon_1_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_1_text')?>">
        <div class="icon-select">
            <img class="option <?= $cat_info->get('icon_1') == 2 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/02_road_r.jpg'?>"     data-input="1" data-id="location" data-icon-id="2" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 3 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/03_promenad_r.jpg'?>" data-input="1" data-id="location" data-icon-id="3" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 4 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/04_beach_r.jpg'?>"    data-input="0" data-id="location" data-icon-id="4" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 5 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/05_town_r.jpg'?>"     data-input="0" data-id="location" data-icon-id="5" data-beach-icon="0">
        </div>
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_2') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/06_center_r.jpg'?>" data-id="center" data-icon-id="6" ></div>

<!----------------------------------------------- BUILD    ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_3') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/07_build_r.jpg'?>"  data-id="build"    data-icon-id="7" >
        <input type="text" data-name="icon_3_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_3_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>


<!----------------------------------------------- REFRSH   ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_4') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/08_refresh_r.jpg'?>"data-id="refresh"  data-icon-id="8" >
        <input type="text" data-name="icon_4_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_4_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>

      <div class="icon-placeholder">
        <h1 class="center"><b><?=$airport_map[$region_id]?></b></h1>
        <input type="text" data-name="ap_distance" class="form-control icon-text-info" value="<?= $cat_info_luxury->get('ap_distance')?>" style="opacity: 1">
      </div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_6') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/10_low_r.jpg'?>"    data-id="cheap"  data-icon-id="10"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_7') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/11_mice_r.jpg'?>"   data-id="mice"   data-icon-id="11"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_8') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/12_honey_r.jpg'?>"  data-id="honey"  data-icon-id="12"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_9') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/13_family_r.jpg'?>" data-id="family" data-icon-id="13"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_10') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/14_active_r.jpg'?>" data-id="active" data-icon-id="14"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_11') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/15_relax_r.jpg'?>"  data-id="relax"  data-icon-id="15"></div>
    </div>
    <div id="base-info-text" data-for="#base-info" data-type="text" class="block luxury ">
      <div class="block-name ">Информация об отеле 
      </div>
        <?php $base_info = $cat_info_luxury->get('base_info') ? $cat_info_luxury->get('base_info') : $cat_info->get('base_info'); ?>
        <textarea data-name="base_info"> <?=$base_info?>
        </textarea>
    </div>

    <div id="signature-text" data-for="#signature-info .content" data-type="text" class="block luxury">
      <div class="block-name">Отличительная черта <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="signature_info"><?=$cat_info_luxury->get('signature_info') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_luxury->get('site_services_left') ? $cat_info_luxury : $cat_info; ?>
    <div id="site-services-text" data-for="#site-services" class="block list luxury">
      <div class="block-name">Услуги и сервис в отеле<input title="убрать" type="checkbox"></input>
      </div>
        <div class="left-part" data-type="list" data-for="#site-services .left-part">
          <textarea data-name="site_services_left"><?=$source->get('site_services_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#site-services .right-part">
          <textarea data-name="site_services_right"><?=$source->get('site_services_right') ?>
          </textarea>
        </div>
    </div>

    <div id="spa-text" data-for="#spa-info .content" data-type="text" class="block luxury">
      <div class="block-name">Описание СПА<input title="убрать" type="checkbox"></input></div>
        <textarea data-name="spa_info"><?=$cat_info_luxury->get('spa_info') ?>
        </textarea>
    </div>

    <!-- TODO -->
    <div id="buisiness-services-text" data-for="#buiseness-services" class="block list luxury">
      <div class="block-name">Бизнес Услуги<input title="убрать" type="checkbox"></input></div>
         <div class="left-part" data-type="list" data-for="#buiseness-services .left-part">
          <textarea data-name="business_services_left"><?=$cat_info_luxury->get('business_services_left') ?>
        
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#buiseness-services .right-part">
          <textarea data-name="business_services_right"><?=$cat_info_luxury->get('business_services_right') ?>
          </textarea>
        </div>
    </div>

    <div id="other-services-text" data-for="#other-services" class="block list luxury">
      <div class="block-name">Другие услуги<input title="убрать" type="checkbox"></input></div>
        <div class="left-part" data-type="list" data-for="#other-services .left-part">
          <textarea data-name="other_services_left"><?=$cat_info_luxury->get('other_services_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#other-services .right-part">
          <textarea data-name="other_services_right"><?=$cat_info_luxury->get('other_services_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_luxury->get('pool_top') ? $cat_info_luxury : $cat_info; ?>
    <div id="pool-text" data-for="#pool" class="block list luxury">
      <div class="block-name">Бассейн <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#pool .top-part">
            <textarea data-name="pool_top"><?= $source->get('pool_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#pool .left-part">
          <textarea data-name="pool_left"><?=$source->get('pool_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#pool .right-part">
          <textarea data-name="pool_right"><?=$source->get('pool_right') ?>
          </textarea>
        </div>
    </div>
    <?php $source = $cat_info_luxury->get('children_services_left') ? $cat_info_luxury : $cat_info; ?>
    <div id="children-services-text" data-for="#children-services" class="block list luxury">
      <div class="block-name">Услуги для детей <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#children-services .left-part">
          <textarea data-name="children_services_left"><?=$source->get('children_services_left') ?>
        
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#children-services .right-part">
          <textarea data-name="children_services_right"><?=$source->get('children_services_right') ?>
          </textarea>
        </div>
    </div>

    <?php $restaurants_table = $cat_info_luxury->get('restaurants');
    if (!$restaurants_table) {
        $restaurants_table = array();
        foreach (explode('#', $cat_info->get('restaurants')) as $rest) {
            $restArr = explode(';', $rest);
            $restaurants_table[] = "{$restArr[0]};{$restArr[2]};\t;{$restArr[1]}";
        }
        $restaurants_table = implode('#', $restaurants_table);
    }
    ?>
    <div id="restaurants-text" data-for="#restaurants" data-type="table" class="block luxury">
      <div class="block-name"> Рестораны и бары<input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="restaurants"><?=$restaurants_table?>
        </textarea>
    </div>

    <div id="restaurants-signs-text" data-for="#restaurants-signs .content" data-type="text" class="block luxury">
      <div class="block-name">Особенности ресторанов<input title="убрать" type="checkbox"></input></div>
        <textarea data-name="restaurants_signs"><?=$cat_info_luxury->get('restaurants_signs') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_luxury->get('sports_ents_left') ? $cat_info_luxury : $cat_info; ?>
    <div id="sports-ents-text" data-for="#sports-ents" class="block list luxury">
      <div class="block-name"> Спорт и развлечения <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#sports-ents .left-part">
          <textarea data-name="sports_ents_left"><?=$source->get('sports_ents_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#sports-ents .right-part">
          <textarea data-name="sports_ents_right"><?=$source->get('sports_ents_right') ?>
          </textarea>
        </div>
    </div>

    <?php $source = $cat_info_luxury->get('beach_top') ? $cat_info_luxury : $cat_info; ?>
    <div id="beach-text" data-for="#beach" class="block list luxury">
      <div class="block-name"> Пляж <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#beach .top-part">
            <textarea data-name="beach_top"><?= $source->get('beach_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#beach .left-part">
          <textarea data-name="beach_left"><?=$source->get('beach_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#beach .right-part">
          <textarea data-name="beach_right"><?=$source->get('beach_right') ?>
          </textarea>
        </div>
    </div>
    
    <?php $source = $cat_info_luxury->get('cards') ? $cat_info_luxury : $cat_info; ?>
    <div id="cards-text" data-for="#cards .content" data-type="text" class="block luxury">
      <div class="block-name">Карты к оплате <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="cards"><?=$source->get('cards') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_luxury->get('deposit') ? $cat_info_luxury : $cat_info; ?>
    <div id="deposit-text" data-for="#deposit .content" data-type="text" class="block luxury">
      <div class="block-name">Депозит при заселении <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="deposit"><?=$source->get('deposit') ?>
        </textarea>
    </div>

    <?php $source = $cat_info_luxury->get('comments_sayama') ? $cat_info_luxury : $cat_info; ?>
    <div id="sayama-comments-text" data-for="#sayama-comments" data-type="text" class="block luxury">
      <div class="block-name">Комментарии Саямы
      </div>
        <textarea data-name="comments_sayama"><?=$source->get('comments_sayama') ?>
        </textarea>
    </div>

</div>
</div>
<br><br>
<div class="container luxury">
    <br>
    <?php if ($engine->isUserInGroup('editor')): ?>
    <div class="col-xs-12">
        <button type="button" class="btn btn-info js-add-room"><h4>Добавить номер</h4></button>
    </div>
    <?php endif;?>
    <br>
    <div style="display: none;" id="rooms-table"><?=trim($cat_info_luxury->get('rooms_table'))?></div>
    <div id="maintable-wrap col-xs-12">
        <div id="maintable"> </div>
    </div>
    <div class="col-xs-12">
        <br>
        <textarea data-name="room_table_last_row_1"><?=$source->get('room_table_last_row_1')?></textarea>
        <br>
        <textarea data-name="room_table_last_row_2"><?=$source->get('room_table_last_row_2')?></textarea>
    </div>
    <br>
</div>
<br>
<script src="/libs/handsontable/dist/handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="/libs/handsontable/dist/handsontable.full.css">


<script>

    var data = [
        ['']
        ,["Количество"],["Площадь, м2"],["Взрослых + детей"],["Вид"],
        ["Личный бассейн (размер, м)"],["Внешняя зона отдыха"],["Кровати DBL"],["Кровати TWN"],
        ["Доп. спальное место: кровать"],["Гостиная"],["Столовая"],["Кухня"],
        ["Кофе-машина"],["Печь СВЧ"],["Раздельные спальни"],["Дверь между спальней и гостиной"],
        ["Сообщающиеся номера"],["Количество ванных комнат"],["IDD-телефон\n(международный прямой набор)"],["Бесплатная местная газета"],
        ["Батлер"],["Балкон"],["Русские каналы"],["Стереосистема"],
        ["Док-станция для iPod"],["Утюг, гладильная доска"],["Холодильник"],["Винный холодильник"],
        ["Джакузи"],["Зеркало для макияжа"],["Свежие фрукты ежедневно"],["Свежие фрукты по прибы тии"],
        ["Разрешение на пребывание\nс домашними животными"]
    ];

    if (json_table = $('#rooms-table').html()) {
        data = JSON.parse(json_table);
    }

    var container = document.getElementById('maintable');
    var luxury_rooms_table = new Handsontable(container, {
        data: data,
        colHeaders: true,
        manualColumnResize: true,
        allowInsertRow: false,
        afterChange: function(change, source) {
            if (change == null) {
                return;
            }
            for (var i in change) {
                if (change[i][0] == 0) {
                    col = change[i][1];
                    $('.js-room-descr-title-'+col).html(change[i][3]).trigger('input');
                    $('.js-room-descr-title-'+col+'-textarea').val(change[i][3]);
                }
            }
        }
    });

    luxury_rooms_table.updateSettings({
        cells: function(row, col, prop) {
            if (col == 0) {
                return {'readOnly': true};
            }
        }
    });

    $('.js-add-room').click(function() {
        luxury_rooms_table.alter("insert_col",100);
    });

    var ta = $('textarea');
    ta.trigger('autosize.resize');
</script>

<div class="container luxury">
<div class="preview col-xs-6">
  <div id="end-line-luxury-descr">
  </div>

<?php for($i = 1; $i < 15; ++$i): ?>
  <div id="room_descr_<?=$i?>" class="text block luxury">
      <div class="block-name luxury">
      </div>
      <div class="content"></div>
  </div>
<?php endfor;?>
  <div id="awards" class="text block luxury">
      <div class="block-name luxury"> Награды отеля
      </div>
      <div class="content"></div>
  </div>


</div>

<!--- EDIT -->

<div class="col-xs-6 edit luxury">
<?php for($i = 1; $i < 15; ++$i): ?>
    <div id="" data-for="#room_descr_<?=$i?> .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text" data-for="#room_descr_<?=$i?> .block-name">
            <div class="js-room-edit js-room-descr-title-<?=$i?> col-xs-9" type="text"><?=$cat_info_luxury->get("room_descr_title_$i")?></div>
            <textarea class="js-room-descr-title-<?=$i?>-textarea" style="display:none" data-name="room_descr_title_<?=$i?>"><?=$cat_info_luxury->get("room_descr_title_$i")?></textarea>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="room_descr_<?=$i?>"><?=$cat_info_luxury->get("room_descr_$i") ?>
        </textarea>
    </div>
<?php endfor;?>
    <div id="" data-for="#awards .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-9" type="text">Награды отеля</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="awards"><?=$cat_info_luxury->get("awards") ?>
        </textarea>
    </div>
    <div id="" data-for="#map .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-10" type="text">Файл с картой</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="map" class="no_split"><?=$cat_info_luxury->get("map") ?>
        </textarea>
    </div>
    <div id="" data-for="#map_index .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Индекс карты</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="map_index"><?=$cat_info_luxury->get("map_index") ?>
        </textarea>
    </div>
    <div id="" data-for="#h_logo .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с лого</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="h_logo" class="no_split"><?=$cat_info_luxury->get("h_logo") ?>
        </textarea>
    </div>
    <div id="" data-for="#qr .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с QR</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="qr" class="no_split"><?=$cat_info_luxury->get("qr") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_1 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 1</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_1" class="no_split"><?=$cat_info_luxury->get("img_1") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_2 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 2</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_2" class="no_split"><?=$cat_info_luxury->get("img_2") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_2_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 2</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_2_txt"><?=$cat_info_luxury->get("img_2_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_3 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 3</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_3" class="no_split"><?=$cat_info_luxury->get("img_3") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_3_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 3</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_3_txt"><?=$cat_info_luxury->get("img_3_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_4 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 4</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_4" class="no_split"><?=$cat_info_luxury->get("img_4") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_4_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 4</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_4_txt"><?=$cat_info_luxury->get("img_4_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_5 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 5</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_5" class="no_split"><?=$cat_info_luxury->get("img_5") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_5_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 5</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_5_txt"><?=$cat_info_luxury->get("img_5_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_6 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 6</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_6" class="no_split"><?=$cat_info_luxury->get("img_6") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_6_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 6</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_6_txt"><?=$cat_info_luxury->get("img_6_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_7 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 7</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_7" class="no_split"><?=$cat_info_luxury->get("img_7") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_7_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 7</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_7_txt"><?=$cat_info_luxury->get("img_7_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_8 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 8</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_8" class="no_split"><?=$cat_info_luxury->get("img_8") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_8_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 8</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_8_txt"><?=$cat_info_luxury->get("img_8_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_9 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 9</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_9" class="no_split"><?=$cat_info_luxury->get("img_9") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_9_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 9</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_9_txt"><?=$cat_info_luxury->get("img_9_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_10 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 10</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_10" class="no_split"><?=$cat_info_luxury->get("img_10") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_10_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 10</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_10_txt"><?=$cat_info_luxury->get("img_10_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_11 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 11</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_11" class="no_split"><?=$cat_info_luxury->get("img_11") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_11_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 11</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_11_txt"><?=$cat_info_luxury->get("img_11_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_12 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 12</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_12" class="no_split"><?=$cat_info_luxury->get("img_12") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_12_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 12</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_12_txt"><?=$cat_info_luxury->get("img_12_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_13 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 13</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_13" class="no_split"><?=$cat_info_luxury->get("img_13") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_13_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 13</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_13_txt"><?=$cat_info_luxury->get("img_13_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_14 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 14</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_14" class="no_split"><?=$cat_info_luxury->get("img_14") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_14_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 14</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_14_txt"><?=$cat_info_luxury->get("img_14_txt") ?>
        </textarea>
    </div>
    
    <div id="" data-for="#img_15 .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Файл с изображением 15</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_15" class="no_split"><?=$cat_info_luxury->get("img_15") ?>
        </textarea>
    </div>
    <div id="" data-for="#img_15_txt .content" data-type="text" class="block luxury">
      <div class="block-name js-room-descr-field">
        <div data-type="text">
            <div class="col-xs-11" type="text">Описание изображения 15</div>
            <input title="убрать" type="checkbox"></input>
        </div>
      </div>
        <textarea data-name="img_15_txt"><?=$cat_info_luxury->get("img_15_txt") ?>
        </textarea>
    </div>
    
</div>

</div>
