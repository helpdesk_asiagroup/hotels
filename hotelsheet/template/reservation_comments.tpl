<?php if($cat_info->get('finished')): ?>
<div class="row well" id="control">
  <div class="row">
    <div class="col-xs-9"> 
        <h4>
        Правки резервации <button class="js-corrections-show btn btn-primary" type="button"> Показать </button> | 
        Статус: <span class="status"> &nbsp <?=$corrections->getStatusText()?> </span> 
        <?php if ($corrections->status == 2) : ?>
            <button class="js-corrections-open-reason js-why btn btn-primary">Почему?</button>
        <?php endif ?>
        </h4>
    </div>
    <div class="col-xs-3"> </div>
  </div>
  <div class="row js-corrections-textarea" style="display: none;">
    <?php $disabled_corrections = $engine->isUserInGroup('reservation') ? '' : 'disabled'; ?>
    <?php $disabled_corrections = $corrections->status > 0 ? 'disabled' : $disabled_corrections; ?>
    <div class="col-xs-12"><textarea class="js-corrections-data" <?=$disabled_corrections?>>
        <?=$corrections->text?>
    </textarea></div>
  </div>
  <div class="row js-corrections-target"
       data-hotel_id="<?=$corrections->hotel_id?>"
       data-type="<?=$corrections->type?>"
       data-year="<?=$corrections->year?>">
    <div class="col-xs-12 js-corrections-buttons" style="display:none">
      <?php if ($engine->isUserInGroup('reservation') && $corrections->status == 0): ?>
        <button class="js-corrections-actions btn btn-primary" data-action="ajax/corrections.php?action=save">Сохранить правки</button>
        <button class="js-corrections-actions btn btn-success" data-action="ajax/corrections.php?action=checked_ok">Проверено, правок не требует</button>
      <?php endif;?>
      <?php if ($engine->isUserInGroup('editor')): ?>
        <?php if ($corrections->status == 0): ?>
            <button class="js-corrections-actions btn btn-success" data-action="ajax/corrections.php?action=accept">Принять правки</button>
            <button class="js-corrections-open-reason btn btn-danger js-decline-btn">Отклонить правки</button>
        <?php else: ?>
            <button class="js-corrections-actions btn btn-primary" data-action="ajax/corrections.php?action=topending">В ожидание</button>
        <?php endif; ?>
      <?php endif;?>
    </div>
    <div class="js-corrections-reason col-xs-12" style="display:none">
       <h5>Причина отклонения:</h5>
        <?php $disabled_reason = $engine->isUserInGroup('editor') ? '' : 'disabled'; ?>
        <?php $disabled_reason = $corrections->status != 0 ? 'disabled' : $disabled_reason; ?>
        <div class="col-xs-12"><textarea class="js-corrections-reason-data" <?=$disabled_reason?>>
           <?=$corrections->reason?>
        </textarea>
        </div>
        <?php if ($engine->isUserInGroup('editor') && $corrections->status == 0): ?>
        <button class="js-corrections-actions btn btn-primary" data-action="ajax/corrections.php?action=decline">Сохранить</button>
        <?php endif; ?>
    </div>
  </div>
</div>
<?php endif;?>
