<?php if (!$engine->isUserInGroup('editor')): ?>
    <script> 
    $(function() {
       $('input, textarea:not(.js-corrections-data), select').attr('disabled','disabled')
       $('.edit').hide();
    });
    </script>
<?php endif; ?>
<div class="container">
<div class="row well" id="control">

    <div class="overlay">
        <div class="msg">
            <span class="loadbar">
                Please wait <br>
                <img src="../images/ajax_loader.gif">
            </span>
            <span class="text"></span>
            <button type="button" class="btn btn-block btn-primary">OK</button>
        </div>
    </div>
        <div class="col-xs-9">
            <h4>Year <?=$cat_info->get('year')?></h4>
        </div>
        <div class="col-xs-3"> 
            <?php $disabled = $cat_info->get('ques') ? '' : 'disabled' ;?>
            <a class="btn btn-primary" target="_blank" href="<?=$cat_info->get('ques')?>" <?=$disabled?> > Анкета </a>

            <?php $disabled = $engine->isUserInGroup('designer') ? '' : 'disabled';?>
            <a class="btn btn-primary" target="_blank" href="#" <?=$disabled?> > Выгрузить </a>

        </div>
    <div class="col-xs-9">
        <?php $region_id = $cat_info->get('block') ? $cat_info->get('block') : $hotel->get('region_id'); ?>
        <input id="year"         type="hidden" value="<?=$cat_info->get('year')?>">
        <input id="type"         type="hidden" value="<?=$type?>">
        <input id="exported"     type="hidden" value="<?=$cat_info->get('exported')?>">
        <input id="hotel-id"     type="hidden" name="hotel_id"      value="<?=$hotel->get('id')?>">
        <input id="region_id"    type="hidden" name="region_id"     value="<?=$region_id ?>">
        <input id="subregion_id" type="hidden" name="subregion_id"  value="<?=$cat_info->get('area')?>">
        <input id="hotel-name"   type="text"   class="form-control" value="<?=$hotel->get('name')?>">
        <input id="rate"         type="text"   class="form-control" value="<?=$cat_info->get('rate')?>">

        <?php $opacity = $cat_info->get('icon_video') == 99 ? '' : 'opacity1' ?>
        <img class="video-icon <?=$opacity?>" src="<?=CAT_IMG.'/16_video.jpg'?>" data-value="<?=$cat_info->get('icon_video')?>">
        <div class="row"></div>
        <div class="col-xs-1 notes">
          <button class="btn btn-danger" type="button"><span class="glyphicon glyphicon-pencil"></span></button>
          <div class="note-wrapper">
            <div class="note-text"><input type="text" class="form-control" value="<?= $cat_info->get('notes')?>"></div>
            <div class="note-connector"></div>
          </div>
        </div>
        <div class="col-xs-3 site">
            <label for="check-out"> Site</label>
            <input  id="site" class="form-control input-sm" type="text" value="<?=$cat_info->get('site')?>">
        </div>
        <div class="col-xs-4 ceo">
            <label for="check-out"> CEO</label>
            <input  id="ceo"  class="form-control input-sm" type="text" value="<?=$cat_info->get('ceo')?>">
        </div>
        <div class="col-xs-2 region">
           <label>Block</label>
           <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-sm', $region_id); ?>
        </div>
        <div class="col-xs-2 subregion">
           <label>Area</label>

           <?php foreach ($regions_obj as $reg_obj): ?>
             <select name="subregion" class="form-control input-sm parent-id-<?=$reg_obj->get('id')?>">
               <option data-parent-id="<?=$reg_obj->get('id')?>" value="0">N/A</option>

               <?php foreach ($reg_obj->subregions as $sub_id => $subregion): ?>
                 <?php $selected = $cat_info->get('area') == $sub_id ? 'selected="selected"' : ''; ?>
                 <option data-parent-id=<?=$subregion->get('parent_region')?> value="<?=$sub_id?>" <?=$selected?>>
                     <?=$subregion->get('name')?>
                 </option>
               <?php endforeach; ?>
             </select>
           <?php endforeach; ?>
        </div>
        
    </div>
    <div class="col-xs-3">
      <?php $disable_save = $engine->isUserInGroup('editor') ? '' : $disabled; ?>
      <button type="button" <?=$disable_save?> class="btn btn-primary save-btn">Сохранить</button>
      <label class="finished">
      <?php $check = $cat_info->get('finished') ? 'checked=checked' : ''; ?>
        <input id="finished" type="checkbox"<?= $check ?> id="finished"> Заполнен
      </label>
      <div class="check-in-out form-group">
        <label for="check-in"> Check in </label>
        <input type="text" id="check-in" class="form-control input-sm" value="<?=$cat_info->get('check_in')?>">
      </div>
      <div class="check-in-out form-group">
        <label for="check-out"> Check out </label>
        <input type="text" id="check-out" class="form-control input-sm" value="<?=$cat_info->get('check_out')?>">
      </div>
    </div>
</div>
<?php include 'reservation_comments.tpl'?>

<!-------------------------------------------------------------------------------------------------------------------- PREVIEW-------------------------------------------->
<div class="preview col-xs-6">
    <div class="icon-tab">
      <div class="icon-placeholder" data-name="icon_0"  data-value="1" ><img class="check     opacity1" src="<?=CAT_IMG.'/01_time.jpg'?>"   data-id="check"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_1"  data-value="2" ><img class="location  opacity1" src="<?=CAT_IMG.'/02_road.jpg'?>"   data-id="location" alt=""></div>
      <div class="icon-placeholder" data-name="icon_2"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/06_center.jpg'?>" data-id="center"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_3"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/07_build.jpg'?>"  data-id="build"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_4"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/08_refresh.jpg'?>"data-id="refresh"  alt=""></div>
      <div class="icon-placeholder" data-name="icon_5"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/09_vip.jpg'?>"    data-id="vip"      alt=""></div>
      <div class="icon-placeholder" data-name="icon_6"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/10_low.jpg'?>"    data-id="cheap"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_7"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/11_mice.jpg'?>"   data-id="mice"     alt=""></div>
      <div class="icon-placeholder" data-name="icon_8"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/12_honey.jpg'?>"  data-id="honey"    alt=""></div>
      <div class="icon-placeholder" data-name="icon_9"  data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/13_family.jpg'?>" data-id="family"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_10" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/14_active.jpg'?>" data-id="active"   alt=""></div>
      <div class="icon-placeholder" data-name="icon_11" data-value="99"><img class="simple    opacity0" src="<?=CAT_IMG.'/15_relax.jpg'?>"  data-id="relax"    alt=""></div>
    </div>
  <div id="end-line">
  </div>
  <div id="ceo-line">
  </div>
  <div id="sayama-comments" class="text block">
  </div>
  <div id="base-info" class="text block">
  </div>
      <div class="block-name">Информация об отеле</div>
  <div id="rooms" class="table-block block">
      <div class="block-name">Номерной фонд
      </div>
      <table>
          <thead>
              <tr>
                  <th>Типы номеров</th>
                  <th>Кол-во</th>
                  <th>м2</th>
                  <th>Взр+дет.</th>
                  <th>Вид</th>
              </tr>
          </thead>
          <tbody>
          <tr>
          </tr>
      </table>
  </div>

  <div id="room-services" class="text block list">
  <div class="block-name">Услуги в номере
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="site-services" class="text block list">
  <div class="block-name">Услуги на территории отеля
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="pool" class="text block list">
  <div class="block-name">Бассейн
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="business-services" class="table-block block">
  <div class="block-name">Бизнес услуги
  </div>
    <table>
          <thead>
              <tr>
                  <th>Зал</th>
                  <th>м2</th>
                  <th>Theater</th>
                  <th>Classroom</th>
                  <th>Banquet</th>
                  <th>Cocktail</th>
                  <th>U-shape</th>
              </tr>
          </thead>
          <tbody>
          <tr>
          </tr>
    </table>
  </div>

  <div id="children-services" class="text block list">
  <div class="block-name">Услуги для детей
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="restaurants" class="table-block block list">
  <div class="block-name">Рестораны и бары на территории
  </div>
    <table>
          <thead>
              <tr>
                  <th>Наименование</th>
                  <th>Режим работы</th>
                  <th>Кухня</th>
              </tr>
          </thead>
          <tbody>
          <tr>
          </tr>
    </table>
  </div>

  <div id="sports-ents" class="text block list">
  <div class="block-name">Спорт и развлечения
  </div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>

  <div id="beach" class="text block list">
  <div class="block-name">Пляж
  </div>
    <div class="top-part"></div>
    <div class="left-part">
    </div>
    <div class="right-part">
    </div>
  </div>
  <div id="cards" class="text block">
      <div class="block-name">Карты к оплате
      </div>
      <div class="content"></div>
  </div>
  <div id="deposit" class="text block">
      <div class="block-name">Депозит при заселении
      </div>
      <div class="content"></div>
  </div>
  
  <!--
  <div id="map" class="text block">
      <div class="block-name">Файл с картой
      </div>
      <div class="content"></div>
  </div>
  
  <div id="map_index" class="text block">
      <div class="block-name">Индекс квадрата на карте
      </div>
      <div class="content"></div>
  </div>
  
  <div id="image_1" class="text block">
      <div class="block-name">Файл первого изображения
      </div>
      <div class="content"></div>
  </div>
  -->
</div>

<!--- EDIT -->
<?php include 'reservation_icons_text.tpl';?>
<div class="col-xs-6 edit">
    <div class="icon-tab">
    <div class="exported-overlay"></div>
<!----------------------------------------------- LOCATION ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="location"  src="<?=CAT_IMG.'/02_road_r.jpg'?>" data-id="location">
       <input type="text" data-name="icon_1_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_1_text')?>">
        <div class="icon-select">
            <img class="option <?= $cat_info->get('icon_1') == 2 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/02_road_r.jpg'?>"     data-input="1" data-id="location" data-icon-id="2" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 3 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/03_promenad_r.jpg'?>" data-input="1" data-id="location" data-icon-id="3" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 4 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/04_beach_r.jpg'?>"    data-input="0" data-id="location" data-icon-id="4" data-beach-icon="1">
            <img class="option <?= $cat_info->get('icon_1') == 5 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/05_town_r.jpg'?>"     data-input="0" data-id="location" data-icon-id="5" data-beach-icon="0">
        </div>
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_2') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/06_center_r.jpg'?>" data-id="center" data-icon-id="6" ></div>

<!----------------------------------------------- BUILD    ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_3') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/07_build_r.jpg'?>"  data-id="build"    data-icon-id="7" >
        <input type="text" data-name="icon_3_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_3_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>


<!----------------------------------------------- REFRSH   ------------------------------------------------------------------------------>
      <div class="icon-placeholder">
        <img class="simple <?= $cat_info->get('icon_4') != 99 ? 'init-click' : ''?>" src="<?=CAT_IMG.'/08_refresh_r.jpg'?>"data-id="refresh"  data-icon-id="8" >
        <input type="text" data-name="icon_4_text" class="form-control icon-text-info" readonly="true" value="<?= $cat_info->get('icon_4_text')?>">
      </div>
<!-------------------------------------------------------  ------------------------------------------------------------------------------>

      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_5') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/09_vip_r.jpg'?>"    data-id="vip"    data-icon-id="9" ></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_6') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/10_low_r.jpg'?>"    data-id="cheap"  data-icon-id="10"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_7') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/11_mice_r.jpg'?>"   data-id="mice"   data-icon-id="11"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_8') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/12_honey_r.jpg'?>"  data-id="honey"  data-icon-id="12"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_9') != 99 ? 'init-click' : '' ?>"  src="<?=CAT_IMG.'/13_family_r.jpg'?>" data-id="family" data-icon-id="13"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_10') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/14_active_r.jpg'?>" data-id="active" data-icon-id="14"></div>
      <div class="icon-placeholder"><img class="simple <?=$cat_info->get('icon_11') != 99 ? 'init-click' : '' ?>" src="<?=CAT_IMG.'/15_relax_r.jpg'?>"  data-id="relax"  data-icon-id="15"></div>
    </div>
    <div id="base-info-text" data-for="#base-info" data-type="text" class="block">
      <div class="block-name">Информация об отеле 
      </div>
        <textarea data-name="base_info"> <?=$cat_info->get('base_info') ?>
        </textarea>
    </div>

    <div id="rooms-text" data-for="#rooms" data-type="table" class="block">
      <div class="block-name">Номерной фонд
      </div>
        <textarea data-name="rooms"><?=$cat_info->get('rooms') ?>
        </textarea>
    </div>

    <div id="room-services-text" data-for="#room-services" class="block list">
      <div class="block-name">Услуги в номере
      </div>
      <div class="left-part" data-type="list" data-for="#room-services .left-part">
        <textarea data-name="room_services_left"><?=$cat_info->get('room_services_left') ?>
        </textarea>
      </div>
      <div class="right-part" data-type="list" data-for="#room-services .right-part">
        <textarea data-name="room_services_right"><?=$cat_info->get('room_services_right') ?>
        </textarea>
      </div>
    </div>

    <div id="site-services-text" data-for="#site-services" class="block list">
      <div class="block-name">Услуги на территории отеля
      </div>
        <div class="left-part" data-type="list" data-for="#site-services .left-part">
          <textarea data-name="site_services_left"><?=$cat_info->get('site_services_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#site-services .right-part">
          <textarea data-name="site_services_right"><?=$cat_info->get('site_services_right') ?>
          </textarea>
        </div>
    </div>

    <div id="pool-text" data-for="#pool" class="block list">
      <div class="block-name">Бассейн <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#pool .top-part">
            <textarea data-name="pool_top"><?= $cat_info->get('pool_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#pool .left-part">
          <textarea data-name="pool_left"><?=$cat_info->get('pool_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#pool .right-part">
          <textarea data-name="pool_right"><?=$cat_info->get('pool_right') ?>
          </textarea>
        </div>
    </div>

    <div id="business-services-text" data-for="#business-services" data-type="table" class="block">
      <div class="block-name">Бизнес услуги <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="business_services"><?=$cat_info->get('business_services') ?>
        </textarea>
    </div>

    <div id="children-services-text" data-for="#children-services" class="block list">
      <div class="block-name">Услуги для детей <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#children-services .left-part">
          <textarea data-name="children_services_left"><?=$cat_info->get('children_services_left') ?>
        
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#children-services .right-part">
          <textarea data-name="children_services_right"><?=$cat_info->get('children_services_right') ?>
          </textarea>
        </div>
    </div>

    <div id="restaurants-text" data-for="#restaurants" data-type="table" class="block">
      <div class="block-name"> Рестораны и бары на территории
      </div>
        <textarea data-name="restaurants"><?=$cat_info->get('restaurants') ?>
        </textarea>
    </div>

    <div id="sports-ents-text" data-for="#sports-ents" class="block list">
      <div class="block-name"> Спорт и развлечения <input title="убрать" type="checkbox"></input>
      </div>
         <div class="left-part" data-type="list" data-for="#sports-ents .left-part">
          <textarea data-name="sports_ents_left"><?=$cat_info->get('sports_ents_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#sports-ents .right-part">
          <textarea data-name="sports_ents_right"><?=$cat_info->get('sports_ents_right') ?>
          </textarea>
        </div>
    </div>

    <div id="beach-text" data-for="#beach" class="block list">
      <div class="block-name"> Пляж <input title="убрать" type="checkbox"></input>
      </div>
         <div class="top-part" data-type="text" data-for="#beach .top-part">
            <textarea data-name="beach_top"><?= $cat_info->get('beach_top') ?>
            </textarea>
         </div>
         <div class="left-part" data-type="list" data-for="#beach .left-part">
          <textarea data-name="beach_left"><?=$cat_info->get('beach_left') ?>
          </textarea>
        </div>
        <div class="right-part" data-type="list" data-for="#beach .right-part">
          <textarea data-name="beach_right"><?=$cat_info->get('beach_right') ?>
          </textarea>
        </div>
    </div>
    
    <div id="cards-text" data-for="#cards .content" data-type="text" class="block">
      <div class="block-name">Карты к оплате
      </div>
        <textarea data-name="cards"><?=$cat_info->get('cards') ?>
        </textarea>
    </div>

    <div id="deposit-text" data-for="#deposit .content" data-type="text" class="block">
      <div class="block-name">Депозит при заселении <input title="убрать" type="checkbox"></input>
      </div>
        <textarea data-name="deposit"><?=$cat_info->get('deposit') ?>
        </textarea>
    </div>
    
    <div id="sayama-comments-text" data-for="#sayama-comments" data-type="text" class="block">
      <div class="block-name">Комментарии Саямы
      </div>
        <textarea data-name="comments_sayama"><?=$cat_info->get('comments_sayama') ?>
        </textarea>
    </div>
    
    <div id="map-text" data-for="#map .content" data-type="text" class="block">
    	<div class="block-name">Файл с картой</div>
    	<textarea data-name="map" class="no_split"><?=$cat_info->get('map') ?></textarea>
    </div>
    
    <div id="map_index-text" data-for="#map_index .content" data-type="text" class="block">
    	<div class="block-name">Индекс квадрата на карте</div>
    	<textarea data-name="map_index" class="no_split"><?=$cat_info->get('map_index') ?></textarea>
    </div>
    
    <div id="image_1-text" data-for="#image_1 .content" data-type="text" class="block">
    	<div class="block-name">Файл 1-го изображения</div>
    	<textarea data-name="image_1" class="no_split"><?=$cat_info->get('image_1') ?></textarea>
    </div>
    
    <div id="image_2-text" data-for="#image_2 .content" data-type="text" class="block">
    	<div class="block-name">Файл 2-го изображения</div>
    	<textarea data-name="image_2" class="no_split"><?=$cat_info->get('image_2') ?></textarea>
    </div>
    
    <div id="image_2_txt-text" data-for="#image_2_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 2-го изображения</div>
    	<textarea data-name="image_2_txt"><?=$cat_info->get('image_2_txt') ?></textarea>
    </div>
    
    <div id="image_3-text" data-for="#image_3 .content" data-type="text" class="block">
    	<div class="block-name">Файл 3-го изображения</div>
    	<textarea data-name="image_3" class="no_split"><?=$cat_info->get('image_3') ?></textarea>
    </div>
    
    <div id="image_3_txt-text" data-for="#image_3_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 3-го изображения</div>
    	<textarea data-name="image_3_txt"><?=$cat_info->get('image_3_txt') ?></textarea>
    </div>
    
    <div id="image_4-text" data-for="#image_4 .content" data-type="text" class="block">
    	<div class="block-name">Файл 4-го изображения</div>
    	<textarea data-name="image_4" class="no_split"><?=$cat_info->get('image_4') ?></textarea>
    </div>
    
    <div id="image_4_txt-text" data-for="#image_4_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 4-го изображения</div>
    	<textarea data-name="image_4_txt"><?=$cat_info->get('image_4_txt') ?></textarea>
    </div>
    
    <div id="image_5-text" data-for="#image_5 .content" data-type="text" class="block">
    	<div class="block-name">Файл 5-го изображения</div>
    	<textarea data-name="image_5" class="no_split"><?=$cat_info->get('image_5') ?></textarea>
    </div>
    
    <div id="image_5_txt-text" data-for="#image_5_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 5-го изображения</div>
    	<textarea data-name="image_5_txt"><?=$cat_info->get('image_5_txt') ?></textarea>
    </div>
    
    <div id="image_6-text" data-for="#image_6 .content" data-type="text" class="block">
    	<div class="block-name">Файл 6-го изображения</div>
    	<textarea data-name="image_6" class="no_split"><?=($cat_info->get('image_6'))?></textarea>
    </div>
    
    <div id="image_6_txt-text" data-for="#image_6_txt .content" data-type="text" class="block">
    	<div class="block-name">Описание 6-го изображения</div>
    	<textarea data-name="image_6_txt"><?=$cat_info->get('image_6_txt') ?></textarea>
    </div>
</div>

</div>
