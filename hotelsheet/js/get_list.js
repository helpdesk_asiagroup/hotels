(function($) {
var re = /([^&=]+)=?([^&]*)/g;
var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
$.parseParams = function(query) {
    var params = {}, e;
    while ( e = re.exec(query) ) { 
        var k = decode( e[1] ), v = decode( e[2] );
        if (k.substring(k.length - 2) === '[]') {
            k = k.substring(0, k.length - 2);
            (params[k] || (params[k] = [])).push(v);
        }
        else params[k] = v;
    }
    return params;
};
})(jQuery);

function applyFilter(filter) {
    if ($.trim(filter.name)) {
        var query = $.trim(filter.name).toUpperCase();
        $('.list-group a').each(function() {
            var name = $.trim($(this).html()).toUpperCase();
            if(name.indexOf(query)+1) {
                $(this).show();
            } else {  
                $(this).hide();
            }
        });

    } else {
        $('.list-group a').show();
    }

    if (Number(filter.region) && filter.only_finished) {
        $('.js-export-btn').show();
    } else {
        $('.js-export-btn').hide();
    }

    if (Number(filter.region)) {
        var selector = '.list-group a[data-block-id!='+filter.region+']:visible';
        $(selector).hide();
        console.log(selector)
    }

    if (Number(filter.tom)) {
        var selector = '.list-group a[data-tom!='+filter.tom+']:visible';
        $(selector).hide();
        console.log(selector)
    }

    if (filter.only_not_finished) {
         $('.list-group a[data-finished=1]:visible').hide()
    } else {
         $('.list-group a[data-finished=1]:visible').show()
    }

    if (filter.only_finished) {
         $('.list-group a[data-finished=0]:visible').hide()
    } else {
         $('.list-group a[data-finished=0]:visible').show()
    }

    if (filter.with_notes) {
         $('.list-group a[data-notes=0]:visible').hide()
    } else {
         $('.list-group a[data-notes=0]:visible').show()
    }

    if (filter.luxury) {
         $('.list-group a[data-luxury=0]:visible').hide()
    } else {
         $('.list-group a[data-luxury=0]:visible').show()
    }

    if (filter.mice) {
         $('.list-group a[data-mice=0]:visible').hide()
    } else {
         $('.list-group a[data-mice=0]:visible').show()
    }
    if (filter.exported) {
         $('.list-group a[data-exported=0]:visible').hide()
    } else {
         $('.list-group a[data-exported=0]:visible').show()
    }
    if (filter.not_exported) {
         $('.list-group a[data-exported=1]:visible').hide()
    } else {
         $('.list-group a[data-exported=1]:visible').show()
    }
    if (filter.changed_after_export) {
         $('.list-group a[data-changed-after-export=0]:visible').hide()
    } else {
         $('.list-group a[data-changed-after-export=0]:visible').show()
    }
    if (filter.changed_page) {
         $('.list-group a[data-changed-page=0]:visible').hide()
    } else {
         $('.list-group a[data-changed-page=0]:visible').show()
    }
    if (filter.changed_position) {
         $('.list-group a[data-changed-position=0]:visible').hide()
    } else {
         $('.list-group a[data-changed-position=0]:visible').show()
    }

    if (filter.corr_checked) {
         $('.list-group a[data-corr-checked=0]:visible').hide()
    } else {
         $('.list-group a[data-corr-checked=0]:visible').show()
    }

    if (filter.corr_unchecked) {
         $('.list-group a[data-corr-unchecked=0]:visible').hide()
    } else {
         $('.list-group a[data-corr-unchecked=0]:visible').show()
    }

    if (filter.corr_declined) {
         $('.list-group a[data-corr-status-declined=0]:visible').hide()
    } else {
         $('.list-group a[data-corr-status-declined=0]:visible').show()
    }

    if (filter.corr_checked_ok) {
         $('.list-group a[data-corr-status-checked-ok=0]:visible').hide()
    } else {
         $('.list-group a[data-corr-status-checked-ok=0]:visible').show()
    }

    if (filter.approved) {
         $('.list-group a[data-approved=0]:visible').hide()
    } else {
         $('.list-group a[data-approved=0]:visible').show()
    }

    if (filter.not_approved) {
         $('.list-group a[data-not-approved=0]:visible').hide()
    } else {
         $('.list-group a[data-not-approved=0]:visible').show()
    }

    $('.js-approved-checkbox').hide();
    $('.list-group a:visible').each(function() {
        hid = $(this).attr('data-hotel-id');
        $('#js-approved-checkbox-hotel-id-'+hid).show();
    });

    $('#approved').siblings('.js-count').text(' (' + $('.list-group a[data-approved=1]:visible').length+')');
    $('#not-approved').siblings('.js-count').text(' (' + $('.list-group a[data-approved=0]:visible').length+')');

}

$(function() {

    filter = {
        name: '', only_not_finished: false, only_finished: false, 
        region: 0, with_notes: 0, luxury: 0, mice: 0, tom: 0,
        changed_after_export: 0, changed_page: 0, changed_position: 0, exported: 0, not_exported: 0,
        corr_checked: 0, corr_unchecked: 0, corr_declined: 0, corr_checked_ok: 0,
        approved: 0, not_approved: 0
    };

    applyFilter(filter);

    $('#luxury-filter').change(function() {
        filter.luxury = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#mice-filter').change(function() {
        filter.mice = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#not-finished-filter').change(function() {
        filter.only_not_finished = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#finished-filter').change(function() {
        filter.only_finished = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#name-filter').on('input', function() {
        filter.name = $(this).val();
        applyFilter(filter);
    });

    $('#with-notes').change(function() {
        filter.with_notes = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#changed-after-export-filter').change(function() {
        filter.changed_after_export = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#changed-page-filter').change(function() {
        filter.changed_page = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#changed-position-filter').change(function() {
        filter.changed_position = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#not-exported-filter').change(function() {
        filter.not_exported = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#exported-filter').change(function() {
        filter.exported = $(this).is(':checked');
        applyFilter(filter);
    });

    $('select[name=region_id]').change(function () {
        filter.region = $(this).val();
        applyFilter(filter);
    });

    $('select[name=volume_id]').change(function () {
        filter.tom = $(this).val();
        applyFilter(filter);
    });

    $('#corr-unchecked').change(function() {
        filter.corr_unchecked = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#corr-checked').change(function() {
        filter.corr_checked = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#corr-declined').change(function() {
        filter.corr_declined = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#corr-checked-ok').change(function() {
        filter.corr_checked_ok = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#approved').change(function() {
        filter.approved = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#not-approved').change(function() {
        filter.not_approved = $(this).is(':checked');
        applyFilter(filter);
    });

    $('.total').click(function () {
        $('.tom').slideToggle({duration: 1000, easing: 'easeOutCubic'});
    });

    $('.tom .progress').click(function() {
        var block_id = $(this).attr('data-block-id');
        $('select[name=region_id]').val(block_id).change();
    });

    $('.js-year-select-redirect, .js-type-select-redirect').change(function(){
        var request_obj = $.parseParams(window.location.search.replace('?', ''));
        var param = $(this).attr('name');
        request_obj[param] = $(this).val();
        window.location.search = '?' + $.param(request_obj);
    })
    
    var url = '/hotelsheet/?year=' + $('.js-year-select-redirect').val() + '&type=' + $('.js-type-select-redirect').val(); 
    window.history.replaceState('Marketing App', 'Marketing App', url);

    $('.js-export-btn').click(function () {
        type = $('.js-type-select-redirect').val();
        year = $('.js-year-select-redirect').val();
        block = $('.js-block-select').val();
        ids = [];
        $('.list-group-item:visible').each(function() {
            ids.push($(this).attr('data-hotel-id'));
        });

        form = $('#export-block');
        form.find('[name=block_id]').val(block);
        form.find('[name=type]').val(type);
        form.find('[name=year]').val(year);
        form.find('[name=ids]').val(JSON.stringify(ids));

        if (type == 3 || type == 1) {
            form.submit();
        }
    });

function ajax(url, query, successf, beforef) {
    $.ajax({
        url: url,
        data: query,
        type: 'POST',
        dataType: 'json',
        beforeSend: beforef,
        success: successf
    });
}

    $('.js-approved-checkbox').click(function(e){
        e.preventDefault();
        checkbox = $(this);
        hotel_id = checkbox.attr('data-hotel-id');
        checkbox.hide();
        data = {
            id: hotel_id,
            type: $('.js-type-select-redirect').val(),
            year: $('.js-year-select-redirect').val(),
            action: checkbox.is(':checked') ?  "approve" : "reject"
        }

        ajax('ajax/approve.php', data,
             success = function(answer) {
                if (answer.status == 1) {
                    action = answer.action;
                    checkbox.show();
                    checkbox.prop("checked", action == "approve" ? true : false)
                    hotel = $('a.list-group-item[data-hotel-id='+hotel_id+']');
                    switch (action) {
                        case 'approve':
                            hotel.removeClass('list-group-item-success')
                                .addClass('list-group-item-info')
                                .attr('data-approved', 1);
                            break;
                        case 'reject':
                            hotel.removeClass('list-group-item-info')
                                .addClass('list-group-item-success')
                                .attr('data-approved', 0);
                    }
                }
             })
    });

});
