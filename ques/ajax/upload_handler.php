<?php
    include '../../include/include.php';

    header('Content-Type: application/json');

    $result = array('status' => 0);

    $hotel_id = Request::getInt('hotel_id');
    $year     = Request::getInt('year');
    $file     = Request::getFile('file');

    if (!$hotel_id || !$file || !$year || $file['error'] != UPLOAD_ERR_OK) {
        $result['message'] = 'File uploading failed! <br> May be file is too big or not Word/PDF file?';
        echo json_encode($result);
        die();
    }

    $path = "ques_repo/$year/";
    $ext = end(explode('.', $file['name']));

    $hotel = Hotel::load($hotel_id);
    $hotel->loadCatalogueInfo($year);

    $file_name = preg_replace('/[^\w\d]/', '_', $hotel->get('name'));
    $file_name .= '_'.$hotel_id . '_' . date('Y-m-d_H_i');
    $fullpath = '/data/sites/hotels.sayama.hq/'.$path.$file_name.".$ext";

    if (!move_uploaded_file($file['tmp_name'], $fullpath)) {
        $result['message'] = 'FAIL! Cant save file on server!';
        echo json_encode($result);
        die();
    }

    $hotel->catalogue_info->set('ques', "/".$path.$file_name.".$ext");
    $hotel->save($save_comp_info = false, $save_catalogue_info = true);

    $result['status'] = 1;
    $result['hotel_id'] = $hotel_id;
    $result['filepath'] = '/'.$path.$file_name.".$ext";
    $result['message']  = "SUCCESS! <br> File successfully saved with name $path$file_name.$ext";
    echo json_encode($result);
