<?php

    include '../include/include.php';

    $engine->requireGroup('thai');

    $action = Request::getVar('action', $default = "get_list");
    $year = Request::getInt('year', $default = 2017);

    switch ($action) {
        case 'get_list':
            $regions_obj = Region::load();
            $regions[0] = 'ALL';
            foreach ($regions_obj as $reg_obj) {
                $regions[$reg_obj->get('id')] = $reg_obj->get('name');
            }

            $db = db::instance();
            $sql = "SELECT h.id, h.name, r.name AS block, cat.block AS block_id, cat.exported AS exported,
                           cat.notes, r.tom, cat.ques, cc.luxury, cc.mice FROM hotels h
                           LEFT JOIN catalogue_contents cc ON cc.hotel_id = h.id
                           LEFT JOIN catalogue_$year cat ON cat.hotel_id = h.id
                           LEFT JOIN regions r ON r.id = cat.block
                        WHERE cc.general = 1 AND cc.year = $year
                        ORDER BY r.tom ASC, cat.block ASC, cat.order ASC";

            $db->query($sql);
            while($row = $db->fetch_row()) {
                $list[] = $row;
            }

            $view = new View();
            $view->setTemplate('get_list', compact('list', 'regions', 'year'));
            $view->addGlobalJs('jquery.ui.widget.js');
            $view->addGlobalJs('jquery.fileupload.js');
            $view->addGlobalCss('jquery.fileupload.css');
            $view->addAppsCss('get_list.css');
            $view->addAppsJs('get_list.js');
            $view->addAppsJs('ques_upload.js');

            $view->display();
            break;
    }
