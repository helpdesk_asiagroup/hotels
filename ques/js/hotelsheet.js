function toggleNoteWrapper() {

    wrapper = $('.note-wrapper');

    if(wrapper.height()) {
        wrapper.animate({width: '54px'}, 600, 'easeInCubic', function() {
            $(this).animate({height: '0px'}, 200);
        });
    } else {
        wrapper.animate({height: '54px'}, 200, function() {
            $(this).animate({width: '1075px'}, 600, 'easeOutCubic');
        });
    }
}

function showSubregionsForRegionId(region_id) {
    var selector = '.subregion select.parent-id-'+region_id;
    $(selector).show().siblings('select').hide();
}

function iconSelect(action) {

    switch (action) {
        case 'open':
            var menu = $('.edit .icon-select');
            menu.css('height','56px');
            menu.animate({width: '90px'}, 200, function() {
                $(this).animate({height:'280px'}, 400);
            });
            break;

        case 'close':
            var menu = $('.edit .icon-select');
            menu.animate({height: '56px'}, 400, function() {
                $(this).animate({width:'0px'}, 200);
            });
            break;
    }
}

function parseInput(type, text) {
    switch (type) {
        case 'text':
            return $.trim(text);
            break;

        case 'list':
            var retval = '';
            var lines = text.split('\n');
            retval += '<ul>';
            for (var line in lines) {
                retval += '<li>'+$.trim(lines[line])+'</li>';
            }
            retval += '</ul>';
            return retval;

        case 'table':
            var retval = '';
            var rows = text.split('\n');

            for (var row in rows) {
                if (!$.trim(rows[row])) {
                    console.log(row);
                    continue;
                }
                retval += '<tr>'
                var cells = rows[row].split(';');

                for (var cell in cells) {
                    retval += '<td>'+$.trim(cells[cell])+'</td>';
                }
                retval += '</tr>';
            }
            return retval;
    }
}

function getQueryParams() {
    //trigger change on block and area, to be sure
    //that we save what user see.

    $('#control select:visible').change();

    var params = {};
    params.year       = $('#year').val();
    params.hotel_id   = $('#hotel-id').val();
    params.name       = $('#hotel-name').val();
    params.finished   = $('#finished').is(':checked') ? 1 : 0;
    params.rate       = $('#rate').val();
    params.site       = $('#site').val();
    params.ceo        = $('#ceo').val();
    params.check_in   = $('#check-in').val();
    params.check_out  = $('#check-out').val();
    params.block      = $('#region_id').val();
    params.area       = $('#subregion_id').val();
    params.icon_video = $('img.video-icon').attr('data-value');
    params.notes      = $.trim($('.note-text input').val());
    
    $('textarea[data-name]').each(function() {
        var name = $(this).attr('data-name');
        var val = $(this).val().split('\n').join('#');
        var hide = $(this).parents('.block').find('input[type=checkbox]').is(':checked')
        if (hide == false) {
            params[name] = val;
        } else {
            params[name] = '!empty!';
        }
    });

    $('.preview .icon-placeholder').each(function() {
        var name = $(this).attr('data-name');
        var val  = $(this).attr('data-value');
        params[name] = val;
    });

    $('input.icon-text-info').each(function() {
        var name = $(this).attr('data-name');
        var val = $(this).attr('readonly') ? '' : $(this).val();
        params[name] = val;
    });
    return params;
}


function sendRequest(url, query) {
    $.ajax({
        url: url, 
        data: query,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('.overlay').show();
        },
        success: function(data) {
            if (data.status == 'OK') {
                $('.overlay').hide();
            } else {
                $('.overlay .msg .text').html(data.error);
                $('.overlay .msg button').slideDown();
            }
        }
    }); 
}

$(function() {

    if(parseInt($('#exported').val())) {
        $('textarea, input, select, .save-btn').attr('disabled', 'disabled');
        $('.exported-overlay').show();
    }


    $('input[type=checkbox]').click(function() {
        var selector = $(this).parents('[data-for]').attr('data-for');
        var block_div = $(selector).parents('.block').length ? $(selector).parents('.block') : $(selector);
        if ($(this).is(':checked')) {
            block_div.fadeOut('fast');
        } else {
            block_div.fadeIn('fast');
        }
    });

    
    $('textarea').autosize();

    $('textarea').bind('input propertychange', function() {
        saved = false;

        var parent_div = $(this).parents('div[data-type]');

        var selector = parent_div.attr('data-for');
        var type     = parent_div.attr('data-type');
        var text     = $(this).val();

        if ($.trim(text) == '!empty!') {
            var checkbox = $(this).parents('.block').find('input[type=checkbox]');
            if (checkbox.is(':checked') == false) {
                checkbox.trigger('click');
                $(this).val('');
            }
            return;
        }

        selector = type == 'table' ? selector+' tbody': selector;

        $(selector).html(parseInput(type,text));
    })

    $('#control button.save-btn').click(function() {
        saved = true;
        sendRequest('ajax/save_catalogue_info.php', getQueryParams());
    });

    $('textarea').val(function () {
        return $.trim($(this).val()).split('#').join('\n');
    });

    $('textarea').trigger('propertychange').trigger('autosize.resize');


    window.onbeforeunload = function (e) {
        var message = 'Есть несохраненные изменения! Если покинуть страницу, они пропадут!'
        e = e || window.event;

        if (e && !saved) {
            e.returnValue = message;
        }

        if (!saved) {
            return message;
        }
    }

    $('.edit .icon-placeholder img.simple').click(function(){
        saved = false;
        var preview = $('.preview .icon-placeholder img[data-id='+$(this).attr('data-id')+']');
        var icon_id  = $(this).attr('data-icon-id');
        var placeholder = preview.parents('.icon-placeholder');
        var input = $(this).siblings('input');

        preview.toggleClass('opacity0 opacity1');

        preview.hasClass('opacity1') ? placeholder.attr('data-value', icon_id) : placeholder.attr('data-value', '99');

        $(this).toggleClass('opacity1');

        if ($(this).hasClass('opacity1')) {
            input.removeAttr('readonly').addClass('opacity1');
        } else {
            input.attr('readonly', 'true').removeClass('opacity1');
        }
    });

    // Open icon slider options #######################################################
    $('.edit .icon-placeholder img.location').click(function() {
        $(this).toggleClass('open-menu');

        //Remove bech icons if BANGKOK, CHIANGMAI, CHIANGRAI, NAKHONRATCHASIMA
        regionsWithoutBeaches = ['1','19','20','26'];
        if (regionsWithoutBeaches.indexOf($('[name=region_id]').val()) > -1) {
            $('[data-beach-icon=1]').hide()
        } else {
            $('[data-beach-icon=1]').show()
        }

        if ($(this).hasClass('open-menu')) {
            iconSelect('open');
        } else {
            iconSelect('close');
        }
    });


    // Put selected Icon into preview and edit #######################################
    $('.edit img.option').click(function() {
        saved = false;
        var src     = $(this).attr('src');
        var icon_id = $(this).attr('data-icon-id');
        var input   = $(this).parents('.icon-placeholder').find('input');

        $(this).addClass('opacity1').siblings('img').removeClass('opacity1');
        $('.edit .icon-placeholder img.location').attr('src', src);
        $('.preview .icon-placeholder img.location')
            .attr('src', src.split('_r.jpg').join('.jpg'))
            .parents('.icon-placeholder').attr('data-value', icon_id);

        //Show input if need to provide a text info with icon 
        if ($(this).attr('data-input') == 1) {
            input.removeAttr('readonly').addClass('opacity1');
        } else {
            input.attr('readonly', 'true').removeClass('opacity1');
        }
    })

    $('.region select').change(function() {
        saved = false;
        $('#region_id').val($(this).val());
        showSubregionsForRegionId($(this).val());
    });

    $('.subregion select').change(function(){
        saved = false;
        $('#subregion_id').val($(this).val());
    });

    $('img.video-icon').click(function(){
        saved = false;
        $(this).toggleClass('opacity1');
        $(this).attr('data-value', function(i,cur_val){
            return cur_val == '99' ? '16' : '99';
        })
    });

    $('.init-click').click();
    $('.region select').change(); 

    $('.notes button').click(function() { toggleNoteWrapper()});

    var saved = true;
});
