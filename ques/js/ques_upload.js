var hotel_id;
$(function() {
    var modal = {};
    var modalObj = $('.modal');
    var year = $('.js-year-select-redirect').val();

    modal.title = modalObj.find('.modal-title');
    modal.cur_file = modalObj.find('.cur-file');
    modal.upload_btn = modalObj.find('#fileupload');

    upload_progress = $('#progress .progress-bar');

    modalObj.on('hidden.bs.modal', function() {
        upload_progress.css('width', '0%');
        $('.js-success-message').html('');
        $('.js-error-message').html('');
        modal.cur_file.html('');
    });

    $('.js-dwnld-link').click(function () {
        window.open($(this).attr('data-href'),'_blank')
    });

    $('.js-upload-btn').click(function() {
        modal.title.html($(this).attr('data-hotel-name'));
        hotel_id = $(this).attr('data-hotel-id');
        var link = $(this).siblings('.js-dwnld-link').attr('data-href');
        modal.cur_file.html(link);
    });

    $('#fileupload').bind('fileuploadsubmit', function(e, data) {
        data.formData = {'hotel_id': hotel_id};
    });

    $('.fileinput-button').click(function() {
        upload_progress.css('width', '0%');
        $('.js-success-message').html('');
        $('.js-error-message').html('');
    });

    // Change this to the location of your server-side upload handler:
    var url ='ajax/upload_handler.php?year=' + year
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            console.log(data);
            if (data.result.status == 1) {
                modal.cur_file.text(data.result.filepath);
                var list_item = $('a.list-group-item[data-hotel-id='+data.result.hotel_id+']');
                list_item.find('.js-uploaded-icon').addClass('glyphicon-ok');
                list_item.find('.js-dwnld-link').attr('data-href', data.result.filepath).removeAttr('disabled');
                $('.js-success-message').html(data.result.message);
            } else {
                $('.js-error-message').html(data.result.message);
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            upload_progress.css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
