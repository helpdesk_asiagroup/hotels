(function($) {
var re = /([^&=]+)=?([^&]*)/g;
var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
$.parseParams = function(query) {
    var params = {}, e;
    while ( e = re.exec(query) ) { 
        var k = decode( e[1] ), v = decode( e[2] );
        if (k.substring(k.length - 2) === '[]') {
            k = k.substring(0, k.length - 2);
            (params[k] || (params[k] = [])).push(v);
        }
        else params[k] = v;
    }
    return params;
};
})(jQuery);

function applyFilter(filter) {

    if ($.trim(filter.name)) {
        var query = $.trim(filter.name).toUpperCase();
        $('.list-group a').each(function() {
            var name = $.trim($(this).find('.js-hot-name').html()).toUpperCase();
            if(name.indexOf(query)+1) {
                $(this).show();
            } else {  
                $(this).hide();
            }
        });

    } else {
        $('.list-group a').show();
    }

    if (Number(filter.region)) {
        var selector = '.list-group a[data-block-id!='+filter.region+']:visible';
        $(selector).hide();
        console.log(selector)
    }

    if (filter.only_not_finished) {
         $('.list-group a[data-finished=1]:visible').hide()
    } else {
         $('.list-group a[data-finished=1]:visible').show()
    }

    if (filter.only_finished) {
         $('.list-group a[data-finished=0]:visible').hide()
    } else {
         $('.list-group a[data-finished=0]:visible').show()
    }

    if (filter.with_notes) {
         $('.list-group a[data-notes=0]:visible').hide()
    } else {
         $('.list-group a[data-notes=0]:visible').show()
    }

    if (filter.luxury) {
         $('.list-group a[data-luxury=0]:visible').hide()
    } else {
         $('.list-group a[data-luxury=0]:visible').show()
    }

    if (filter.mice) {
         $('.list-group a[data-mice=0]:visible').hide()
    } else {
         $('.list-group a[data-mice=0]:visible').show()
    }
}

$(function() {

    filter = {name: '', only_not_finished: false, only_finished: false, region: 0, with_notes: 0, luxury: 0, mice: 0};

    $('#luxury-filter').change(function() {
        filter.luxury = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#mice-filter').change(function() {
        filter.mice = $(this).is(':checked');
        applyFilter(filter);
    });


    $('#not-finished-filter').change(function() {
        filter.only_not_finished = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#finished-filter').change(function() {
        filter.only_finished = $(this).is(':checked');
        applyFilter(filter);
    });

    $('#name-filter').on('input', function() {
        filter.name = $(this).val();
        applyFilter(filter);
    });

    $('#with-notes').change(function() {
        filter.with_notes = $(this).is(':checked');
        applyFilter(filter);
    });
    
    $('select[name=region_id]').change(function () {
        filter.region = $(this).val();
        applyFilter(filter);
    });

    $('.total').click(function () {
        $('.tom').slideToggle({duration: 1000, easing: 'easeOutCubic'});
    });

    $('.tom .progress').click(function() {
        var block_id = $(this).attr('data-block-id');
        $('select[name=region_id]').val(block_id).change();
    });

    $('.js-year-select-redirect').change(function(){
        var request_obj = $.parseParams(window.location.search.replace('?', ''));
        request_obj['year'] = $(this).val();
        window.location.search = '?' + $.param(request_obj);
    })
});
