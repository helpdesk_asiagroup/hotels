<?php $available_years = array(2015 => 2015, 2016 => 2016, 2017 => 2017); ?>
<div class="container fade-in-loading"> 
    <form class="form-horizontal">
      <div class="form-group">
        <label class="control-label col-xs-7"><h2>General catalogue year:</h2></label>
        <div class="col-sm-2">
        <h3>
            <?= HtmlSnippets::select($available_years, 'year', 'form-control input-lg js-year-select-redirect', $year); ?>
        </h3>
        </div>
      </div>
    </form>

    <div class="row control fade-in-loading">
      <div class="col-xs-10">
        <input id="name-filter" placeholder="Enter Hotel name" class="form-control" type="text">
      </div>
      <div class="col-xs-2">
         <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-sm', $region_id); ?>
      </div>
    </div>

    <div class="row fade-in-loading">
      <div class="list-group">
      <?php foreach ($list as $hot): ?> 
          <?php $cls      = $hot['ques'] ? 'glyphicon-ok' : '' ?>
          <?php $disabled = $hot['ques'] ? '' : 'disabled' ?>
          <a class="list-group-item row" 
             data-hotel-id="<?=$hot['id']?>" 
             data-block-id="<?=$hot['block_id']?>">

            <span class="col-xs-6 js-hot-name"><?= $hot['name'] ?></span>
            <span class="col-xs-3">
              <p class="js-uploaded-icon glyphicon <?=$cls?>" aria-hidden="true"></p>
              <p> <?= $hot['block'] ?> </p>
            </span>
            <span class="col-xs-3">
                <span class="btn btn-primary js-dwnld-link" <?=$disabled?> data-href="<?=$hot['ques']?>"> Download </span>
                <span class="btn btn-primary js-upload-btn" 
                      data-hotel-name="<?=$hot['name']?>" 
                      data-toggle="modal" 
                      data-target="#upload-dialog" 
                      data-hotel-id="<?=$hot['id']?>"> 
                            Upload 
                </span>
            </span>
          </a>
      <?php endforeach; ?>
      </div>
  </div>

<div class="modal " id="upload-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
       <span class="row"> 
            Current questionarie file: <span class="cur-file"></span>
       </span>

       <span class="row"> 
        <span class="btn btn-primary fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Select files...</span>
            <!-- The file input field used as target for the file upload widget -->
            <input id="fileupload" type="file" name="file">
        </span>
        <br><br>
        <!-- The global progress bar -->
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <!-- The container for the uploaded files -->
        <div id="upload-result" class="files">
           <div style="" class="js-success-message text-success"></div>
           <div style="" class="js-error-message text-danger"></div>
        </div>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


</div>


