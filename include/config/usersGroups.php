<?php

$usersGroups = array(
    'developer' => array('editor'),
    'a_duboveckaya' => array('editor', 'thai'),
    'y_snitko' => array('editor', 'thai'/*, 'structureEditor', 'root'*/),
    't_petrov' => array('designer'),
    'thai_staff_1' => array('thai'),
    'thai_staff_2' => array('thai'),
    'dos@sayamatravel.com' => array('reservation'),
    'ados1@sayamatravel.com' => array('reservation'),
    'info@sayamaluxury.com' => array('reservation'),
    'ados@sayamatravel.com' => array('reservation'),
    'rsvn1@sayamaluxury.com' => array('reservation'),
	'buxx' => array('editor', 'designer'),
	'kashuba' => array('editor'),
	'dev3' => array('editor'),
	'helpdesk' => array('editor'),
	'front' => array('designer', 'editor'),
	'journalist' => array('editor', 'thai')
);

return $usersGroups;
