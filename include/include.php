<?php

    define('SCRIPT_PATH', $_SERVER['SCRIPT_FILENAME']);
    define('SCRIPT_DIR', dirname(SCRIPT_PATH)."/");
    define('SCRIPT_TEMPLATE_DIR', SCRIPT_DIR . "template/");

    define('PATH_ROOT', realpath(dirname(__FILE__)) . "/../");
    define('PUBLIC_ROOT', "http://" . $_SERVER['SERVER_NAME'].'/');

    define('PATH_INCLUDE', PATH_ROOT . 'include/');
    define('PUBLIC_INCLUDE', PUBLIC_ROOT . 'include/');

    define('PATH_CONFIG', PATH_INCLUDE . 'config/');
    define('PATH_CLASS', PATH_INCLUDE . 'class/');

    define('PATH_CSS', PATH_INCLUDE . 'css/');
    define('PATH_JS' , PATH_INCLUDE . 'js/');
    define('GLOBAL_TEMPLATES', PATH_ROOT . 'global_template/');

    define('PUBLIC_CSS', PUBLIC_ROOT.'css/');
    define('PUBLIC_JS' , PUBLIC_ROOT.'js/' );

    define('SCRIPT_JS', "js/");
    define('SCRIPT_CSS', "css/");

    define('TIMTHUMB_PATH', PUBLIC_ROOT . 'plugins/timthumb.php');
    define('GLOBAL_SWF', PUBLIC_ROOT . 'swf/');

    define('CAT_ROOT', 'S:\PRINT PRODUCTION\2017\CATALOGUE 2018');

    date_default_timezone_set('Asia/Bangkok');

    function autoload_classes($classname)
    {
        include PATH_CLASS.$classname.'.php';
    }

    spl_autoload_register('autoload_classes');

    function checkSecret()
    {
        $engine = engine::instance();
        $engine->init();

        if ($engine->input['sw'] != 'dartvader') {
            exit('lal');
        }
    }

    include 'functions.php';

    $engine = engine::instance();
    $engine->init();
