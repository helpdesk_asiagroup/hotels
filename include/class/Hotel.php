<?php

class Hotel extends BaseGetSetClass
{
    protected $id = 0;
    protected $name = '';
    protected $latitude = 0;
    protected $longitude = 0;
    protected $region_id = 0;
    protected $general = 0;
    protected $mice = 0;
    protected $luxury = 0;
    protected $comment_2013 = '';
    protected $comment_2014 = '';

    public $competitors_info = array();
    public $catalogue_info;
    public $luxury_info;
    public $mice_info;

    /**
     * __construct
     *
     * @param mixed $data
     * @access private
     * @return Hotel object
     */
    private function __construct($data = false)
    {
        if ($data) {
            foreach ($data as $prop => $val) {
                if (!$this->set($prop, $val)) {
                    $error = "Uknown proprty $prop in ".__METHOD__."()";
                    //throw new Exception ($error);
                }
            }
        }
    }

    /**
     * load
     *
     * @param int $id
     * @static
     * @access public
     * @return void
     */
    public static function load($id = 0)
    {
        if ($id == 0) {

        }

        if (!intval($id)) {
            $error = "Invalid arg ($id) Expected int > 0 in ".__METHOD__."() ";
            throw new Exception($error, 400);
        }

        $db = db::instance();

        $sql = "SELECT * from hotels WHERE id = $id LIMIT 1";
        $db->query($sql);

        if ($row = $db->fetch_row()) {
            return new self($row);
        } else {
            throw new Exception("Hotel with id = $id not find!");
        }

    }

    /**
     * save
     *
     * @access public
     * @return $this
     */
    public function save($save_competitor_info = false, $save_catalogue_info = false, $save_luxury_info = false, $save_mice_info = false)
    {
        $data = array();
        $db = db::instance();

        foreach ($this as $prop => $val) {
            if (!($prop == 'competitors_info' ||
                  $prop == 'catalogue_info'   ||
                  $prop == 'mice_info'        ||
                  $prop == 'luxury_info')) {
                $data[$prop] = trim($val);
                if ($prop == 'name') {
                	$data[$prop] = preg_replace('/\s{2,}/', ' ', $data[$prop]);
                }
            }
        }

        if ($data['id'] == 0) unset($data['id']);
        //$db->replace($data, $table = 'hotels');

        if($save_competitor_info) {
            if ($this->competitors_info) {
                foreach ($this->competitors_info as $comp_obj) {
                    $comp_obj->save();
                }
            } else {
                $error = "Not Loaded competitors information. Noting to save in ".__METHOD__." ()";
            }
        }
        if ($save_catalogue_info) {
            $this->catalogue_info->save();
        }

        if ($save_luxury_info) {
            $this->luxury_info->save();
        }

        if ($save_mice_info) {
            $this->mice_info->save();
        }

        return $this;
    }

    /**
     * createEmpty
     *
     * @static
     * @access public
     * @return $this
     */
    public static function createEmpty()
    {
        return new self();
    }

    /**
     * loadCompetitorsInfo
     *
     * @access public
     * @return $this
     */
    public function loadCompetitorsInfo()
    {
        if(!$this->id) {
            $error = "Invalid Hotel id (".$this->id."). Expected int > 0 in ".__METHOD__."() ";
            throw new Exception($error, 400);
        }
        $this->competitors_info = CompetitorInfo::load($this->id);
        return $this;
    }

    public function loadCatalogueInfo($year = 2015)
    {
        if($this->id != 0) {
            $class = "Catalogue$year";
            $this->catalogue_info = $class::load($this->id, $year);
        }
    }

    public function loadLuxuryInfo($year = 2015)
    {
        if($this->id != 0) {
            $class = "Luxury$year";
            //echo $class;
            $this->luxury_info = $class::load($this->id, $year);
        }
    }

    public function loadMiceInfo($year = 2015)
    {
        if($this->id != 0) {
            $class = "Mice$year";
            $this->mice_info = $class::load($this->id, $year);
        }
    }



    public static function load_all()
    {
        $db = db::instance();

        $sql = "SELECT * from hotels ORDER BY name";
        $db->query($sql);
        $retobjs = array();
        while ($row = $db->fetch_row()) {
            $retobjs[] = new self($row);
        }
        return $retobjs;
    }

    /**
     * getExportData
     *
     * Return Array formed data for InDesign csv
     *
     * @param mixed $position
     * @access public
     * @return void
     */
    public function getExportData($position, $year) {
        if (!$this->catalogue_info) {
            $this->loadCatalogueInfo($year);
        }

        $retarr = $this->catalogue_info->getExportData($position);

        if (!$this->catalogue_info->finished) {
            foreach($retarr as &$val) {
                $val = '';
            }
            $retarr['hotel_id'] = $this->id;
        }

        $retarr['name'] = $this->name;

        return $retarr;
    }
}
