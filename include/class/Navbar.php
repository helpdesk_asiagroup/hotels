<?php

class Navbar
{
    private $right_elements = array();
    private $left_elements  = array();
    private $raw_html       = array();
    private $brand          = 'Zagranica TV Monitoring';

    private $fixed = true;
    private $type  = 'default';

    public static function loadNavbar() {
        $navbar = new Navbar();
        return $navbar->display(true);
    }

    public function __construct($default = true, $fixed = true)
    {
        $this->fixed = $fixed;
    }

    public function addElement($src, $name, $float_right = false)
    {
        if (!$src || !$name) {
            throw new Exception("Invalid use of method Navbar::add_element. Invalid params");
        }
        
        if ($float_right) {
            $this->right_elements[$src] = $name;
        }
        else {
            $this->left_elements[$src] = $name;
        }
    }

    public function setBrand($brand_name)
    {
        if ($brand_name) {
            $this->brand = $brand_name;
        }
    }

    private function renderNavbar() 
    {
        if ($this->type == 'default') {
            ob_start();
            include(GLOBAL_TEMPLATES . "navbar.tpl");
            $this->raw_html = ob_get_contents();
            ob_end_clean();
        }
    }

    public function display($as_string = false)
    {
        $this->renderNavbar();
        if($as_string) {
            return $this->raw_html;
        }
        
        echo $this->raw_html;
    }
}
