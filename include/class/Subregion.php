<?php

class Subregion extends BaseGetSetClass {

    protected $id = 0;
    protected $parent_region = 0;
    protected $name = ''; 

    private function __construct() {

    }

    public static function load($parent_region) {
        if (!intval($parent_region) && $parent_region != '*') {
            throw new Exception ('Parent region can not be 0! in '.__METHOD__.'()');
        }

        $where = $parent_region == '*' ? '' : "WHERE parent_region = $parent_region";

        $db = db::instance();
        $sql = "SELECT * FROM sub_regions $where ORDER BY name"; 

        $db->query($sql);

        $retval = array();

        while($row = $db->fetch_row()) {
            $self = new self();
            foreach ($row as $prop => $val) {
                $self->set($prop,  $val);
                $retval[$self->get('id')] = $self;
            }
        }

        return $retval;
    }

    public static function addNew($parent_region = 0, $name = '') {
        if (!(int) $parent_region || !$name) {
            throw new Exception ('Pleasen provide parent region id AND name for subregion!'); 
        }

        $self = new self();
        $self->set('parent_region', $parent_region);
        $self->set('name', $name);
        return $self;
    }

    public function save() {
        $db = db::instance();
        $arr = array();
        foreach($this as $prop => $val) {
            $arr[$prop] = $val;
        }

        if ($this->id == 0) { 
            unset($arr['id']);
        }

        $db->replace($arr, 'sub_regions');
    }
}
