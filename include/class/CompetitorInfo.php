<?php

class CompetitorInfo extends BaseGetSetClass 
{
    protected $id = 0;
    protected $hotel_id = 0;
    protected $competitor_id = 0;
    protected $site = 2;
    protected $catalog = 2;
    protected $price = 2;
    protected $rate = 0;
    protected $name_ru = '';
    protected $name_eng = '';

    /**
     * __construct 
     * 
     * @param array $data 
     * @access private
     * @return CompetitorInfo object
     */
    private function __construct($data = array()) 
    {
        if($data) {
            foreach ($data as $prop => $val) {
                $this->set($prop, $val);
            }
        }
    }

    /**
     * load 
     * 
     * @param int $hotel_id 
     * @static
     * @access public
     * @return CompetitorInfo Object
     */
    public static function load($hotel_id = 0)
    {
        $hotel_id = (int) $hotel_id;
        if (!$hotel_id) {
            throw new Exception("Invalid arg! Expected int > 0 in ".__METHOD__."() ");
        }

        $retval = array();

        $db = db::instance('marketing');

        $sql = "SELECT ci.*, c.name_ru, c.name_eng 
                FROM competitors_info ci
                LEFT JOIN competitors c ON ci.competitor_id = c.id
                WHERE ci.hotel_id = $hotel_id";

        $db->query($sql);
        while ($row = $db->fetch_row()) 
        {
            $retval[$row['competitor_id']] = new self($row);
        }

        return $retval;
    }

    public function save() {
        $data = array();
        $db = db::instance();

        foreach ($this as $prop => $val) {
            $data[$prop] = $val;
        }

        if ($data['id'] == 0) unset($data['id']);
        unset($data['name_eng'], $data['name_ru']);
        $db->replace($data, $table = 'competitors_info');
    }
}
