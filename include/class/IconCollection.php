<?php

/**
 * IconCollection
 *
 * @uses BaseGetSetClass
 * @author Alexey Zverev
 */
class IconCollection extends BaseGetSetClass {

    private $rates = array();
    private $sides = array();

    private static $instance;

    private function __construct()
    {
        $db = db::instance();

        /* --------- LOAD RATES ICONS PATHS ------------*/
        $sql = "SELECT * FROM rates_icons";
        $db->query($sql);

        while ($row = $db->fetch_row())
        {
            $block = $row['block_id'];
            $rate  = $row['rate'];

            $this->rates[$block][$rate]['left']  = CAT_ROOT.$row['left'];
            $this->rates[$block][$rate]['right'] = CAT_ROOT.$row['right'];
        }


        /* --------- LOAD SIDES ICONS PATHES ------------*/
        $sql = "SELECT * FROM side_icons";
        $db->query($sql);

        while ($row = $db->fetch_row())
        {
            $id = intval($row['id']);

            $this->sides[$id]['left']  = CAT_ROOT.$row['left'];
            $this->sides[$id]['right'] = CAT_ROOT.$row['right'];
        }
        /*-----------SIDE ICON 99 MEANS NO ICON----------------*/
        $this->sides[99]['left']  = '';
        $this->sides[99]['right'] = '';
    }

    /**
     * init
     *
     * Singleton!
     *
     * @static
     * @access public
     * @return IconCollection obj
     */
    public static function init()
    {
        if(!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Rate
     *
     * @param mixed $block
     * @param mixed $rate
     * @param mixed $position
     * @static
     * @access public
     * @return string
     */
    public static function Rate($block, $rate, $position)
    {
        $rate = trim($rate);
        if (!$position) {
            throw new Exception("Please provide position for icon! in ".__METHOD__."()");
        }

        $iconCollection = IconCollection::init();
        if(isset($iconCollection->rates[$block][$rate][$position])) {
            return $iconCollection->rates[$block][$rate][$position];
        } else {
            throw new Exception("Cant Find rate icon for block $block, rate $rate in ".__METHOD__."()");
        }
    }

    /**
     * Side
     *
     * @param mixed $icon_id
     * @param mixed $position
     * @static
     * @access public
     * @return string
     */
    public static function Side($icon_id, $position)
    {
        if (!$position) {
            throw new Exception("Please provide position for icon! in ".__METHOD__."()");
        }

        $iconCollection = IconCollection::init();
        if (isset($iconCollection->sides[$icon_id][$position])) {
            return $iconCollection->sides[$icon_id][$position];
        } else {
            throw new Exception("Cant Find side icon for icon_id $icon_id in ".__METHOD__."()");
        }
    }
}
