<?php

class engine
{
	public $test_mode = false;
	public $debug_mode = false;
	public $openapi = false;
	public $openapi_userid = 0;
	public $openapi_ip = "";
	public $format = '';
    public $user;
    public $auth;

	public $request_from = "";

	public $input;

	public $cron = false;

	public $vars = array();

	private static $instance;

	private function __construct()
	{

	}

	/**
	 * @return engine
	 */
	public static function instance()
	{
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function init()
	{
		date_default_timezone_set("Asia/Irkutsk");

		setlocale(LC_ALL, 'ru_RU.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251');

		if (!$_SERVER['HTTP_HOST']) {
			$this->cron = true;
		}

        $this->user   = $_SERVER['PHP_AUTH_USER'];
        $this->rules  = include PATH_CONFIG.'/rules.php';

        $groups = include PATH_CONFIG.'/usersGroups.php';
        $this->groups = isset($groups[$this->user]) ? $groups[$this->user] : array();
        $this->groups[] = 'default';

		foreach ($_REQUEST as $key => $value) {
			//$this->clearQuotes($value);
			$this->input[$key] = $value;
		}
    }

    public function requireGroup($groupName)
    {

        if ($this->isUserInGroup('root')) {
            return true;
        }

        if ($this->isUserInGroup($groupName)) {
            return true;
        }

        $this->error('403', "YOU DONT HAVE RIGHTS TO DO IT");
    }

    public function isActionAllowed($action)
    {
        if (empty($this->rules[$action])) {
            throw new Exception("Unknown action");
        }

        return array_intersect($this->rules[$action]['required_groups'], $this->groups);
    }

    public function isUserInGroup($groupName)
    {
        return in_array($groupName, $this->groups) || in_array('root', $this->groups);
    }

	public function clearQuotes($var)
	{
		if (is_array($var)) {
			foreach ($var as $v_id => $v_val) {
				$var[$v_id] = $this->clearQuotes($v_val);
			}
		}
		else {
			return stripslashes($var);
		}
		return $var;
	}

	public function redirect($url, $msg = null)
	{
		header("Location: $url");
		die();
	}

	public function setCookie($name, $value, $expire = 31536000)
	{
		if ($expire) {
			$expire = time() + $expire;
		}
		setcookie($name, $value, $expire);
	}

	public function check($expr, $error_code, $error_message)
	{
		if (!$expr) {
			$this->error($error_code, $error_message);
		}
	}

	public function error($code = 400, $message = "")
	{
		$errors[400] = "400 Bad Request";
		$errors[401] = "401 Unauthorized";
		$errors[403] = "403 Forbidden";
		$errors[404] = "404 Not Found";
		$errors[429] = "429 Too Many Requests";
		$errors[500] = "500 Internal Server Error";
		$errors[304] = "304 Not Modified";

		if (!isset($errors[$code])) {
			$code = 400;
		}

		header("Content-type: text/plain; charset=utf-8");
		header($_SERVER['SERVER_PROTOCOL']." ".$errors[$code], true, $code);
		echo ($message);
		die();
	}

	public function setVar($key, $value)
	{
		$this->vars[$key] = $value;
	}
}
