<?php
  
class View
{
    public $header = '';
    public $footer = '';
    public $navbar = '';
    public $output = '';

    private $encoding = 'utf-8';
    private $content_type = 'text/html';

    private $css = '';
    private $js  = '';

    private $raw_html  = '';
    private $html_head = '';
    private $html_body = '';

    public function __construct($navbar = true, $bootstrap = true, $use_google_font = true)
    {
        if ($bootstrap) {
            $this->addLib('bootstrap');
            $this->addGlobalJs('easing.min.js');
        }
        if ($use_google_font) {
            $this->addExternalCss('http://fonts.googleapis.com/css?family=Noto+Sans:400,700|Roboto:400,700&subset=latin,cyrillic-ext');
        }
        $this->addGlobalCss('main.css');
        $this->addGlobalJs('main.js');
    }

    public function addLib($lib_name) 
    {
        switch($lib_name)
        {
            case 'bootstrap':
                $this->addGlobalCss('bootstrap.min.css', array('media' => 'screen'));
                $this->addLib('jquery');
                $this->addGlobalJs('bootstrap.min.js');
                break;

            case 'jquery':
                $this->addGlobalJs('jquery-1.10.2.min.js');
                break;

            case 'datepicker':
                $this->addGlobalJs('datepicker.js');
                $this->addGlobalCss('datepicker3.css');
                break;

            case 'typeahead':
                $this->addGlobalJs('typeahead.min.js');
                $this->addGlobalCss('typeahead.css');
                break;

            case 'jquery-ui':
                $this->addGlobalCss('jquery-ui.css');
                $this->addGlobalJs('jquery-ui.js');
                break;

            default:
                throw new Exception("Unknown library: $lib_name " . __METHOD__ . '()');
        }
    }
    public function addGlobalJs($filename, $attrs = array())
    {
        $this->js .= '<script type="text/javascript" src="' . PUBLIC_JS . $filename . '" ';

        if ($attrs) {
            foreach ($attrs as $name => $val) {
                $this->js .= $name . '="' .$val. '" ';
            }
        }
        $this->js .= "></script>";
    }

    public function addGlobalCss($filename, $attrs = array()) 
    {
        $this->css .= '<link rel="stylesheet"  href="' . PUBLIC_CSS . $filename .'" ';

        if ($attrs) {
            foreach ($attrs as $name => $val) {
                $this->css .= $name . '="' .$val. '" ';
            }
        }
        $this->css .= ">";
    }

    public function addAppsJs($filename)
    {
        $this->js .= '<script type="text/javascript" src="' . SCRIPT_JS .$filename.'"></script>';
    }

    public function addAppsCss($filename)
    {
        $this->css .= '<link rel="stylesheet" href="' . SCRIPT_CSS .$filename.'"></script>';
    }

    public function addExternalCss($href)
    {
        $this->css .= '<link href="' . $href .'" ';
        $this->css .= 'rel="stylesheet" type="text/css">';
    }

    public function addNavbar($navbar_obj) {
        if ($navbar_obj instanceof Navbar) {
            $this->navbar = $navbar_obj->display(true);
        }
        else {
            throw new Exception ("Invalid argument for method View::add_navbar. Expected Navbar object");
        }
    }

    public function addJsCode($string) {
        $this->js .= '<script type="text/javascript">' . $string . '</script>';
    }

    public function setTemplate($filename, $vars = array())
    {
        if ($vars) {
            extract($vars);
        }

        ob_start();
        include(SCRIPT_TEMPLATE_DIR . "$filename.tpl");
        $this->output .= ob_get_contents();
        ob_end_clean();
    }

    public function renderPage()
    {
        $this->navbar = $this->navbar ? $this->navbar : Navbar::loadNavbar();

        //-------------------------------Rendering <head> -------------------------------------------------------
        $this->html_head = "<head>" . $this->css . $this->js . "</head>";

        //-------------------------------Rendering <body> -------------------------------------------------------
        $this->html_body  = "<body>" . $this->navbar . $this->header . $this->output . $this->footer . "</body>";

        //-------------------------------Rendering full html5 document-------------------------------------------
        $this->raw_html = "<!DOCTYPE html><html>" . $this->html_head . $this->html_body.'</html>';
    }

    public function display() 
    {
        $this->renderPage();
        header("Content-Type: $this->content_type; charset=$this->encoding"); 
        echo $this->raw_html;
    }

    public function display_ajax() 
    {
        header("Content-Type: $this->content_type; charset=$this->encoding"); 
        echo $this->output;
    }
}
