<?php

class Corrections extends BaseGetSetClass
{
    public $id = 0;
    public $hotel_id = 0;
    public $type = 0;
    public $year = 0;
    public $text = '';
    public $reason = '';
    public $status = 0;

    public static $instances = array();

    public static function load($hotel_id, $type, $year)
    {
        if (empty(self::$instances[$hotel_id][$type][$year])) {
            $db = db::instance();
            $sql = "SELECT * FROM corrections
                    WHERE hotel_id = $hotel_id
                    AND type = $type
                    AND year = $year";
            $db->query($sql);
            $row = $db->fetch_row();
            $retObj = $row ? new self($row) : self::getEmpty();
            $retObj->hotel_id = $hotel_id;
            $retObj->year = $year;
            $retObj->type = $type;
            self::$instances[$hotel_id][$type][$year] = $retObj;
        }

        return $retObj;
    }

    protected function __construct($row)
    {
        if ($row) {
            foreach ($row as $prop => $val) {
                if (!$this->set($prop, $val)) {
                    $error = "Uknown proprty $prop in ".__METHOD__."()";
                    //throw new Exception ($error);
                }
            }
        }
    }

    public static function getEmpty()
    {
        return new self($row = false);
    }

    public function getStatusText()
    {
        switch($this->status) {
            case 0:
                return $this->text ? "Ожидает рассмотрения" : "Правок нет";
            case 1:
                return "Правки приняты";
            case 2:
                return "Правки отклонены";
            case 3:
                return "Проверено, правок не требует";
        }
    }

    public function save()
    {
        $cols = array();
        foreach ($this as $prop => $val) {
            $cols[$prop] = $val;
        }

        $db = db::instance();
        $db->replace($cols, $table = 'corrections');
        return $this;
    }
}
