<?php
class Luxury2015 extends BaseGetSetClass {

    public $id = 0;
    public $hotel_id = 0;
    public $order = 0;
    public $block = 0;
    public $area  = 0;
    public $rate = '0';
    public $finished = 0;
    public $ap_distance = '';
    public $base_info = '';
    public $signature_info = '';
    public $site_services_left = '';
    public $site_services_right = '';
    public $spa_info = '';
    public $other_services_left = '';
    public $other_services_right = '';
    public $pool_top = '';
    public $pool_left = '';
    public $pool_right = '';
    public $business_services_left = '';
    public $business_services_right = '';
    public $children_services_left = '';
    public $children_services_right = '';
    public $restaurants = '';
    public $restaurants_signs = '';
    public $sports_ents_left = '';
    public $sports_ents_right = '';
    public $beach_top = '';
    public $beach_left = '';
    public $beach_right = '';
    public $cards = '';
    public $deposit = '';
    public $comments_sayama = '';
    public $rooms_table = '';
    public $room_table_last_row_2 = '';
    public $room_table_last_row_1 = '';
    public $room_descr_title_1 = '';
    public $room_descr_title_2 = '';
    public $room_descr_title_3 = '';
    public $room_descr_title_4 = '';
    public $room_descr_title_5 = '';
    public $room_descr_title_6 = '';
    public $room_descr_title_7 = '';
    public $room_descr_title_8 = '';
    public $room_descr_title_9 = '';
    public $room_descr_title_10 = '';
    public $room_descr_title_11 = '';
    public $room_descr_title_12 = '';
    public $room_descr_title_13 = '';
    public $room_descr_title_14 = '';
    public $room_descr_1 = '';
    public $room_descr_2 = '';
    public $room_descr_3 = '';
    public $room_descr_4 = '';
    public $room_descr_5 = '';
    public $room_descr_6 = '';
    public $room_descr_7 = '';
    public $room_descr_8 = '';
    public $room_descr_9 = '';
    public $room_descr_10 = '';
    public $room_descr_11 = '';
    public $room_descr_12 = '';
    public $room_descr_13 = '';
    public $room_descr_14 = '';
    public $awards = '';
	public $exported = '';
    public $changed_after_export = 0;
    public $changed_page = 0;
    public $changed_position = 0;
	public $notes = '';
    public $ques = '';
    public $approved = 0;

    protected $year = '';

    protected function __construct()
    {

    }

    public static function load($hotel_id, $year)
    {
        $self = new static();

        $db = db::instance();
        $sql = "SELECT * FROM luxury_$year WHERE hotel_id = $hotel_id";
        //echo $sql;
        $db->query($sql);

        if ($row = $db->fetch_row()) {
            foreach ($row as $prop => $val) {
                $self->set($prop, $val);
            }
        } else {
            $self->set('hotel_id', $hotel_id);
        }
        $self->set('year' ,$year);
        //print_r( $self);
        return $self;
    }

    /**
     * Список полей которые тримаем
     */
    protected function getFilterFields()
    {
    	return [
    		'site_services_left',
    		'site_services_right',
    		'pool_left',
    		'pool_right',
    		'business_services_left',
    		'business_services_right',
    		'children_services_left',
    		'children_services_right',
    		'sports_ents_left',
    		'sports_ents_right',
    		'beach_left',
    		'beach_right',
    	];
    }
    
    /**
     * Тримаем и убираем знак # в конце
     *
     * @param string $value
     * @return string
     */
    protected function filterField($value)
    {
    	$result = [];
    	 
    	$tmp = explode('#', $value);
    	foreach ($tmp as $item) {
    		if (trim($item) != '') {
    			$result[] = trim($item);
    		}
    	}
    	 
    	return implode('#', $result);
    }
    
    public function save()
    {
        if (!$this->hotel_id) {
            throw new exception("Hotel id = 0, I cant save this bullshit! ".__METHOD__."()");
        }
        $data = array();
        $db = db::instance();
        foreach ($this as $prop => $val) {
        if (in_array($prop, $this->getFilterFields())) {
        		$data[$prop] = $this->filterField($val);
        	} else {
            	$data[$prop] = $val;
        	}
        }
        unset($data['year']);
        
        if ($this->finished == 1) {
        	$data['changed_after_export'] = 1;
        }
        
        $db->replace($data, $table = 'luxury_' . $this->year);
    }

    /**
     * getExportData
     *
     * Return array formed data for export csv for InDesign
     *
     * @access public
     * @return array()
     */
    public function getExportData() {
        $position = 'left';

        $retarr = array();
        $patterns = array("/\s+/", "/#+/", "/[#\s]+$/");
        $replaces = array(" "    , "#"   , "");

        $region_obj = Region::load($this->block);
        $subregions = $region_obj->loadSubregions()->get('subregions');

        $retarr['hotel_id']          = $this->hotel_id;
        $retarr['area']              = $this->area ? $subregions[$this->area]->get('name'):'';
        $retarr['basic_info']        = preg_replace("/[\s#]+/", " ", $this->base_info); //Remove tabs
        $retarr['sign_info']         = preg_replace("/[\s#]+/", " ", $this->signature_info); //Remove tabs
        $retarr['site_services']     = preg_replace($patterns, $replaces, $this->site_services_left."#".$this->site_services_right);
        $retarr['spa_info']          = preg_replace("/[\s#]+/", " ", $this->spa_info); //Remove tabs
        $retarr['other_services']    = preg_replace($patterns, $replaces, $this->other_services_left.'#'.$this->other_services_right);
        $retarr['pool_head']         = preg_replace($patterns, $replaces, $this->pool_top);
        $retarr['pool_body']         = preg_replace($patterns, $replaces, $this->pool_left."#". $this->pool_right);
        $retarr['children_services'] = preg_replace($patterns, $replaces, $this->children_services_left.'#'.$this->children_services_right);
        $retarr['table_restaurants'] = preg_replace($patterns, $replaces, $this->restaurants);
        $retarr['restaurants_signs'] = preg_replace("/[\s#]+/", " ", $this->restaurants_signs); //Remove tabs
        $retarr['sports_ents']       = preg_replace($patterns, $replaces, $this->sports_ents_left."#".$this->sports_ents_right);
        $retarr['beach_head']        = preg_replace($patterns, $replaces, $this->beach_top);
        $retarr['beach_body']        = preg_replace($patterns, $replaces, $this->beach_left.'#'.$this->beach_right);
        $retarr['cards']             = preg_replace($patterns, $replaces, $this->cards);
        $retarr['deposit']           = preg_replace("/[\s#]+/", " ", $this->deposit);
        $retarr['comments_sayama']   = preg_replace("/[\s#]+/", " ", $this->comments_sayama);

        $rooms_table = json_decode($this->rooms_table, true);
        $rooms_table_tmp = array();
        foreach($rooms_table[0] as &$cell) {
            $cell = str_replace("\n", "%", $cell);
        }

        foreach($rooms_table as $i => $row) {
            if ($row[0] || $i == 0) { // If not empty row
                $rooms_table_tmp[] = preg_replace("/[\s#]+/", " ", implode(';', $row));
            }
        }
        $columns = count($rooms_table[0]);
        $last_row_1 = array_fill(0, $columns, "");
        $last_row_2 = array_fill(0, $columns, "");
        $last_row_1[0] = preg_replace("/[\s#]+/", " ", $this->room_table_last_row_1);
        $last_row_2[0] = preg_replace("/[\s#]+/", " ", $this->room_table_last_row_2);

        $rooms_table_tmp[] = implode(';', $last_row_1);
        $rooms_table_tmp[] = implode(';', $last_row_2);

        $retarr['rooms_table'] = implode('#', $rooms_table_tmp);

        // Place empty rooms descriptions blocks at the end
        $j = 14; //INdex for filling empty from end of array
        $k = 1;  //Index for filling NOT empty from start of array
        for ($i = 1; $i < 15; ++$i) {
            $title = str_replace("#", " ", $this->get("room_descr_title_$i"));
            $descr = preg_replace("/[\s#]+/", " ", $this->get("room_descr_$i"));

            if (!$title || trim($title) == '!empty!' || !$descr || trim($descr) == '!empty!') {
                $index = $j--;
            } else {
                $index = $k++;
            }

            $pre_retarr["room_descr_title_$index"] = $title;
            $pre_retarr["room_descr_$index"] = $descr;
        }

        for ($i = 1; $i < 15; ++$i) {
            $retarr["room_descr_title_$i"] = $pre_retarr["room_descr_title_$i"];
            $retarr["room_descr_$i"] = $pre_retarr["room_descr_$i"];
        }

        $retarr['awards']      = preg_replace("/[\s]+/", " ", $this->awards);
        $retarr['ap_distance'] = preg_replace("/[\s#]+/", " ", $this->ap_distance);
        return $retarr;
    }
}
