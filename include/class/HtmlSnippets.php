<?php
/**
 *       Class for generating routine HTML code such as select, inputs, tables etc
 *          from Arrays or Objects.
 *
 *       REMEMBER! All methods must be static public! Thank You!
 *
 *
 *
 */

class HtmlSnippets
{
    public static function select($inputArray, $name = '', $class = 'form-control', $selectedKey = '', $keyAsValueAttr = true, $disabled = false)
    {
        $disabled = $disabled ? 'disabled' : '';
        $retString = '<select name="' . $name . '" class="' . $class . '"' . $disabled .'>';

        foreach ($inputArray as $k => $v) {
            $selected = $selectedKey == $k ? 'selected="selected"' : '';

            if ($keyAsValueAttr) {
                $value = $k;
                $content = $v;
            } else {
                $value = $v;
                $content = $k;
            }

            $retString .= '<option value="' . $value . '" ' . $selected .'> ' . $content . '</option>';
        }

        return $retString . "</select>";
    }

    /**
     * table
     *
     * @param int $cols
     * @param int $rows
     * @param array $data Multidemensial array. Structure:
     *                    array('row_id' => array('col_1','col_2'....'col_n'))
     *                    Zero indexed row interpreted as table header
     * @static
     * @access public
     * @return string
     */
    public static function table($cols, $rows, $data) {
        if (!$cols = intval($cols) || !$row = intval($rows) || !is_array($data)) {

        }
    }
}
