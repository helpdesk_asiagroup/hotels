<?php
class Mice2015 extends BaseGetSetClass {

    public $id = 0;
    public $hotel_id = 0;
    public $order = 0;
    public $block = 0;
    public $area  = 0;
    public $rate = '0';
    public $finished = 0;
    public $base_info = '';
    public $rooms = '';
    public $ap_distance = '';
    public $room_services_left = '';
    public $room_services_right = '';
    public $site_services_left = '';
    public $site_services_right = '';
    public $other_services_left = '';
    public $other_services_right = '';
    public $pool_top = '';
    public $pool_left = '';
    public $pool_right = '';
    public $business_services_left = '';
    public $business_services_right = '';
    public $children_services_left = '';
    public $children_services_right = '';
    public $restaurants = '';
    public $sports_ents_left = '';
    public $sports_ents_right = '';
    public $beach_top = '';
    public $beach_left = '';
    public $beach_right = '';
    public $cards = '';
    public $deposit = '';
    public $comments_sayama = '';
    public $mice_rooms = '';
    public $icon_ppl = 99;
    public $icon_party = 99;
	public $exported = '';
    public $changed_after_export = 0;
    public $changed_page = 0;
    public $changed_position = 0;
	public $notes = '';
    public $ques = '';
    public $approved = 0;

    protected $year = '';

    protected function __construct()
    {

    }

    public static function load($hotel_id, $year)
    {
        $self = new static();

        $db = db::instance();
        $sql = "SELECT * FROM mice_$year WHERE hotel_id = $hotel_id";
        $db->query($sql);

        if ($row = $db->fetch_row()) {
            foreach ($row as $prop => $val) {
                $self->set($prop, $val);
            }
        } else {
            $self->set('hotel_id', $hotel_id);
        }
        $self->set('year' ,$year);

        return $self;
    }

    public function save()
    {
        if (!$this->hotel_id) {
            throw new exception("Hotel id = 0, I cant save this bullshit! ".__METHOD__."()");
        }
        $data = array();
        $db = db::instance();
        foreach ($this as $prop => $val) {
        	if (in_array($prop, $this->getFilterFields())) {
        		$data[$prop] = $this->filterField($val);
        	} elseif (in_array($prop, $this->getFilterFieldsArray())) {
        		$data[$prop] = $this->filterArrayField($val);
        	} else {
            	$data[$prop] = $val;
        	}
        }
        unset($data['year']);
        
        if ($this->finished == 1) {
        	$data['changed_after_export'] = 1;
        }
        
        //$db->replace($data, $table = 'mice_' . $this->year);
        $db->update($data, 'mice_' . $this->year, 'hotel_id = ' . $this->hotel_id);
    }

    /**
     * Список полей которые тримаем
     */
    protected function getFilterFields()
    {
    	return [
    		'room_services_left',
    		'room_services_right',
    		'site_services_left',
    		'site_services_right',
    		'other_services_left',
    		'other_services_right',
    		'pool_left',
    		'pool_right',
    		'business_services_left',
    		'business_services_right',
    		'children_services_left',
    		'children_services_right',
    		'sports_ents_left',
    		'sports_ents_right',
    		'beach_left',
    		'beach_right',
    	];
    }
    
    protected function getFilterFieldsArray()
    {
    	return [
    		'mice_rooms',
    		'rooms',
    		'restaurants'
    	];
    }
    
    /**
     * Тримаем и убираем знак # в конце
     * 
     * @param string $value
     * @return string
     */
    protected function filterField($value)
    {
    	$result = [];
    	
    	$tmp = explode('#', $value);
    	foreach ($tmp as $item) {
    		if (trim($item) != '') {
    			$result[] = trim($item); 
    		}
    	}
    	
    	return implode('#', $result);
    }
    
    /**
     * Фильтрация 2-у мерного массива
     * 
     * @param string $value
     * @return string
     */
    protected function filterArrayField($value)
    {
    	$value = str_replace(["\n", "\t"], '', $value);
    	
    	$result = [];
    	
    	$tmp = explode('#', $value);
    	
    	foreach ($tmp as $row) {
    		$rowTmp = explode(';', $row);
    		$rowResult = [];
    		foreach ($rowTmp as $col) {
    			if (trim($col) != '') {
    				$rowResult[] = trim($col);
    			}
    		}
    		
    		if (!empty($rowResult)) {
    			$result[] = implode(';', $rowResult);
    		}
    		
    	}
    	
    	return implode('#', $result);
    }
    
    /**
     * getExportData
     *
     * Return array formed data for export csv for InDesign
     *
     * @access public
     * @return array()
     */
    public function getExportData() {
        $position = 'left';

        $retarr = array();
        $patterns = array("/\s+/", "/#+/", "/[#\s]+$/");
        $replaces = array(" "    , "#"   , "");

        $region_obj = Region::load($this->block);
        $subregions = $region_obj->loadSubregions()->get('subregions');

        $retarr['hotel_id']          = $this->hotel_id;
        $retarr['area']              = $this->area ? $subregions[$this->area]->get('name'):'';
        $retarr['@icon_rate']        = CAT_ROOT . '\INPUT\STARS\mice\\' . strtoupper($this->rate) . '.eps';
        $retarr['basic_info']        = preg_replace("/[\s#]+/", " ", $this->base_info); //Remove tabs
        $retarr['table_rooms']       = preg_replace($patterns, $replaces, $this->rooms);
        $retarr['room_services']     = preg_replace($patterns, $replaces, $this->room_services_left."#".$this->room_services_right);
        $retarr['site_services']     = preg_replace($patterns, $replaces, $this->site_services_left."#".$this->site_services_right);
        $retarr['other_services']    = preg_replace($patterns, $replaces, $this->other_services_left.'#'.$this->other_services_right);
        $retarr['buiseness_services'] = preg_replace($patterns, $replaces, $this->other_services_left.'#'.$this->other_services_right);
        $retarr['pool_head']         = preg_replace($patterns, $replaces, $this->pool_top);
        $retarr['pool_body']         = preg_replace($patterns, $replaces, $this->pool_left."#". $this->pool_right);
        $retarr['children_services'] = preg_replace($patterns, $replaces, $this->children_services_left.'#'.$this->children_services_right);
        $retarr['table_restaurants'] = preg_replace($patterns, $replaces, $this->restaurants);
        $retarr['business_services'] = preg_replace($patterns, $replaces, $this->business_services_left.'#'.$this->business_services_right);
        $retarr['sports_ents']       = preg_replace($patterns, $replaces, $this->sports_ents_left."#".$this->sports_ents_right);
        $retarr['beach_head']        = preg_replace($patterns, $replaces, $this->beach_top);
        $retarr['beach_body']        = preg_replace($patterns, $replaces, $this->beach_left.'#'.$this->beach_right);
        $retarr['cards']             = preg_replace($patterns, $replaces, $this->cards);
        $retarr['deposit']           = preg_replace("/[\s#]+/", " ", $this->deposit);
        $retarr['comments_sayama']   = preg_replace("/[\s#]+/", " ", $this->comments_sayama);
        $retarr['mice_rooms']        = preg_replace($patterns, $replaces, $this->mice_rooms);
        $retarr['ap_distance']       = preg_replace($patterns, $replaces, $this->ap_distance);
        $retarr['@icon_ppl']         = IconCollection::Side($this->icon_ppl,  'left');
        $retarr['@icon_party']       = IconCollection::Side($this->icon_party,'left');

        return $retarr;
    }
}
