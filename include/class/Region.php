<?php
class Region extends BaseGetSetClass {

    protected $id = 0;
    protected $name = '';

    public $subregions = '';

    private function __construct($data = array()) 
    {
        if($data) {
            foreach ($data as $prop => $val) {
                $this->set($prop, $val);
            }
        }    
    }

    public static function load($id = '*')
    {
        $id = $id == '*' ? $id : intval($id);
        if (!$id) {
            throw new Exception("Invalid arg! Expected int > 0 or '*' in ".__METHOD__."() ");
        }

        $db = db::instance();
        $retval = array();

        $where = $id == '*' ? '' : "WHERE id = $id";

        $sql = "SELECT * FROM regions $where";
        $db->query($sql);
        
        while ($row = $db->fetch_row()) {
            $retval[] = new self($row);
        }

        return count($retval) == 1 ? $retval[0] : $retval;
    }

    public function loadSubregions() {
        $this->subregions = Subregion::load($this->id); 
        return $this;
    }
    
    public function addSubregion($name = false) {
        if (!$name) {
            throw new Exception ('Please provide subregion name! in '.__METHOD__.'()');
        }
        $subr = Subregion::addNew($this->id, $name);
        $subr->save();
        $this->loadSubregions();
    }
    
    public function getName()
    {
    	return $this->name;
    }
}
