<?php
class BaseGetSetClass {
    public function set($prop, $val)
    {
        if (!isset($this->$prop)) {
            return false;
        }
        else {
            $this->$prop = $val;
            return $this;
        }
    }

    public function get($prop)
    {
        if (!isset($this->$prop)) {
            return false;
        }
        else {
            return $this->$prop;
        }
    }
}
