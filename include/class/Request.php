<?php

/**
 * class Request
 *
 * Класс для получения переданных скрипту переменных посредством методов GET или POST
 */
class Request
{
	/**
	 * Возвращает переменную из $_REQUEST; если она отсутствует, возвращается $default
	 *
	 * @param null $var_name имя переменной
	 * @param null $default значение по умолчанию
	 * @return mixed
	 */
	public static function getVar($var_name = null, $default = null) {
		if (!$var_name)
			return $default;
		$engine = engine::instance();
		$var = isset($engine->input[$var_name]) ? $engine->input[$var_name] : $default;
		return $var;
	}

	/**
	 * Возвращает целочисленную переменную из массива $_REQUEST; если она отсутствует или не целочисленная,
	 * возвращается $default
	 *
	 * @param null $var_name Имя переменной
	 * @param int $default Значение по умолчанию
	 * @return int
	 */
	public static function getInt($var_name = null, $default = 0) {
		if (!$var_name)
			return $default;
		$engine = engine::instance();
		$var = isset($engine->input[$var_name]) && preg_match("#^[0-9]+$#", $engine->input[$var_name]) ? (int) $engine->input[$var_name] : $default;
		return $var;
	}

	/**
	 * Возвращает вещественную переменную из массива $_REQUEST; если она отсутствует или не вещественная,
	 * возвращается $default
	 *
	 * @param null $var_name Имя переменной
	 * @param int $default Значение по умолчанию
	 * @return int
	 */
	public static function getFloat($var_name = null, $default = 0) {
		if (!$var_name)
			return $default;
		$engine = engine::instance();
		$var = isset($engine->input[$var_name]) && is_numeric($engine->input[$var_name]) ? (float) $engine->input[$var_name] : $default;
		return $var;
	}

	/**
	 * Возвращает переменную типа date в указанном формате из массива $_REQUEST; если она отсутствует или
	 * не соответствует формату даты, возвращается $default
	 *
	 * @param null $var_name Имя переменной
	 * @param string $default Значение по умолчанию
	 * @param string $format Формат даты. Поддерживаются Y,m,d и всякие - и .
	 * @param string $return_format Формат, в котором возвратить переменную
	 * @return int
	 */
	public static function getDate($var_name = null, $default = "0000-00-00", $format = MYSQLDATE, $return_format = MYSQLDATE) {
		if (!$var_name)
			return $default;
		$engine = engine::instance();
		$replaces = array(
			'Y' => "[0-9]{4}",
			'm' => "[0-9]{2}",
			'd' => "[0-9]{2}",
			'H' => "[0-9]{2}",
			'i' => "[0-9]{2}",
			's' => "[0-9]{2}",
			"." => "\."
		);
		$pattern = str_replace(array_keys($replaces), array_values($replaces), "#^{$format}$#");

		if (isset($engine->input[$var_name]) && preg_match($pattern, $engine->input[$var_name])) {
			$date_var = date_create_from_format($format, $engine->input[$var_name]);
			$var = $date_var->format($return_format);
		} else {
			$var = $default;
		}
		return $var;
	}

	/**
	 * Возвращает переменную из массива $_FILES. Если она отсутствует, возвращает $default
	 *
	 * @param null $var_name Имя переменной
	 * @param bool $default Значение по умолчанию
	 * @return bool|array
	 */
	public static function getFile($var_name = null, $default = false) {
		if (!$var_name)
			return $default;
		$var = isset($_FILES[$var_name]) ? $_FILES[$var_name] : $default;
		return $var;
	}

	/**
	 * Возвращает переменную типа Array. Если она отсутствует или не типа Array, то возвращается $default
	 *
	 * @param null $var_name Имя переменной
	 * @param array $default Значение по умолчанию
	 * @return array
	 */
	public static function getArray($var_name = null, $default = array()) {
		if (!$var_name)
			return $default;
		$engine = engine::instance();
		$var = isset($engine->input[$var_name]) && is_array($engine->input[$var_name]) ? $engine->input[$var_name] : $default;
		return $var;
	}
}
