<?php
class Catalogue2015 extends BaseGetSetClass {

    public $id = 0;
    public $hotel_id = 0;
    public $order = 0;
    public $finished = 0;
    public $block = 0;
    public $area = 0;
    public $rate = '0';
    public $site= '';
    public $ceo = '';
    public $rooms = '';
    public $base_info = '';
    public $room_services_left = '';
    public $room_services_right = '';
    public $site_services_left = '';
    public $site_services_right = '';
    public $pool_top = '';
    public $pool_left = '';
    public $pool_right = '';
    public $business_services = '';
    public $children_services_left = '';
    public $children_services_right = '';
    public $restaurants = '';
    public $sports_ents_left = '';
    public $sports_ents_right = '';
    public $beach_top = '';
    public $beach_left = '';
    public $beach_right = '';
    public $cards = '';
    public $deposit = '';
    public $comments_sayama = '';
    public $icon_video = 99;
    public $icon_0  = 99;
    public $icon_1  = 99;
    public $icon_2  = 99;
    public $icon_3  = 99;
    public $icon_4  = 99;
    public $icon_5  = 99;
    public $icon_6  = 99;
    public $icon_7  = 99;
    public $icon_8  = 99;
    public $icon_9  = 99;
    public $icon_10 = 99;
    public $icon_11 = 99;
    public $icon_1_text = '';
    public $icon_3_text = '';
    public $icon_4_text = '';
    public $check_in  = '';
    public $check_out = '';
    public $exported = 0;
    public $changed_after_export = 0;
    public $changed_page = 0;
    public $changed_position = 0;
    public $notes = '';
    public $ques = '';
    public $approved = 0;
    public $page = 0;
    public $map = ' ';
    public $map_index = ' ';
    public $image_1 = ' ';
    public $image_2 = ' ';
    public $image_3 = ' ';
    public $image_4 = ' ';
    public $image_5 = ' ';
    public $image_6 = ' ';
    public $image_2_txt = ' ';
    public $image_3_txt = ' ';
    public $image_4_txt = ' ';
    public $image_5_txt = ' ';
    public $image_6_txt = ' ';

    protected $year = '';

    private function __construct()
    {

    }

    public static function load($hotel_id, $year)
    {
        $self = new static();

        $db = db::instance();
        $sql = "SELECT * FROM catalogue_$year WHERE hotel_id = $hotel_id";
        $db->query($sql);

        if ($row = $db->fetch_row()) {
            foreach ($row as $prop => $val) {
                $self->set($prop, $val);
            }
        } else {
            $self->set('hotel_id', $hotel_id);
        }
        $self->year = $year;

        return $self;
    }

    /**
     * Список полей которые тримаем
     */
    protected function getFilterFields()
    {
    	return [
    			'room_services_left',
    			'room_services_right',
    			'site_services_left',
    			'site_services_right',
    			'pool_left',
    			'pool_right', 
    			'children_services_left',
    			'children_services_right', 
    			'sports_ents_left',
    			'sports_ents_right',
    			'beach_left',
    			'beach_right',
    	];
    }
    
    /**
     * Тримаем и убираем знак # в конце
     *
     * @param string $value
     * @return string
     */
    protected function filterField($value)
    {
    	$result = [];
    	 
    	$tmp = explode('#', $value);
    	foreach ($tmp as $item) {
    		if (trim($item) != '') {
    			$result[] = trim($item);
    		}
    	}
    	 
    	return implode('#', $result);
    }
    
    protected function getFilterFieldsArray()
    {
    	return [
    		'bussiness_services',
    		'rooms',
    		'restaurants'
    	];
    }
    
    /**
     * Фильтрация 2-у мерного массива
     *
     * @param string $value
     * @return string
     */
    protected function filterArrayField($value)
    {
    	$value = str_replace(["\n", "\t"], '', $value);
    	 
    	$result = [];
    	 
    	$tmp = explode('#', $value);
    	 
    	foreach ($tmp as $row) {
    		$rowTmp = explode(';', $row);
    		$rowResult = [];
    		foreach ($rowTmp as $col) {
    			if (trim($col) != '') {
    				$rowResult[] = trim($col);
    			}
    		}
    
    		if (!empty($rowResult)) {
    			$result[] = implode(';', $rowResult);
    		}
    
    	}
    	 
    	return implode('#', $result);
    }
    
    public function save()
    {
        if (!$this->hotel_id) {
            throw new exception("Hotel id = 0, I cant save this bullshit! ".__METHOD__."()");
        }
        $data = array();
        $db = db::instance();
        
        foreach ($this as $prop => $val) {
        	if ($val == '!empty!') {
        		$val = '';
        	}
        	if (in_array($prop, $this->getFilterFields())) {
        		$data[$prop] = $this->filterField($val);
        	} elseif (in_array($prop, $this->getFilterFieldsArray())) {
        		$data[$prop] = $this->filterArrayField($val);
        	} else {
            	$data[$prop] = $val;
        	}
        }
        
        if ($this->finished == 1) {
        	$data['changed_after_export'] = 1;
        }
        
        unset($data['year']);
        //$db->replace($data, $table = 'catalogue_' . $this->year);
        $db->update($data, 'catalogue_' . $this->year, 'hotel_id = ' . $this->hotel_id);
    }

    /**
     * getExportData
     *
     * Return array formed data for export csv for InDesign
     *
     * @access public
     * @return array()
     */
    public function getExportData($position) {
        switch (trim($position)) {
            case 'l':
            case 'left':
                $position = 'left';
                break;

            case 'r':
            case 'right':
                $position = 'right';
                break;

            default:
                throw new Exception ("Expected position! You provide: '$position' in ".__METHOD__."()");
                break;
        }

        $retarr = array();
        $patterns = array("/\s+/", "/#+/", "/[#\s]+$/");
        $replaces = array(" "    , "#"   , "");

        $region_obj = Region::load($this->block);
        $subregions = $region_obj->loadSubregions()->get('subregions');

        $retarr['hotel_id']          = $this->hotel_id;
        $area = isset($subregions[$this->area]) ? $subregions[$this->area]->get('name') : '';
        $retarr['page']          	 = $this->page;
        $retarr['region']          	 = $region_obj->getName();
        $retarr['area']              = $area;
        $retarr['map']               = $this->map;
        $retarr['map_index']         = $this->map_index;
        $retarr['image_1']           = $this->image_1;
        $retarr['image_2']           = $this->image_2;
        $retarr['image_3']           = $this->image_3;
        $retarr['image_4']           = $this->image_4;
        $retarr['image_5']           = $this->image_5;
        $retarr['image_6']           = $this->image_6;
        $retarr['image_2_txt']       = $this->image_2_txt;
        $retarr['image_3_txt']       = $this->image_3_txt;
        $retarr['image_4_txt']       = $this->image_4_txt;
        $retarr['image_5_txt']       = $this->image_5_txt;
        $retarr['image_6_txt']       = $this->image_6_txt;
        $retarr['web_site']          = $this->site;
        $retarr['basic_info']        = preg_replace("/[\s#]+/", " ", $this->base_info); //Remove tabs
        $retarr['table_rooms']       = preg_replace($patterns, $replaces, $this->rooms);
        $retarr['room_services']     = preg_replace($patterns, $replaces, $this->room_services_left."#".$this->room_services_right);
        $retarr['site_services']     = preg_replace($patterns, $replaces, $this->site_services_left."#".$this->site_services_right);
        $retarr['pool_head']         = preg_replace($patterns, $replaces, $this->pool_top);
        $retarr['pool_body']         = preg_replace($patterns, $replaces, $this->pool_left."#". $this->pool_right);
        $retarr['table_mice']        = preg_replace($patterns, $replaces, $this->business_services);
        $retarr['children_services'] = preg_replace($patterns, $replaces, $this->children_services_left.'#'.$this->children_services_right);
        $retarr['table_restaurants'] = preg_replace($patterns, $replaces, $this->restaurants);
        $retarr['sports_ents']       = preg_replace($patterns, $replaces, $this->sports_ents_left."#".$this->sports_ents_right);
        $retarr['beach_head']        = preg_replace($patterns, $replaces, $this->beach_top);
        $retarr['beach_body']        = preg_replace($patterns, $replaces, $this->beach_left.'#'.$this->beach_right);
        $retarr['cards']             = preg_replace($patterns, $replaces, $this->cards);
        $retarr['deposit']           = preg_replace("/[\s#]+/", " ", $this->deposit);
        $retarr['ceo']               = $this->ceo;
        $retarr['comments_sayama']   = preg_replace("/[\s#]+/", " ", $this->comments_sayama);

        /*
        $retarr['@icon_video']       = IconCollection::Side($this->icon_video, $position);
        $retarr['@icon_rate']        = $this->rate ? IconCollection::Rate($this->block, strtolower($this->rate), $position) : '';
        $retarr['@icon_check']       = IconCollection::Side($this->icon_0,  $position);
        $retarr['@icon_location']    = IconCollection::Side($this->icon_1,  $position);
        $retarr['@icon_center']      = IconCollection::Side($this->icon_2,  $position);
        $retarr['@icon_build_at']    = IconCollection::Side($this->icon_3,  $position);
        $retarr['@icon_refresh']     = IconCollection::Side($this->icon_4,  $position);
        $retarr['@icon_vip']         = IconCollection::Side($this->icon_5,  $position);
        $retarr['@icon_lowcost']     = IconCollection::Side($this->icon_6,  $position);
        $retarr['@icon_mice']        = IconCollection::Side($this->icon_7,  $position);
        $retarr['@icon_honey']       = IconCollection::Side($this->icon_8,  $position);
        $retarr['@icon_family']      = IconCollection::Side($this->icon_9,  $position);
        $retarr['@icon_active']      = IconCollection::Side($this->icon_10, $position);
        $retarr['@icon_relax']       = IconCollection::Side($this->icon_11, $position);
        */
        $retarr['icon_video']       = IconCollection::Side($this->icon_video, 'left');
        $retarr['icon_rate']        = $this->rate ? IconCollection::Rate($this->block, strtolower($this->rate), 'left') : '';
        $retarr['icon_check']       = IconCollection::Side($this->icon_0,  'left');
        $retarr['icon_location']    = IconCollection::Side($this->icon_1,  'left');
        $retarr['icon_center']      = IconCollection::Side($this->icon_2,  'left');
        $retarr['icon_build_at']    = IconCollection::Side($this->icon_3,  'left');
        $retarr['icon_refresh']     = IconCollection::Side($this->icon_4,  'left');
        $retarr['icon_vip']         = IconCollection::Side($this->icon_5,  'left');
        $retarr['icon_lowcost']     = IconCollection::Side($this->icon_6,  'left');
        $retarr['icon_mice']        = IconCollection::Side($this->icon_7,  'left');
        $retarr['icon_honey']       = IconCollection::Side($this->icon_8,  'left');
        $retarr['icon_family']      = IconCollection::Side($this->icon_9,  'left');
        $retarr['icon_active']      = IconCollection::Side($this->icon_10, 'left');
        $retarr['icon_relax']       = IconCollection::Side($this->icon_11, 'left');
        
        $retarr['icon_video_right']       = IconCollection::Side($this->icon_video, 'left');
        $retarr['icon_rate_right']        = $this->rate ? IconCollection::Rate($this->block, strtolower($this->rate), 'left') : '';
        $retarr['icon_check_right']       = IconCollection::Side($this->icon_0,  'left');
        $retarr['icon_location_right']    = IconCollection::Side($this->icon_1,  'left');
        $retarr['icon_center_right']      = IconCollection::Side($this->icon_2,  'left');
        $retarr['icon_build_at_right']    = IconCollection::Side($this->icon_3,  'left');
        $retarr['icon_refresh_right']     = IconCollection::Side($this->icon_4,  'left');
        $retarr['icon_vip_right']         = IconCollection::Side($this->icon_5,  'left');
        $retarr['icon_lowcost_right']     = IconCollection::Side($this->icon_6,  'left');
        $retarr['icon_mice_right']        = IconCollection::Side($this->icon_7,  'left');
        $retarr['icon_honey_right']       = IconCollection::Side($this->icon_8,  'left');
        $retarr['icon_family_right']      = IconCollection::Side($this->icon_9,  'left');
        $retarr['icon_active_right']      = IconCollection::Side($this->icon_10, 'left');
        $retarr['icon_relax_right']       = IconCollection::Side($this->icon_11, 'left');

	/* Replace tabs with space */
        $retarr['icon_location_text']   = implode(' ', explode("\t", $this->icon_1_text));
        $retarr['icon_build_at_text']   = implode(' ', explode("\t", $this->icon_3_text));
        $retarr['icon_refresh_text']    = implode(' ', explode("\t", $this->icon_4_text));
        $retarr['check_in']  = implode(' ', explode("\t", $this->check_in));
        $retarr['check_out'] = implode(' ', explode("\t", $this->check_out));

        $retarr['exported'] = true;


        return $retarr;
    }
}
