<?php

class db extends mysqli
{
	public $query;
	public $result;
	public $query_list;

	private static $instances = array();
	protected static $configs = array();

	private function __construct($cname)
	{
		if (is_array($cname)) {
			$config = $cname;
		}
		else {
			$config = $this->load_config($cname);
		}

		parent::__construct($config['host'], $config['user'], $config['pass'], $config['dbname']);

		$this->set_charset("utf8");
	}

	/**
	 * @static
	 * @param string $name название соединения
	 * @return db
	 */
	public static function instance($name = "marketing")
	{
		if (!isset(self::$instances[$name])) {
			self::$instances[$name] = new self($name);
		}
		return self::$instances[$name];
	}

	private function load_config($name)
	{
		if (empty(self::$configs)) {
			$engine = engine::instance();
			$file = "dbconfig.php";
			/* @var $DBCONFIG array */
			include PATH_CONFIG.$file;
			self::$configs = $DBCONFIG;
		}
		return self::$configs[$name];
	}

	/**
	* Запрос к базе данных
	*
	* @param string $query строка запроса
	* @return mysqli_result
	*/
	public function query($query)
	{
		if ($query instanceof query) {
			$this->query = $query->sql();
		}
		elseif (is_array($query)) {
			$this->query = $this->build_query($query);
		}
		else {
			$this->query = $query;
		}

		$this->query_list[] = $this->query;

		$this->result = parent::query($this->query);

		if ($this->error) {
			$this->error();
		}

		return $this->result;
	}

	/**
	 * Строит запрос из массива
	 *
	 * @param array $query
	 * @return string
	 */
	protected function build_query($query)
	{
		$str = "SELECT ".$query['select'];

		if (isset($query['from'])) {
			$str .= " FROM ".$query['from'];
		}
		if (isset($query['left_join'])) {
			if (is_array($query['left_join'])) {
				foreach ($query['left_join'] as $lj) {
					$str .= " LEFT JOIN ".$lj;
				}
			}
			else {
				$str .= " LEFT JOIN ".$query['left_join'];
			}
		}
		if (isset($query['where'])) {
			$str .= " WHERE ".$query['where'];
		}
		if (isset($query['group'])) {
			$str .= " GROUP BY ".$query['group'];
		}
		if (isset($query['having'])) {
			$str .= " HAVING ".$query['having'];
		}
		if (isset($query['order'])) {
			$str .= " ORDER BY ".$query['order'];
		}
		if (isset($query['limit'])) {
			$str .= " LIMIT ".$query['limit'];
		}
		if (isset($query['offset'])) {
			$str .= " OFFSET ".$query['offset'];
		}

		return $str;
	}

	/**
	 * Выполняет запрос к БД и возвращает первую строчку результата
	 *
	 * @param string $query запрос
	 * @param null $field если указана этот параметр, возвращается не вся строка а только значение указанного поля
	 * @return array
	 */
	public function query_row($query, $field = null)
	{
		$this->query($query);
		$row = $this->fetch_row();
		if (!is_null($field)) {
			return $row[$field];
		}
		else {
			return $row;
		}
	}

	/**
	 * Выполняет запрос к БД и возвращает все строки результата
	 *
	 * @param $query запрос
	 * @param string $primarykey название поля, значение которого будет использоваться в качестве индексов в массиве
	 * @param string $field название поля, значение которого будет использоваться в качестве значения (вместо всей строки)
	 * @param bool $multiple
	 * @return array
	 */
	public function query_rows($query, $primarykey = "", $field = "", $multiple = false)
	{
		$this->query($query);

		$list = array();
		while($row = $this->fetch_row()) {
			if ($primarykey) {
				if ($multiple) {
					$list[$row[$primarykey]][] = $field ? $row[$field] : $row;
				} else {
					$list[$row[$primarykey]] = $field ? $row[$field] : $row;
				}
			}
			else {
				$list[] = $field ? $row[$field] : $row;
			}
		}
		return $list;
	}

	/**
	* Возвращает количество строк в таблице/выборке
	*
	* @param string|query $table название таблицы или объект запроса
	* @param string $where условие выборки
	* @return int
	*/
	public function query_count($table, $where = "")
	{
		if ($table instanceof query) {
			$query = $table->count_sql();
		}
		else {
			$query = "SELECT COUNT(*) as count FROM {$table}";
			if ($where) {
				$query .= " WHERE $where";
			}
		}
		$this->query($query);
		$row = $this->fetch_row();

		return $row['count'];
	}

	/**
	* Возвращает строку в виде ассоциативного массива из результата последнего запроса к БД
	*
	* @param mysqli_result $result альтеранативный результат
	* @return array
	*/
	public function fetch_row($result = null)
	{
		if ($result) {
			return $result->fetch_assoc();
		}
		else if ($this->result) {
			return $this->result->fetch_assoc();
		}
		else {
			return null;
		}
	}

	public function fetch_array($primarykey = "")
	{
		$list = array();

		while ($row = $this->result->fetch_assoc()) {
			if ($primarykey) {
				$list[$row[$primarykey]] = $row;
			}
			else {
				$list[] = $row;
			}
		}
		return $list;
	}

	/**
	* Обновляет записи в таблице
	*
	* @param array $data массив вида поле => значение
	* @param string $table исходная таблица
	* @param string $where условие выборки для обновления
	* @return int количество обновленных строк
	*/
	public function update(array $data, $table, $where)
	{
		if (empty($data)) return false;

		$pairs = array();
		foreach ($data as $key => $value) {
			if (is_null($value) || $value === "NULL") {
				$value = "NULL";
			}
			else if (!is_numeric($value)) {
				$value = "'".$this->escape($value)."'";
			}
			$pairs[] = "`$key` = $value";
		}

		$this->query("UPDATE `$table` SET ".implode(", ", $pairs)." WHERE $where");

		return $this->affected_rows;
	}

	/**
	* Добавляет записи в таблицу
	*
	* @param array $data массив вида поле => значение
	* @param string $table исходная таблица
	* @return int количество добавленных строк
	*/
	public function insert(array $data, $table)
	{
		$keys = array();
		$values = array();

		foreach ($data as $key => $value) {
			if (is_null($value) || $value === "NULL") {
				$value = "NULL";
			} else if (!is_numeric($value)) {
				$value = "'".$this->escape($value)."'";
			}
			$keys[] = "`$key`";
			$values[] = $value;
		}

		$this->query("INSERT INTO `$table` (".implode(", ", $keys).") VALUES (".implode(", ", $values).")");

		return $this->insert_id;
	}

    public function replace(array $data, $table)
	{
		$keys = array();
		$values = array();

		foreach ($data as $key => $value) {
			if (is_null($value) || $value === "NULL") {
				$value = "NULL";
			} else if (!is_numeric($value)) {
				$value = "'".$this->escape($value)."'";
			}
			$keys[] = "`$key`";
			$values[] = $value;
		}

		$this->query("REPLACE INTO `$table` (".implode(", ", $keys).") VALUES (".implode(", ", $values).")");
        //print_r($this);
		return $this->insert_id;
	}
	/**
	 * Удаляет строки из таблицы с указанной выборкой
	 *
	 * @param $table
	 * @param $where
	 * @param int $limit
	 */
	public function delete($table, $where, $limit = 0)
	{
		$query = "DELETE FROM {$table}";
		if ($where) $query .= " WHERE $where";
		if ($limit) $query .= " LIMIT $limit";
		$this->query($query);
	}

	/**
	* Форматирует unix-дату в используемый mysql формат
	*
	* @param int $unixtime дата в формате unix
	* @return string
	*/
	public function datetime($unixtime)
	{
		return date("Y-m-d H:i:s", $unixtime);
	}

	public function escape($str)
	{
		return $this->real_escape_string($str);
	}

	protected function error()
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		header("Content-type: text/html; charset=windows-1251");

		$engine = engine::instance();
		$engine->debug_mode = true;
		if ($engine->debug_mode) {
            throw new Exception("MYSQL ERROR: ".$this->error . $this->query);
		}
		else {
			$message = "MYSQL ERROR";
		}

		die($message);

		$log_message = str_replace("\n", " ", $this->error);
		$log_message = " [{$_SERVER['REMOTE_USER']}] [{$_SERVER['PHP_SELF']}] ".$log_message;
		$log = new log("mysql-errors", true);
		$log->write($log_message);
		$log->close();
	}
}
