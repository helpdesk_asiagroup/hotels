<?php

class Regexp 
{
    public static $MysqlInt = array(
            'regexp'      => "/^\s*\d+\s*$|^\s*(=|>|<|>=|<=|<>)\s*\d+\s*$/",
            'callback'    => "none",
            'description' => "Possible comparisons: =,>,<,>=,<=,<>, then %int, or just int'"
        );

    public static $MysqlBoolInt = array(
            'regexp'      => "/^\s*(=|<>)\s*?[01]\s*$/",
            'callback'    => "none",
            'description' => "Possible values =, <> then 0 or 1. Or just 0 or 1"
        );

    public static $MysqlString = array(
            'regexp'      => "/^\s*'[\w\s]+'\s*$|^\s*(=|<>)\s*'[\w\s]+'\s*$/",
            'callback'    => "escape",
            'description' => "Possible values: =, or <>, then quoted(') string. Or just quoted string"
        );
}
