<?php
    function getMiceExportData($hid, $year, $ids_to_export)
    {
        $hotel = Hotel::load($hid);
        $hotel->loadCatalogueInfo($year);
        $general_data = $hotel->catalogue_info->getExportData('l');

        $hotel->loadMiceInfo($year);
        $mice_data = $hotel->mice_info->getExportData();

        unset($general_data['exported']);
        unset($general_data['table_mice']);
        unset($general_data['@icon_mice']);

        $region = Region::load($hotel->catalogue_info->get('block'));
        $output = array_merge($general_data, $mice_data);

        $region_id = $region->get('id');

        $output['name']  = $hotel->get('name');
        $output['block'] = $region->get('name');

        //Dirty Hack to save legacy csv order (in Indesign data fetched by column index, NOT COLUMN NAME)
        $ap_distance = $output['ap_distance'];
        unset($output['ap_distance']);
        $output['ap_distance'] = $ap_distance;


        $airport_map = array(
            1 =>  'BKK', 8 =>  'HKT', 13 => 'KBV', 18 => 'USM',19 => 'CNX', 20 => 'CEI'
        );

        $output['airport'] = $airport_map[$region_id];

        if (!$hotel->mice_info->get('finished') || !in_array($hid, $ids_to_export))
        {
            foreach ($output as $key => &$val) {
                if ($key != 'hotel_id') {
                    $val = '';
                }
            }
        } else {
            $hotel->mice_info->set('exported', 1);
            $hotel->mice_info->set('changed_after_export', 0);
            $hotel->mice_info->set('changed_page', 0);
            $hotel->mice_info->set('changed_position', 0);
            $hotel->mice_info->save();
        }

        return array('title' => array_keys($output), 'data' => array_values($output));
    }

