<?php
    include '../include/include.php';

    if (!$engine->isUserInGroup('designer')) {
        die("You didnt have privileges to do that");
    }

    $block_id = Request::getInt('block_id');
    $type     = Request::getInt('type');
    $year     = Request::getInt('year');
    $ids      = json_decode(Request::getVar('ids'), true);

    $regions_obj = Region::load();
    $db = db::instance();

    foreach ($regions_obj as $reg_obj) {
        $regions[$reg_obj->get('id')] = $reg_obj->get('name');
    }

    switch ($type) {
        case 1:
            $header = array();
            $lines = array();
            $strokes = array();

            $hotel_objs = array();

            $output = '';

            $db = db::instance();
	    $db->autocommit(false);

            $sql = "SELECT h.id FROM hotels h
                        LEFT JOIN catalogue_$year c ON h.id = c.hotel_id
                        LEFT JOIN catalogue_contents cc ON h.id = cc.hotel_id
                        WHERE c.block = $block_id AND cc.general = 1 AND cc.year = $year
                    ORDER BY c.order ASC";
            $db->query($sql);

            while($row = $db->fetch_row()) {
                $hotel_ids[] = $row['id'];
            }

            foreach ($hotel_ids as $id) {
                $hotel_objs[] = Hotel::load($id, $year);
            }

            $i = 0;
            $total_hotels = count($hotel_objs);
            foreach ($hotel_objs as $hotel) {
                $i++;
                $position = $i % 2 ? 'left' : 'right';

                $csv_arr = $hotel->getExportData($position, $year);

                if (!in_array($hotel->get('id'), $ids)) {
                    foreach ($csv_arr as $key => &$val) {
                        if ($key != 'hotel_id') {
                            $val = '';
                        }
                    }
                } else {
                    $hotel->catalogue_info->set('exported', 1);
                    $hotel->catalogue_info->set('changed_after_export', 0);
                    $hotel->catalogue_info->set('changed_page', 0);
                    $hotel->catalogue_info->set('changed_position', 0);
                    $hotel->catalogue_info->save();
                }

                if ($i <= 2) {
                    $suffix = '-'.strtoupper($position);
                    foreach($csv_arr as $name => $data) {
                        $header[] = $name.$suffix;
                    }
                }

                $strokes[] = implode("\t", $csv_arr);
                if ($position == 'right' || $total_hotels == $i) {
                    $lines[] = implode("\t", $strokes);
                    $strokes = array();
                }
            }

            $header = implode("\t", $header);
            $lines  = implode("\r\n", $lines);

            $output = $header."\r\n".$lines;
            $filename = "GENERAL_";

            break;
        case 3:
            include 'getMiceDataFunc.php';
            $db->query("SELECT cc.hotel_id FROM catalogue_contents cc
                    LEFT JOIN catalogue_$year gen_cat ON cc.hotel_id = gen_cat.hotel_id
                    LEFT JOIN mice_$year mc ON mc.hotel_id = cc.hotel_id
                    WHERE cc.mice = 1 AND cc.year = $year AND gen_cat.block = $block_id
                    ORDER BY mc.`order` ASC");

            while($row = $db->fetch_row())
            {
                $hotel_ids[] = $row['hotel_id'];
            }

            foreach ($hotel_ids as $hid) {
                $export = getMiceExportData($hid, 2017, $ids);
                $block_lines[] = implode("\t", $export['data']);
            }

            $output = implode("\t", $export['title']);

            foreach ($block_lines as $line)
            {
                $output .= "\r\n".$line;
            }

            $output = iconv("UTF-8", 'UCS-2LE//TRANSLIT', $output);

            $filename = "MICE_";
            break;
    }

    $block_name = $regions[$block_id];
    $filename .= $block_name . "_UCS-2LE" . date("_d_m_H_i").".txt";
    header('Content-type: text/plain; Charset: UCS-2LE');
    header('Content-Disposition: attachment; filename="'.$filename.'"');

    echo($output);

    if ($type == 1) {
      $db->commit();
    }
