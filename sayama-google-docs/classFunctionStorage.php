<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	
	
	use Google\Spreadsheet\DefaultServiceRequest;
	use Google\Spreadsheet\ServiceRequestFactory;


	class googleDataOperations{
		public static $sheetnames = ['01 MAINLAND','02 ISLANDS', '03 EXOTIC', '04 EXCURSIONS','05 LUXURY', '06 MICE'];
		public static function dataOperations($operation = false,$book_name = false,$sheet_name = false, $data_array = false){

			if( !defined( __DIR__ ) ) define( __DIR__, dirname(__FILE__) );
			require __DIR__ . '/vendor/autoload.php';
			
			putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/client_secret.json');
			$client = new Google_Client;
			$client->useApplicationDefaultCredentials();
			 
			$client->setApplicationName("Something to do with my representatives");
			$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
			 
			if ($client->isAccessTokenExpired()) {
			    $client->refreshTokenWithAssertion();
			}
			 
			$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
			ServiceRequestFactory::setInstance(
			    new DefaultServiceRequest($accessToken)
			);
			$shit_number = array_search($sheet_name, self::$sheetnames);
			if($book_name && $shit_number > -1) {
				$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)->getSpreadsheetFeed()->getByTitle($book_name);
				$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
				$worksheet = $worksheets[$shit_number];
				$listFeed = $worksheet->getListFeed();
				if($operation == 'insert' && is_array($data_array) && count($data_array) > 0){

					foreach ($data_array  as $data_row) {
						$listFeed->insert($data_row);
					}
				} else if($operation == 'delete'){
				    $row_count = (count($listFeed->getEntries()) > 0)?count($listFeed->getEntries()):0;
				    echo $row_count;
				    while ($row_count > 0){
                        foreach ($listFeed->getEntries() as $entry) {
                            if ($entry->getValues()['type'] = $book_name ) {
                                $entry->delete();
                                break;
                            }
                        }
                        --$row_count;
                        //echo $row_count;
                    }
				} else if($operation == 'display'){
				    echo 'row count - '.count($listFeed->getEntries());
                    foreach ($listFeed->getEntries() as $entry) {
                        //$representative = $entry->getValues();
                        //print_r($representative);
                        $entry->getValues()['type'] = $book_name ?print_r($entry->getValues()):print('no entries');

                    }

                } else { echo 'wrong entries';}
			}
		}

	}

    class mySqlConnect {

        private  static $dbConnSet = [
            "host"=>"127.0.0.1",
            "user"=>"root",
            "password"=>"srv_hotels_mysql",
            "dbname"=>"marketing"
        ];

        public static function myPDOquery($query){
            try{
                $conn = new PDO("mysql:host=".self::$dbConnSet['host'].";dbname=".self::$dbConnSet['dbname'].";charset=utf8", self::$dbConnSet['user'], self::$dbConnSet['password']);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //echo "Connected successfully";

                $stmt = $conn->prepare($query);
                $stmt->execute();
                //echo $query.'<hr>';
                // set the resulting array to associative
                $result = false;
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                try {
                    $result = new RecursiveArrayIterator($stmt->fetchAll());
                    $result = $result->getArrayCopy();
                }
                catch(PDOException $e){
                    echo "Connection failed: " . $e->getMessage();
                }

            }
            catch(PDOException $e){
                echo "Connection failed: " . $e->getMessage();
            }
            return $result;
        }

    }

?>