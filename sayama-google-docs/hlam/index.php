<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	
	
	use Google\Spreadsheet\DefaultServiceRequest;
	use Google\Spreadsheet\ServiceRequestFactory;

	class googleDataOperations{
		public static $sheetnames = ['01 MAINLAND','02 ISLANDS', '03 EXOTIC', '04 EXCURSIONS','05 LUXURY', '06 MICE'];
		public static function dataOperations($operation = false,$book_name = false,$sheet_name = false, $data_array = false){

			if( !defined( __DIR__ ) ) define( __DIR__, dirname(__FILE__) );
			require __DIR__ . '/vendor/autoload.php';
			
			putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/client_secret.json');
			$client = new Google_Client;
			$client->useApplicationDefaultCredentials();
			 
			$client->setApplicationName("Something to do with my representatives");
			$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
			 
			if ($client->isAccessTokenExpired()) {
			    $client->refreshTokenWithAssertion();
			}
			 
			$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
			ServiceRequestFactory::setInstance(
			    new DefaultServiceRequest($accessToken)
			);
			$shit_number = array_search($sheet_name, self::$sheetnames);
			if($book_name && $shit_number > -1) {
				$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)->getSpreadsheetFeed()->getByTitle($book_name);
				$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
				$worksheet = $worksheets[$shit_number];
				$listFeed = $worksheet->getListFeed();
				if($operation == 'insert' && is_array($data_array) && count($data_array) > 0){

					foreach ($data_array  as $data_row) {
						$listFeed->insert($data_row);
					}
				} else if($operation == 'delete'){
						foreach ($listFeed->getEntries() as $entry) {
						   if ($entry->getValues()['type'] === $sheet_name) {
						       $entry->delete();
						       //break;
						   }
						}
				}else { echo 'wrong entries';}
			}
		}
		
	}
	
	//
?>