<?php

require 'classFunctionStorage.php';

$hotel_vals = [
    [
        'page' => '01 MAINLAND',
        'order' => 'test_222mass_1',
        'name' => 'test_222mass_3',
        'rate' => 'test_222mass_11111',
        'hotelid' => 323,
        'type' => '01 MAINLAND',
        'folderlink' => 'S:\\sam\\test\\linl\\'
       // ...
    ],
    [
        'page' => '01 MAINLAND',
        'order' => '3',
        'name' => 'thffgjjghjmrmkgh',
        'rate' => '5',
        'hotelid' => 195,
        'type' => '01 MAINLAND',
        'folderlink' => 'S:\\sam\\test\\linlked2222\\'
       // ...
    ],
    [
        'page' => '01 MAINLAND',
        'order' => 'test_2esfsg22mass_1',
        'name' => 'test_22grsd2mass_3',
        'rate' => 'test_gsd222sdgdmass_11111',
        'hotelid' => 32443,
        'type' => '01 MAINLAND',
        'folderlink' => 'S:\\sam\\test\\linl\\'
        // ...
    ],
    [
        'page' => '01 MsfsfdAINLAND',
        'order' => '55',
        'name' => 'thffdsffdsfgjjghjmrmkgh',
        'rate' => '5',
        'hotelid' => 19995,
        'type' => '01 MAINLAND',
        'folderlink' => 'S:\\sam\\test\\linlked2222\\'
        // ...
    ]
];
$query = "SELECT * FROM ((SELECT
    CASE WHEN _cat.order < 10 THEN CONCAT('00', _cat.order) WHEN _cat.order < 100 THEN CONCAT('0', _cat.order) ELSE _cat.order END AS `order`,
    _cat.rate AS `rate`,
    _hot.name AS `name`,
    CONCAT('#', _cat.hotel_id) AS `hotel_id`,
    _cat.hotel_id AS `hot_id`,
    CASE WHEN _reg.order_mainland < 10 THEN CONCAT('0', _reg.order_mainland, ' ', _reg.name) WHEN _reg.order_mainland >= 10 THEN CONCAT(_reg.order_mainland, ' ', _reg.name) END AS `folders`,
    CASE WHEN _vol.id < 10 THEN CONCAT('0', _vol.id, ' ', _vol.name) ELSE CONCAT(_vol.id, ' ', _vol.name) END AS `type`
  FROM volumes _vol,
       regions_copy _reg
         JOIN catalogue_2019 _cat
           ON (_reg.id = _cat.block)
         JOIN hotels _hot
           ON (_cat.hotel_id = _hot.id)
  WHERE _reg.order_mainland > 0
  AND _vol.name = 'MAINLAND'
  ORDER BY _reg.order_mainland)
UNION ALL
(SELECT
    CASE WHEN _cat.order < 10 THEN CONCAT('00', _cat.order) WHEN _cat.order < 100 THEN CONCAT('0', _cat.order) ELSE _cat.order END AS `order`,
    _cat.rate AS `rate`,
    _hot.name AS `name`,
    CONCAT('#', _cat.hotel_id) AS `hotel_id`,
    _cat.hotel_id AS `hot_id`,
    CASE WHEN _reg.order_islands < 10 THEN CONCAT('0', _reg.order_islands, ' ', _reg.name) WHEN _reg.order_islands >= 10 THEN CONCAT(_reg.order_islands, ' ', _reg.name) END AS `folders`,
    CASE WHEN _vol.id < 10 THEN CONCAT('0', _vol.id, ' ', _vol.name) ELSE CONCAT(_vol.id, ' ', _vol.name) END AS `type`
  FROM volumes _vol,
       regions_copy _reg
         JOIN catalogue_2019 _cat
           ON (_reg.id = _cat.block)
         JOIN hotels _hot
           ON (_cat.hotel_id = _hot.id)
  WHERE _reg.order_islands > 0
  AND _vol.name = 'ISLANDS'
  ORDER BY _reg.order_islands)
UNION ALL
(SELECT
    CASE WHEN _cat.order < 10 THEN CONCAT('00', _cat.order) WHEN _cat.order < 100 THEN CONCAT('0', _cat.order) ELSE _cat.order END AS `order`,
    _cat.rate AS `rate`,
    _hot.name AS `name`,
    CONCAT('#', _cat.hotel_id) AS `hotel_id`,
    _cat.hotel_id AS `hot_id`,
    CASE WHEN _reg.order_exotic < 10 THEN CONCAT('0', _reg.order_exotic, ' ', _reg.name) WHEN _reg.order_exotic >= 10 THEN CONCAT(_reg.order_exotic, ' ', _reg.name) END AS `folders`,
    CASE WHEN _vol.id < 10 THEN CONCAT('0', _vol.id, ' ', _vol.name) ELSE CONCAT(_vol.id, ' ', _vol.name) END AS `type`
  FROM volumes _vol,
       regions_copy _reg
         JOIN catalogue_2019 _cat
           ON (_reg.id = _cat.block)
         JOIN hotels _hot
           ON (_cat.hotel_id = _hot.id)
  WHERE _reg.order_exotic > 0
  AND _vol.name = 'EXOTIC'
  ORDER BY _reg.order_exotic)
UNION ALL
(SELECT
    CASE WHEN _cat.order < 10 THEN CONCAT('00', _cat.order) WHEN _cat.order < 100 THEN CONCAT('0', _cat.order) ELSE _cat.order END AS `order`,
    _cat.rate AS `rate`,
    _hot.name AS `name`,
    CONCAT('#', _cat.hotel_id) AS `hotel_id`,
    _cat.hotel_id AS `hot_id`,
    CASE WHEN _reg.order_luxury < 10 THEN CONCAT('0', _reg.order_luxury, ' ', _reg.name) WHEN _reg.order_luxury >= 10 THEN CONCAT(_reg.order_luxury, ' ', _reg.name) END AS `folders`,
    CASE WHEN _vol.id < 10 THEN CONCAT('0', _vol.id, ' ', _vol.name) ELSE CONCAT(_vol.id, ' ', _vol.name) END AS `type`
  FROM volumes _vol,
       regions_copy _reg
         JOIN luxury_2019 _cat
           ON (_reg.id = _cat.block)
         JOIN hotels _hot
           ON (_cat.hotel_id = _hot.id)
  WHERE _reg.order_luxury > 0
  AND _vol.name = 'LUXURY'
  ORDER BY _reg.order_luxury)
UNION ALL
(SELECT
    CASE WHEN _cat.order < 10 THEN CONCAT('00', _cat.order) WHEN _cat.order < 100 THEN CONCAT('0', _cat.order) ELSE _cat.order END AS `order`,
    _cat.rate AS `rate`,
    _hot.name AS `name`,
    CONCAT('#', _cat.hotel_id) AS `hotel_id`,
    _cat.hotel_id AS `hot_id`,
    CASE WHEN _reg.order_mice < 10 THEN CONCAT('0', _reg.order_mice, ' ', _reg.name) WHEN _reg.order_mice >= 10 THEN CONCAT(_reg.order_mice, ' ', _reg.name) END AS `folders`,
    CASE WHEN _vol.id < 10 THEN CONCAT('0', _vol.id, ' ', _vol.name) ELSE CONCAT(_vol.id, ' ', _vol.name) END AS `type`
  FROM volumes _vol,
       regions_copy _reg
         JOIN mice_2019 _cat
           ON (_reg.id = _cat.block)
         JOIN hotels _hot
           ON (_cat.hotel_id = _hot.id)
  WHERE _reg.order_mice > 0
  AND _vol.name = 'MICE'
  ORDER BY _reg.order_mice)) _all WHERE _all.type = '01 MAINLAND';";
print_r(mySqlConnect::myPDOquery($query));
//googleDataOperations::dataOperations('insert','HOTELS','01 MAINLAND',$hotel_vals);

?>