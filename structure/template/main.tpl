<script src="/libs/handsontable/dist/handsontable.full.js"></script>
<?php $available_years = array(2015 => 2015, 2016 => 2016, 2017 => 2017, 2018 => 2018); ?>
<?php $available_types = array(1 => "GENERAL", 2 => "LUXURY", 3 => "MICE");?>
<link rel="stylesheet" media="screen" href="/libs/handsontable/dist/handsontable.full.css">

<div class="container">
    <div class="row col-xs-12">

        <div class="col-sm-3">
        <h3>
           <?= HtmlSnippets::select($regions, 'region_id', $class='form-control input-lg js-region-select', $region_id); ?>
        </h3>
        </div>

        <div class="col-sm-3">
        <h3>
            <?= HtmlSnippets::select($available_types, 'type', 'form-control input-lg js-type-select', $type); ?>
        </h3>
        </div>

        <div class="col-sm-2">
        <h3>
            <?= HtmlSnippets::select($available_years, 'year', 'form-control input-lg js-year-select', $year); ?>
        </h3>
        </div>
    </div>
    <br>
    <br>

    <div class="row col-xs-12">
        <div id="excel-table"></div>
        <br>
        <button class="btn btn-primary js-preview-button">Preview</button>
    </div>
    <br><br>
    <div class="col-xs-6">
        <center><h4>Текущее состояние</h4></center>
        <ul class="list-group js-before"></ul>
    </div>
    <div class="col-xs-6">
        <center><h4>После изменений</h4></center>
        <ul class="list-group js-after"></ul>
    </div>

        <br>
        <button class="btn btn-primary js-save-button" style="display:none">Save</button>
        <br><br><br><br>
        <br><br><br><br>
</div>

<script>
    var data = [
    {order: "", trash: "", name: "", rate: ""}
    ];

    var container = document.getElementById('excel-table');
    var hot = new Handsontable(container, {
        data: data,
        columns: [{data: 'order'},{data:'name'},{data:'rate'},{data: 'id'}],
        colHeaders: true
    });

    function renderList(hotelsArray) {
        var ret = '';
        for(var i in hotelsArray) {
            ret += '<li class="list-group-item">';
            ret += hotelsArray[i].name+' '+hotelsArray[i].rate;
            ret += ' </li>';
        }

        return ret;
    }

    function getPostFields()
    {
        return {
            type: $('.js-type-select').val(),
            region_id: $('.js-region-select').val(),
            year: $('.js-year-select').val(),
            data: JSON.stringify(hot.getData())
        };
    }

    $('.js-preview-button').click(function(){
        fields = getPostFields();
        $(this).hide();
        save_btn = $('.js-save-button');
        $.ajax({
            type: "POST",
            url: 'ajax/update.php?preview=1',
            data: fields,
            success: function(data) {
                $('.js-before').html(renderList(data.before))
                $('.js-after').html(renderList(data.after))
                save_btn.show();
            },
            dataType: 'json'
        });
    })

    $('.js-save-button').click(function(){
        fields = getPostFields();
        $(this).addClass('disabled').text("Saving... Please wait.");
        $.ajax({
            type: "POST",
            url: 'ajax/update.php?preview=0',
            data: fields,
            success: function(data) {
                alert(data.message);
            },
            dataType: 'json'
        });
    })

</script>
