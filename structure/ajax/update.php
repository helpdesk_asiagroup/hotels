<?php
    include '../../include/include.php';

    $engine->requireGroup('editor');

    $type = Request::getInt('type');
    $year = Request::getInt('year', 2017);
    $data = json_decode(Request::getVar('data'), true);
    $region_id = Request::getInt('region_id', 1);
    $preview = Request::getInt('preview', 1);

    switch ($type) {
        case 1: //Genereal
            $tableName = "catalogue_$year";
            $colName = "general";
            $className = "Catalogue$year";
            break;
        case 2: //Luxury
            $tableName = "luxury_$year";
            $colName = "luxury";
            $className = "Luxury$year";
            break;
        case 3: //Mice
            $tableName = "mice_$year";
            $colName = "mice";
            $className = "Mice$year";
            break;
    }


    $regions_obj = Region::load();

    foreach ($regions_obj as $reg_obj) {
        $regions[$reg_obj->get('id')] = $reg_obj->get('name');
    }

    $db = db::instance();
    $sql = "SELECT h.id, h.name, cat.order, cat.rate, r.name AS region_name FROM hotels h
                    LEFT JOIN catalogue_contents cc ON cc.hotel_id = h.id
                    LEFT JOIN $tableName cat ON cat.hotel_id = h.id
                    LEFT JOIN catalogue_$year gen_cat ON gen_cat.hotel_id = h.id
                    LEFT JOIN regions r ON r.id = cat.block
                WHERE cc.$colName = 1 AND cc.year = $year AND gen_cat.block = $region_id
                ORDER BY cat.order ASC";

    $db->query($sql);
    while($row = $db->fetch_row()) {
        $hotel['order'] = $row['order'];
        $hotel['name'] = $row['name']. " #".$row['id'];
        $hotel['rate'] = $row['rate'];

        $json['before'][] = $hotel;
        $old[$row['id']] = $hotel;
    }

    if ($preview) {
        $db->autocommit(FALSE);
    }

    foreach($data as $hotel) {
        $hid = (int) $hotel[3];
        if (!$hid) {
            continue;
        }
        $new_ids[] = $hid;
        $order = (int) $hotel[0];
        $rate = $type == 2 ? 5 : $db->escape($hotel[2]);
        $block = $region_id;

//	$hotObj = Hotel::load($hid);
//	$hotObj->set('name', $hotel[1]);
//	$hotObj->save();
	$generalClass = "Catalogue$year";

	$genObj = $generalClass::load($hid, $year);
        $catObj = $className::load($hid, $year);

        $genObj->set('block', $block);
	$genObj->save();

        $catObj->set('order', $order);
        $catObj->set('block', $block);
        $catObj->set('rate',  $rate);
        $changed_page     = $old[$hid]['order'] == $order ? 0 : 1;
        $changed_position = $old[$hid]['order'] % 2 == $order % 2 ? 0 : 1;
        $catObj->set('changed_page',  $changed_page);
        $catObj->set('changed_position',  $changed_position);

        $catObj->save();

        $db->query("INSERT INTO catalogue_contents (hotel_id, $colName, year) VALUES ($hid, 1, $year)
                    ON DUPLICATE KEY UPDATE hotel_id=$hid, year=$year, $colName=1");
    }

    $ids = implode(',', $new_ids);


    $db->query($ccSQL = "UPDATE catalogue_contents cc
                LEFT JOIN catalogue_$year cat on cat.hotel_id = cc.hotel_id
                LEFT JOIN hotels h on h.id = cc.hotel_id
                SET cc.$colName = 0
                WHERE cc.hotel_id NOT IN ($ids) AND (cat.block = $region_id OR h.region_id = $block)");

    $db->query($sql);
    while($row = $db->fetch_row()) {
        $hotel['order'] = $row['order'];
        $hotel['name'] = $row['name']. " #".$row['id'];
        $hotel['rate'] = $row['rate'];

        $json['after'][] = $hotel;
    }

    if ($preview) {
        $db->rollback();
    }
    $json['message'] = "Successfully imported! OK!";
    echo json_encode($json);
