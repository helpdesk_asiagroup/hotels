<?php
    include '../include/include.php';

    $region_id = Request::getInt('region', 1);
    $year   = Request::getInt('year', 2018);
    $type   = Request::getInt('type', 1);

    $dbBlocks = Region::load('*');

    foreach ($dbBlocks as $reg_obj) {
        $regions[$reg_obj->get('id')] = $reg_obj->get('name');
    }

    $view = new View();
    $view->setTemplate('main', compact('region_id', 'year', 'type', 'regions'));
    $view->display();
