<?php 
    include '../../include/include.php';

    $hotel = Request::getArray('hotel', $default = false);
    $competitor_info = Request::getArray('competitor_info', $default = false);

    $json = array('status' => 'OK');

    if (!$hotel || !$competitor_info) {
        $json['status'] = 'Error';
        $json['error'] = 'Wrong data passed! Expected arrays (hotel and competitor_info)';
        echo json_encode($json);
        exit();
    }

    //Cretae Hotel Object From Request
    $hotel_obj = Hotel::createEmpty();

    foreach ($hotel as $id => $hotel_array) {
        $hotel_obj->set('id', (int) $id);
        foreach ($hotel_array as $prop => $val) {
            $hotel_obj->set($prop, $val);
        }
    }

    $hotel_obj->loadCompetitorsInfo();

    //Check for data consistency. Allow change competitor info only for loaded Hotel
    //Compare id from model (competitors_info MySQL table) and row_id from request. 
    //Should be the same to pass the check
    foreach ($hotel_obj->competitors_info as $comp_id => $comp_obj) {
        if ($comp_obj->get('id') != $competitor_info[$comp_id]['row_id']) {
            $json['status'] = 'Error';
            $json['error']  = "Sended data is not consistent! Object id: ";
            $json['error'] .= $comp_obj->get('id'). " != Array row_id: $competitor_info[$comp_id]['row_id']";
            echo json_encode($json);
            exit();
        }
    }

    //If Data consistence fill object from request
    foreach ($competitor_info as $comp_id => $comp_array) {
        foreach ($comp_array as $prop => $val) {
            $prop = $prop == 'row_id' ? 'id' : $prop;
            if (!$hotel_obj->competitors_info[$comp_id]->set($prop, $val)) {
                $json['status'] = 'Error';
                $json['error']  = "Unknown property name $prop for CompetitorInfo object";
                echo json_encode($json);
                exit();
            }
        }
        //$hotel_obj->competitors_info[$comp_id]->set('id', $comp_array['row_id']);
    }
    $hotel_obj->save($save_competitor_info = true);
    $json['answer'] = "Saved succesfully!";
    echo json_encode($json);
