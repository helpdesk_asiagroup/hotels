<?php
    
    include '../../../include/include.php';    

    $json = array();
    $db = db::instance();

    $sql = "SELECT name, region_id FROM hotels";
    $db->query($sql);

    while ($row = $db->fetch_row()) {
        $json[] = $row;
    }
    echo json_encode($json);
