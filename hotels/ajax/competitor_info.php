<?php
 
    include '../../include/include.php';

    $action = Request::getVar('action');
    $json = array('status' => 'OK');

    switch ($action) {
        default :
        case 'list':
            $hotel_id  = Request::getInt('hotel_id');
            $region_id = Request::getInt('region_id');
            $name      = Request::getVar('name');

            $db = db::instance();
            $list = array();

            if ($hotel_id) {
                $list[] = Hotel::load($hotel_id);

            } else if ($region_id) {

                $sql = "SELECT * FROM hotels WHERE region_id = $region_id";
                $db->query($sql);

                while ($row = $db->fetch_row()) {
                    $hotel = Hotel::createEmpty();

                    foreach ($row as $prop => $val) {
                        $hotel->set($prop, $val);
                    }

                    $list[] = $hotel;
                }
            }

            $vars = compact('list');
    
            if (!$list) {
                $json['status'] = 'Error';
                $json['error']  = 'Search return 0 results';
                echo json_encode($json);
                break;
            }

            ob_start();
            $view = new View();
            $view->setTemplate('../../template/_hotel_list', $vars);
            $view->display_ajax();
            $json['answer'] = ob_get_contents();
            ob_end_clean();

            echo json_encode($json);
            break;

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

        case 'full_info':
            $hotel_id = Request::getInt('hotel_id');

            if (!$hotel_id) {
                $json['status'] = 'Error';
                $json['error']  = 'Search return 0 results';
                echo json_encode($json);
                break;
            }

            $hotel = Hotel::load($hotel_id);
            $hotel->loadCompetitorsInfo();

            $bool_select = array(
                '0' => 'Нет',
                '1' => 'Есть',
                '2' => '?'
            );

            $rates = array();
            $rates[] = '?';

            for ($i = 1; $i < 6; $i++) {
                $rates[] = $i;
            }
            
            $regions = array();
            $regions_obj = Region::load();

            foreach ($regions_obj as $reg_obj) {
                $regions[$reg_obj->get('id')] = $reg_obj->get('name');    
            }

            $vars = compact('hotel', 'regions', 'bool_select', 'rates');

            ob_start();
            $view = new View(); 
            $view->setTemplate('../../template/_full_info', $vars);
            $view->display_ajax();

            $json['answer'] = ob_get_contents();
            ob_end_clean();
            echo json_encode($json);
            break;
    }
