<?php 

    require '../../include/include.php';

    $db = db::instance();
    $region_id = Request::getInt('region_id', $default = 0);
    $region = $region_id ? "AND h.region_id = $region_id" : '';
    $name = $db->escape(trim(Request::getVar('term', $default = false)));

    if (!$name) {
        echo '[]';
        exit();
    }

    $json = array();

    $sql = "SELECT h.*, r.name AS region_name FROM hotels h
            LEFT JOIN regions r ON h.region_id = r.id
            WHERE h.name LIKE '%$name%' $region
            ORDER BY h.name";

    $db->query($sql);

    while ($row = $db->fetch_row()) {
        $hotel = array('id' => $row['id'] , 'value' => $row['name'] ." (".$row['region_name'].")");
        $json[] = $hotel;
    }

    echo json_encode($json);
