<?php
	//ini_set('display_errors', 1);

	class mySqlConnect {

		private  $dbConnSet = array(
	    "host"=>"127.0.0.1",
	    "user"=>"root",
	    "password"=>"srv_hotels_mysql",
	    "dbname"=>"marketing"
	    );
	    public function __get($property) {
		    if (property_exists($this, $property)) {
		      	return $this->$property;
		    }
		}
	  	public function __set($property, $value) {
		    if (property_exists($this, $property)) {
		      	$this->$property = $value;
		    }

		    return $this;
		}
	    public function myPDOquery($query){
	    	try{
	    		$conn = new PDO("mysql:host=".$this->dbConnSet['host'].";dbname=".$this->dbConnSet['dbname'].";charset=utf8", $this->dbConnSet['user'], $this->dbConnSet['password']);
		    	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		//echo "Connected successfully"; 

	    		$stmt = $conn->prepare($query); 
    			$stmt->execute();
    			//echo $query.'<hr>';
			    // set the resulting array to associative
			    $result = false;
			    $stmt->setFetchMode(PDO::FETCH_ASSOC);
			    try {
					$result = new RecursiveArrayIterator($stmt->fetchAll());
					$result = $result->getArrayCopy();
			    } 
			    catch(PDOException $e){
		    		echo "Connection failed: " . $e->getMessage();
		    	}

	    	}
	    	catch(PDOException $e){
	    		echo "Connection failed: " . $e->getMessage();
	    	}
	    	return $result;
	    }

	}

?>