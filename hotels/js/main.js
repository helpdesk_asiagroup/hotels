$(function() {
    var default_state_overlay = $('.overlay .msg .text').html();
    $('.overlay .msg button').click(function() {
        $('.overlay .msg .text').html(default_state_overlay);
        $(this)
            .hide()
            .parents('.overlay')
            .hide();
    });

    var autocomplete_config = {
        source: "ajax/searcher.php",
        select: function(event, data) {
            $('.typeahead').attr('data-hotel-id',data.item.id);    
        },
        minLength: 2
    };

    $('.typeahead').bind('input' ,function() {$(this).attr('data-hotel-id', '0')})
    $('.typeahead').autocomplete(autocomplete_config);

    $('[name=region_id]').change(function() {
        $('.typeahead').autocomplete('option', 'source', "ajax/searcher.php?region_id="+$(this).val());
    });

    $('#hotel-form').submit(function (event) {
        var params = {
            hotel_id: $('[name=name]').attr('data-hotel-id'),
            region_id: $('[name=region_id]').val(),
            hotel_name: $('[name=name]').val()
        }
        console.log(params);
        $.ajax({
            url: 'ajax/competitor_info.php', 
            data: params,
            dataType: 'json',
            beforeSend: function () {
                $('.overlay').show();
            },
            success: function(data) {
                if (data.status == 'OK') {
                    $("#search-result").html(data.answer);
                    $(".search-result-tab").removeClass('hidden'); 
                    $('.overlay').hide();
                    $('.search-result-tab > a').trigger('click');
                } else {
                    $('.overlay .msg .text').html(data.error);
                    $('.overlay .msg button').slideDown();
                }
            }
        });
        event.preventDefault();
    });

    $('#search-result').on('click', 'a', function(event) {
        event.preventDefault();
        var params = {
            hotel_id: $(this).attr('data-id'),
            action: 'full_info'
        }
        console.log(params);
        $.ajax({
            url: 'ajax/competitor_info.php',
            data: params,
            dataType: 'json',
            beforeSend: function () {
                $('.overlay').show();
            },
            success: function(data) {
                if (data.status = 'OK') {
                    $('#hotel-info').html(data.answer);
                    $(".hotel-info-tab").removeClass('hidden'); 
                    $('.overlay').hide();
                    $('.hotel-info-tab > a').trigger('click');
                } else {
                    $('.overlay .msg .text').html(data.error);
                    $('.overlay .msg button').slideDown();
                }
            }
        });
    });

    $('#hotel-info').on('change', '[name=rate]', function() {
        var val = $(this).val() == "0" ? "2" : "1";
        $(this).parents('.form-group').find('[name=catalog]').val(val);
    });

    $('#hotel-info').on('click', 'button.save-full-info', function () {
        var all_info = {hotel : {}, competitor_info: {} };
        $('form.full-info-tab').each(function () {
            type = $(this).attr('data-type')
            tmp = $(this).serializeArray();
            group = {};

            for (var i in tmp) {
                if (tmp[i].name == 'comp_id' || tmp[i].name == 'hotel_id') {
                    var id = tmp[i].value 
                } else {
                    group[tmp[i].name] = tmp[i].value;
                }
            }
            all_info[type][id] = group
        });

        $.ajax({
            url: 'ajax/save_full_info.php',
            data: all_info,
            dataType: 'json',
            beforeSend: function() {
                $('.overlay').show();
            },
            success: function (data) {
                message = data.status == 'OK' ? data.status+" "+data.answer : data.status+" "+data.error;
                $('.overlay .msg .text').html(message);
                $('.overlay .msg button').slideDown();
            }
        });
    });

    $('#hotel-info').on('click', '.gps', function () {
        navigator.geolocation.getCurrentPosition(function (pos) {
            $('input[name=longitude]').val(pos.coords.longitude);
            $('input[name=latitude]').val(pos.coords.latitude);
        });
    });
});
