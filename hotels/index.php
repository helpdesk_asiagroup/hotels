<?php
    $APP = 'hotels';
    require '../include/include.php';

    $regions = array();
    $regions[0] = 'ALL';
    $regions_obj = Region::load();

    foreach ($regions_obj as $reg_obj) {
        $regions[$reg_obj->get('id')] = $reg_obj->get('name');    
    }

    $vars = compact('regions');

    $view = new View();
    $view->addLib('jquery-ui');
    $view->addAppsJs('main.js');
    $view->addAppsCss('main.css');
    $view->setTemplate('_default', $vars);
    $view->display();



