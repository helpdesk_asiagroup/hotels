<div class="container">
    <div class="app-container">
    <div class="overlay">
        <div class="msg">
            <span class="text">
                Please wait <br>
                <img src="../images/ajax_loader.gif">
            </span>
            <button type="button" class="btn btn-block btn-primary">OK</button>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active search-tab"><a href="#search" data-toggle="tab">Поиск</a></li>    
        <li class="hidden search-result-tab"><a href="#search-result" data-toggle="tab">Отели</a></li>
        <li class="hidden hotel-info-tab"><a href="#hotel-info" data-toggle="tab">Редактирование</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="search">
            <form id="hotel-form" class="fade-in-loading form-horizontal" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">Название</label>
                    <div class="col-sm-10"><input class="form-control typeahead" name="name" type="text"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="region_id">Регион</label>
                    <div class="col-sm-2">
                        <?= HtmlSnippets::select($regions, 'region_id'); ?>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-success">Поиск</button>
                    <button type="button" class="cancel btn btn-danger">Отмена</button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="search-result">
        </div>
        <div class="tab-pane" id="hotel-info">
        </div>
    </div>
    </div>
</div>
