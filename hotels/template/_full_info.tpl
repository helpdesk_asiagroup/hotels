<div class="base-info">
    <form id="base-info-form" class="form-horizontal full-info-tab" data-type="hotel" action="" method="post">
        <div class="form-group">
            <input type="hidden" name="hotel_id"   value="<?= $hotel->get('id') ?>">

            <label class="col-sm-2 control-label" for="name">Название</label>
            <div class="col-sm-6">
                <input class="form-control" name="name" type="text" value="<?=$hotel->get('name')?>">
            </div>
            <label class="col-sm-1 control-label" for="region">Регион</label>
            <div class="col-sm-3">
                <?= HtmlSnippets::select($regions, 'region_id', 'form-control',$hotel->get('region_id')) ?> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="latitude">Широта / Latitude</label>
            <div class="col-sm-2">
                <input class="form-control" name="latitude" type="text" value="<?=$hotel->get('latitude')?>">
            </div>
            <label class="col-sm-2 control-label" for="longitude">Долгота / Longitude</label>
            <div class="col-sm-2">
                <input class="form-control" name="longitude" type="text" value="<?=$hotel->get('longitude')?>">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary gps" type="button">GPS</button>
            </div>
        </div>
    </form>
</div>
<div class="well">
<div class="competitor-info">
    <ul class="nav nav-tabs">
        <?php foreach ($hotel->competitors_info as $c_id => $c_obj): ?>
            <?php $active = $c_id == 1 ? 'active' : ''; ?>
            <li class="<?=$active?>"><a href="#competitor-<?= $c_id ?>" data-toggle="tab"><?= $c_obj->get('name_ru'); ?></a></li>
        <?php endforeach; ?>
    </ul>

    <div class="tab-content">
        <?php foreach ($hotel->competitors_info as $c_id => $c_obj): ?>
            <?php $active = $c_id == 1 ? 'active in' : ''; ?>
            <div class="tab-pane fade <?= $active ?>" id="competitor-<?= $c_id ?>">
              <form id="competitor-info-form-<?= $c_id ?>" class="form-horizontal full-info-tab" data-type="competitor_info" action="" method="post">
                <div class="form-group">
                    <input type="hidden" name="row_id"  value="<?=$c_obj->get('id') ?>">
                    <input type="hidden" name="comp_id" value="<?=$c_id ?>">

                    <label class="col-sm-1 col-sm-offset-1 control-label" for="site">Сайт</label>
                    <div class="col-sm-1">
                        <?= HtmlSnippets::select($bool_select, 'site',    'form-control', $c_obj->get('site')) ?>
                    </div>

                    <label class="col-sm-1 control-label" for="catalog">Каталог</label>
                    <div class="col-sm-1">
                        <?= HtmlSnippets::select($bool_select, 'catalog', 'form-control', $c_obj->get('catalog')) ?>
                    </div>

                    <label class="col-sm-1 control-label" for="price">Прайс</label>
                    <div class="col-sm-1">
                        <?= HtmlSnippets::select($bool_select, 'price',   'form-control', $c_obj->get('price')) ?>
                    </div>

                    <label class="col-sm-1 control-label" for="rate">Звездность</label>
                    <div class="col-sm-1">
                        <?= HtmlSnippets::select($rates,       'rate',    'form-control', $c_obj->get('rate')) ?>
                    </div>

                </div>
              </form>
            </div>
        <?php endforeach ?>
    </div>  
</div>
</div>
<div class="col-sm-2 col-sm-offset-5">
    <button class="btn btn-primary btn-block save-full-info">Сохранить</button>
</div>
