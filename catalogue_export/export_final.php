<?php
    include '../include/include.php';

    $region_id = 18;
    $region = Region::load($region_id);
    $region = $region->get('name');
    $filename = "$region".date("_d_m_h").".txt";

    $header = array();
    $lines = array();
    $strokes = array();

    $hotel_objs = array();

    $out_csv = '';

    $db = db::instance();

    $sql = "SELECT h.id FROM hotels h 
                LEFT JOIN catalogue_2017 c ON h.id = c.hotel_id 
                LEFT JOIN catalogue_contents cc ON h.id = cc.hotel_id
                WHERE c.block = $region_id AND cc.general = 1 AND cc.year = 2017
            ORDER BY c.order ASC";
    $db->query($sql);

    while($row = $db->fetch_row()) {
        $hotel_ids[] = $row['id'];
    }

    foreach ($hotel_ids as $id) {
        $hotel_objs[] = Hotel::load($id, 2017);
    }
    
    $i = 0; 
    $total_hotels = count($hotel_objs);
    foreach ($hotel_objs as $hotel) {
        $i++;
        $position = $i % 2 ? 'left' : 'right';

	echo $hotel->get('id') . "\n";
        $csv_arr = $hotel->getExportData($position, 2017);
        
        if ($csv_arr['exported']) {
            $for_export[] = $hotel->get('id');
        }

        if ($i <= 2) {
            $suffix = '-'.strtoupper($position);
            foreach($csv_arr as $name => $data) {
                $header[] = $name.$suffix;
            }
        }

        $strokes[] = iconv('UTF-8', 'CP1251//TRANSLIT', implode("\t", $csv_arr));
        if ($position == 'right' || $total_hotels == $i) { 
            $lines[] = implode("\t", $strokes);
            $strokes = array();
        }
    }

    $header = implode("\t", $header);
    $lines  = implode("\r\n", $lines);

    $out_csv = $header."\r\n".$lines;
    file_put_contents($filename, $out_csv);

    echo ('Export Done! Would you like to write changes to db (mark hotels as exported)? y/n:'.PHP_EOL);
    $stdin = fopen('php://stdin','r');
    $ans = trim(fgets($stdin));
    if ($ans == 'y') {
        $ids = implode(',',$for_export);
        $sql = "UPDATE catalogue_2017 SET exported = 1 WHERE hotel_id IN ($ids)";
        $db->query($sql);
    } else {
        echo('As you wish!'.PHP_EOL);
    }

    echo "($ids)";
