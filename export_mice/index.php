<?php
    include '../include/include.php';

$blocks = array(1=> "BKK");
    $db = db::instance();

    foreach ($blocks as $blockid => $blockname) {
        $hotel_ids = [];
        $block_lines = [];

        $db->query($sql = "SELECT cc.hotel_id FROM catalogue_contents cc
                    LEFT JOIN catalogue_2017 gen_cat ON cc.hotel_id = gen_cat.hotel_id
                    LEFT JOIN mice_2017 mc ON mc.hotel_id = cc.hotel_id
                    WHERE cc.mice = 1 AND cc.year = 2017 AND gen_cat.block = $blockid
                    ORDER BY mc.`order` ASC");

        while($row = $db->fetch_row())
        {
            $hotel_ids[] = $row['hotel_id'];
        }

        foreach ($hotel_ids as $hid) {
            $export = getExportData($hid, 2017);
            $block_lines[] = implode("\t", $export['data']);
        }

        $output = implode("\t", $export['title']);

        foreach ($block_lines as $line)
        {
            $output .= "\r\n".$line;
        }

        $output = iconv("UTF-8", 'UCS-2LE', $output);
        file_put_contents("MICE_$blockname"."_UCS-2LE.txt", $output);
 //       file_put_contents("MICE_$blockname"."_UTF.csv",  $output);
    }

    function getExportData($hid, $year)
    {
        $hotel = Hotel::load($hid);
        $hotel->loadCatalogueInfo($year);
        $general_data = $hotel->catalogue_info->getExportData('l');

        $hotel->loadMiceInfo($year);
        $mice_data = $hotel->mice_info->getExportData();

        unset($general_data['exported']);
        unset($general_data['table_mice']);
        unset($general_data['@icon_mice']);

        $region = Region::load($hotel->catalogue_info->get('block'));
        $output = array_merge($general_data, $mice_data);

        $region_id = $region->get('id');

        $output['name']  = $hotel->get('name');
        $output['block'] = $region->get('name');

        //Dirty Hack to save legacy csv order (in Indesign data fetched by column index, NOT COLUMN NAME)
        $ap_distance = $output['ap_distance'];
        unset($output['ap_distance']);
        $output['ap_distance'] = $ap_distance;


        $airport_map = array(
            1 =>  'BKK', 8 =>  'HKT', 13 => 'KBV', 18 => 'USM',19 => 'CNX', 20 => 'CEI'
        );

        $output['airport'] = $airport_map[$region_id];

        if (!$hotel->mice_info->get('finished'))
        {
            foreach ($output as $key => &$val) {
                if ($key != 'hotel_id') {
                    $val = '';
                }
            }
        } else {
            $hotel->mice_info->set('exported', 1);
            $hotel->mice_info->set('changed_after_export', 0);
            $hotel->mice_info->save();
            echo "$hid,";
        }

        return array('title' => array_keys($output), 'data' => array_values($output));
    }
