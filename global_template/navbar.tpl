<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/hotelsheet" class="navbar-brand">Marketing App</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse fade-in-loading" role="navigation">
      <ul class="nav navbar-nav">
      </ul>
    </nav>
  </div>
</header>
