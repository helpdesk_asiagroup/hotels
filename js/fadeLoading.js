function hideLoadedElements() 
{
    $('.fade-in-loading').each(function() {
        var props = {};
        if ($(this).css('position') != 'fixed') {
            props.position = 'relative';
        }
        props.top     = '20px';
        props.opacity = '0';
        $(this).css(props).data('animated', false);
    });
}

function showLoadedElements()
{
    var animation = {
        top:'0px',
        opacity: '1'
    };

    var switcher = true;

    $('.fade-in-loading').each(function() {
        if (!$(this).data('animated')) {
            $(this).data('animated', true).animate(animation, {duration: 500, easing: 'easeOutCubic', step: function(now, fx){
                if (switcher && fx.prop == 'opacity' && now > 0.7){
                    showLoadedElements();
                    switcher = false;
                }
            }});
            return false;
        }
    });
}

function fadeInLoadedElements() {
    hideLoadedElements();
    showLoadedElements();
}

$(function() {
    fadeInLoadedElements();
});
