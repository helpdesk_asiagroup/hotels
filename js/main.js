function showPagePreloader()
{

}

function hidePagePreloader()
{
    
}

function showPostPreviewPopup(elem)
{
    
}


function hideLoadedElements() 
{
    $('.fade-in-loading').each(function() {
        var props = {};
        if ($(this).css('position') != 'fixed') {
            props.position = 'relative';
        }
        props.top     = '20px';
        props.opacity = '0';
        $(this).css(props).data('animated', false);
    });
}

function showLoadedElements()
{
    var animation = {
        top:'0px',
        opacity: '1'
    };

    var switcher = true;

    $('.fade-in-loading').each(function() {
        if (!$(this).data('animated')) {
            $(this).data('animated', true).animate(animation, {duration: 500, easing: 'easeOutCubic', step: function(now, fx){
                if (switcher && fx.prop == 'opacity' && now > 0.7){
                    showLoadedElements();
                    switcher = false;
                }
            }});
            return false;
        }
    });
}

function fadeInLoadedElements() {
    hideLoadedElements();
    showLoadedElements();
}

$(function() {
    fadeInLoadedElements();
});

//Scroll interface for time picking (elements with .time-pick) its a plugin.
//Require jquery.mousewhell plugin
//On focusin bind handler for mousewheel, then, on focusout unbind it.
(function($) {
    $.fn.timePickScroll = function() {
        var timepick_units = this.find('.time-pick');

        timepick_units.each(function(){
            $(this).on('focusin', function () {
                $(this).mousewheel(function(scroll, delta) {
                    var current_val = parseInt($(this).val());
                    var min = 0;
                    var max = parseInt($(this).attr('data-max'));

                    if (delta > 0) { current_val++; }
                    else           { current_val--; }

                    current_val = current_val < min ? min : current_val;
                    current_val = current_val > max ? max : current_val;
                    
                    current_val = current_val < 10 ? "0"+String(current_val) : String(current_val);
                    
                    $(this).val(current_val);
                });
            });

            $(this).on('focusout', function () {
                $(this).unmousewheel();
            });
        });
        
        return this;
    }
})(jQuery);

