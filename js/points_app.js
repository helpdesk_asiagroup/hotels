$(function() {
    $('.btn-sm').click(function() { 
        $(this).addClass("btn-success")
            .removeClass("btn-default")
            .siblings()
            .removeClass("btn-success")
            .addClass("btn-default");
            var point_id = $(this).parents('.point').attr('data-point-id')
            var mode = $(this).children().attr('data-mode-id')
            setMode(point_id, mode)

    }) 

    check_date();

});

$( document ).ajaxSuccess(function() {
    $('.shouter').fadeOut()
});

$( document ).ajaxSend(function() {
    $('.shouter').fadeIn()
});


function check_date() {
    var now = parseInt(new Date().getTime()/1000)
    $('[data-point-id]').each(function () {
        var stat_div = $(this).children('.p2p_info')
        var last_update = stat_div.children('.last_online').attr('data-timestamp')
        if (now - last_update > 20) {
            stat_div.removeClass('on-line')
            stat_div.addClass('off-line')
        } else {
            stat_div.removeClass('off-line')
            stat_div.addClass('on-line')
        }
    })
}

function updateStats() {
    $.getJSON('ajax/update.php').done(function(answer_json) {
        $.each(answer_json, function(point_id, html) {
           selector = '[data-point-id = ' + point_id + ']'
           $(selector).children('.p2p_info').html(html)
        })
    check_date()
    })
}

function setMode(point, mode) {
    $.post(
        "ajax/set_mode.php", {
        id: point,
        mode: mode
        }, 
        function (data) {
            alert(data);
    });
}

window.setInterval(updateStats, 20000);
