<?php
echo 'Start export...';

include '../include/include.php';

$db = db::instance();
$fh = fopen("files2/hotels.csv", 'w');

$hotelsResult = $db->query("SELECT * FROM hotels");

while ($hotel = $db->fetch_row($hotelsResult)) {
	fputcsv($fh, [$hotel['id'] . '#' . $hotel['name']]);
}

flose($fh);

echo 'Done';