<?php

if (!empty($_POST)) {
	
	include '../include/include.php';
	
	$db = db::instance();
	
	echo '<pre>';
	echo $_POST['table'] . '<br>';
	if (!empty($_POST['data'])) {
		$data = explode(PHP_EOL, trim($_POST['data']));
		
		foreach ($data as $row) {
			$cols = explode($_POST['delimeter'], $row);
			
			print_r($cols);
			
			$updateData = [
				'image_1' => $cols[1],
				'image_2' => $cols[2],
				'image_3' => $cols[3],
				'image_4' => $cols[4],
				'image_5' => $cols[5],
				'image_6' => $cols[6],
				'image_2_txt' => $cols[7],
				'image_3_txt' => $cols[8],
				'image_4_txt' => $cols[9],
				'image_5_txt' => $cols[10],
				'image_6_txt' => $cols[11],
				//'map' => $cols[12],
				//'map_index' => str_replace(array("\r\n", "\n", "\r", "\t"), '', $cols[13]),
			];
			
			$db->update($updateData, 'catalogue_2018', 'hotel_id = ' . $cols[0]);
		}
	}
	echo '</pre>';
	
	echo '<br><br>Done';
	
} else {
	?>
	<form action="update_image.php" method="POST">
		<div>
		<select name="table">
			<option value=""></option>
			<option value="catalogue_2018">General</option>
			<option value="mice_2018">Mice</option>
			<option value="luxury_2018">Luxury</option>
		</select>
		<select name="delimeter">
			<option value=""></option>
			<option value=",">,</option>
			<option value=";">;</option>
		</select>
		</div>
		<div>
			<textarea name="data" rows="20" cols="100"></textarea>
		</div>
		<input type="submit" value="Update">
	</form>
	<?php
}