<?php 
$map_mice = array (
		'hotel_id' =>'hotel_id',
		
		//'basic_info' => 'base_info',
		//'table_rooms' => 'rooms',
		//'room_services' => 'room_services_left',
		//'site_services' => 'site_services_left',
		//'pool_head' => 'pool_top',
		//'pool_body' => 'pool_left',
		//'table_mice' => 'business_services',
		//'children_services' => 'children_services_left',
		//'table_restaurants' => 'restaurants',
		//'sports_ents' => 'sports_ents_left',
		//'beach_head' => 'beach_top',
		//'beach_body' => 'beach_left',
		//'cards' => 'cards',
		//'deposit' => 'deposit',
		//'comments_sayama' => 'comments_sayama',
		'map' => 'map',
		'map_index' => 'map_index',
		//'icon_ppl' => 'icon_ppl',
		//'icon_party' => 'icon_party',
		//'ap_distance' => 'ap_distance',
		//'mice_rooms' => 'mice_rooms',
		//'business_services' => 'business_services_left',
		//'check_in_check_out' =>'check_in',
		'img1' => 'image_1',
		'img2' => 'image_2',
		'img3' => 'image_3',
		'img4' => 'image_4',
		'img5' => 'image_5',
		'img6' => 'image_6',
		'img2_txt' => 'image_2_txt',
		'img3_txt' => 'image_3_txt',
		'img4_txt' => 'image_4_txt',
		'img5_txt' => 'image_5_txt',
		'img6_txt' => 'image_6_txt',
		'plan' => 'plan',
		//'vc1txt' => 'vc1txt',
		//'vc2txt' => 'vc2txt',
		//'vc3txt' => 'vc3txt',
);

$separetedFields = [
	'room_services_left',
	'site_services_left',
	'pool_left',
	'children_services_left',
	'business_services_left'
];

$icons = [
	'icon_ppl' => 18,
	'icon_party' => 17
];

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<pre>
<?php
if (!empty($_POST)) {
	
	include '../include/include.php';
	
	$db = db::instance();
	
	if (!empty($_POST['data'])) {
		$data = explode(PHP_EOL, trim($_POST['data']));
		
		$header = array_shift($data);
		$header = explode("\t", str_replace(["\n", "\r"], '', $header));
		
		print_r($header);
		
		foreach ($data as $row) {
			$cols = explode("\t", $row);
			
			print_r($cols);
			
			$update = [];
			
			$unknownFields = [];
			
			foreach ($cols as $colIndex => $colValue) {
				
				echo $header[$colIndex] . ' ';
				if (array_key_exists($header[$colIndex], $map_mice )) {
					$parceKey = $map_mice[$header[$colIndex]];
					if (in_array($parceKey, $separetedFields)) {
						$colValue = explode('#', $colValue);
						$valueArray = array_chunk($colValue, ceil(count($colValue) / 2) );
						$update[$parceKey] = implode('#', array_shift($valueArray));
						if (count($valueArray)) {
							$update[str_replace('_left' , '_right', $parceKey)] = implode('#', array_shift($valueArray));
						}
					} else if (array_key_exists($parceKey, $icons)) {
						$update[$parceKey] = $colValue ? $icons[$parceKey] : 99;
					} else if ($parceKey == 'check_in') {
						$colValue = explode('#', $colValue);
					}else {
						$update[$parceKey] = $colValue;
					}
				} else {
					$unknownFields[] = $header[$colIndex];
				}
			}
			print_r($update);
			
			print_r($unknownFields);
			
			// update
			$db->update($update, 'mice_2018', 'hotel_id = ' . $update['hotel_id']);
			// where hotel_id = $update['hotel_id'] AND finished = 0
		}
		
	}
} else {
?>
	<form action="update_mice.php" method="post">
	<textarea name="data"></textarea>
	<br>
	<input type="submit" value="Обновить">
</form>
<?php } ?>
</pre>
</body>
</html>