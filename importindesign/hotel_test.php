<?php

include '../include/include.php';

$db = db::instance();

$region_id = 26;

if (($fh = fopen("hotels_test.csv", 'r')) !== False) {
	while ($hotel = fgetcsv($fh, 0, ';')) {
		$h['name'] = iconv('WINDOWS-1251', 'UTF-8', trim($hotel[0]));
		$id = $db->insert($h, 'hotels');
		
		echo $id . ' ' .  $h['name'] . '<br>';
	}
} else {
	echo "File dont open<br>\n";
}

echo '<br>Done';