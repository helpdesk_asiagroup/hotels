<?php

include '../include/include.php';

$db = db::instance();

$rows = $db->query_rows("SELECT * FROM catalogue_2018", 'hotel_id');

foreach ($rows as $hotelId => $row) {
	echo $hotelId . '<br>';

	foreach($row as $fieldName => $val) {
		$row[$fieldName] = trim($val, " \t\n\r#");
		
		if (in_array($fieldName, getFilterFields())) {
			$row[$fieldName] = filterField($row[$fieldName]);
		}
		
		if (in_array($fieldName, getFilterFieldsArray())) {
			$row[$fieldName] = filterArrayField($row[$fieldName]);
		}
	}
	
	unset($row['id']);
	
	$db->update($row, 'catalogue_2018', 'hotel_id = ' . $hotelId);
}

function getFilterFieldsArray()
{
	return [
		'bussiness_services',
		'rooms',
		'restaurants'
	];
}

function filterArrayField($value)
{
	$value = str_replace(["\n", "\t"], '', $value);

	$result = [];

	$tmp = explode('#', $value);

	foreach ($tmp as $row) {
		$rowTmp = explode(';', $row);
		$rowResult = [];
		foreach ($rowTmp as $col) {
			if (trim($col) != '') {
				$rowResult[] = trim($col);
			}
		}

		if (!empty($rowResult)) {
			$result[] = implode(';', $rowResult);
		}

	}

	return implode('#', $result);
}

function getFilterFields() {
	return [
		'room_services_left',
		'room_services_right',
		'site_services_left',
		'site_services_right',
		'pool_left',
		'pool_right',
		'children_services_left',
		'children_services_right',
		'sports_ents_left',
		'sports_ents_right',
		'beach_left',
		'beach_right',
	];
}

function filterField($value)
{
	$result = [];

	$tmp = explode('#', $value);
	foreach ($tmp as $item) {
		if (trim($item) != '') {
			$result[] = trim($item);
		}
	}

	return implode('#', $result);
}