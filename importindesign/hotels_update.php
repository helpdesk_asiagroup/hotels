<?php

include '../include/include.php';

$db = db::instance();

if (($fh = fopen("hotels_update_luxury.csv", 'r')) !== False) {
	while ($hotel = fgetcsv($fh, 0, ';')) {
		$hotelId = (int)trim($hotel[2]);
		$hotelName = addslashes(trim($hotel[1]));
		$hotelOrder = (int)trim($hotel[0]);
		
		echo $hotelOrder . ' ' . $hotelName . ' ' . $hotelId . "<br>";
		
		$db->update(['name' => $hotelName], 'hotels', 'id = ' . $hotelId);
		
		$db->update(['order' => $hotelOrder], 'luxury_2018', 'hotel_id = ' . $hotelId);
	}
} else {
	echo "File dont open<br>\n";
}

echo '<br>Done';