<?php 
$map_general = array (
	'hotel_id-LEFT' => 'hotel_id',
	'web_site-LEFT' => 'site',
	'basic_info-LEFT' => 'base_info',
	'table_rooms-LEFT' => 'rooms',
	'room_services-LEFT' => 'room_services_left',
	'site_services-LEFT' => 'site_services_left',
	'pool_head-LEFT' => 'pool_top',
	'pool_body-LEFT' => 'pool_left',
	'children_services-LEFT' => 'children_services_left',
	'table_restaurants-LEFT' => 'restaurants',
	'sports_ents-LEFT' => 'sports_ents_left',
	'beach_head-LEFT' => 'beach_top',
	'beach_body-LEFT' => 'beach_left',
	'cards-LEFT' => 'cards',
	'deposit-LEFT' => 'deposit',
	'ceo-LEFT' => 'ceo',
	'comments_sayama-LEFT' => 'comments_sayama',
	'check_in-LEFT' => 'check_in',
	'check_out-LEFT' => 'check_out',
	'icon_location_text-LEFT' => 'icon_1_text',
	'icon_build_at_text-LEFT' => 'icon_3_text',
	'icon_refresh_text-LEFT' => 'icon_4_text',
);

$separetedFields = [
	'room_services_left',
	'site_services_left',
	'pool_left',
	'children_services_left',
	'sports_ents_left',
	'beach_left',
	//'business_services_left'
];

$fillZero = [
	'check_in',
	'check_out'
];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<pre>
<?php 
if (!empty($_POST)) {
	
	include '../include/include.php';
	
	$db = db::instance();
	
	if (!empty($_POST['data'])) {
		$data = explode(PHP_EOL, trim($_POST['data']));
		
		$header = array_shift($data);
		$header = explode("\t", str_replace(["\n", "\r"], '', $header));
		
		print_r($header);
		
		foreach ($data as $row) {
			$cols = explode("\t", $row);
			
			/*
			$tmp = [];
			foreach ($cols as $colIndex => $colValue) {
				$tmp[$header[$colIndex]] = $colValue;
			}
			print_r($tmp);*/
			
			$unknownFields = [];
			$update = [];
			
			foreach ($cols as $colIndex => $colValue) {
				$colValue = trim($colValue);
				
				if (array_key_exists($header[$colIndex], $map_general )) {
					$parceKey = $map_general[$header[$colIndex]];
					if (in_array($parceKey, $separetedFields)) {
						$colValue = explode('#', $colValue);
						$valueArray = array_chunk($colValue, ceil(count($colValue) / 2) );
						$update[$parceKey] = implode('#', array_shift($valueArray));
						if (count($valueArray)) {
							$update[str_replace('_left' , '_right', $parceKey)] = implode('#', array_shift($valueArray));
						} else {
							$update[str_replace('_left' , '_right', $parceKey)] = '';
						}
					} elseif (in_array($parceKey, $fillZero)) {
						$colValue = str_pad($colValue, 5, '0', STR_PAD_RIGHT);
						$update[$parceKey] = $colValue;
					} else {
						$update[$parceKey] = $colValue;
					}
				} else {
					$unknownFields[] = $header[$colIndex];
				}
			}
			
			print_r($update);
			
			print_r($unknownFields);
			
			if ($update['hotel_id'] != '!empty!') {
				$db->update($update, 'catalogue_2018', 'hotel_id = ' . $update['hotel_id'] . ' AND finished = 0');
			}
		}
		
	}
} else {
?>
	<form action="update_general.php" method="post">
		<textarea name="data"></textarea>
		<br>
		<input type="submit" value="Обновить">
	</form>
<?php } ?>
</pre>
</body>
</html>