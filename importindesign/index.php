<?php
    include '../include/include.php';

    $map_catalogue = array (
        'hotel_id' =>'hotel_id',
        'block' => 'block',
        'area' => 'area',
        'web_site' => 'site',
        'basic_info' => 'base_info',
        'table_rooms' => 'rooms',
        'room_services' => 'room_services_left',
        'site_services' => 'site_services_left',
        'pool_head' => 'pool_top',
        'pool_body' => 'pool_left',
        'table_mice' => 'business_services',
        'children_services' => 'children_services_left',
        'table_restaurants' => 'restaurants',
        'sports_ents' => 'sports_ents_left',
        'beach_head' => 'beach_top',
        'beach_body' => 'beach_left',
        'cards' => 'cards',
        'deposit' => 'deposit',
        'ceo' => 'ceo',
        'comments_sayama' => 'comments_sayama',
        'icon_video' =>'icon_video',
        'icon_rate' =>'rate',
        'icon_check' => 'icon_0',
        'icon_location' => 'icon_1',
        'icon_center' => 'icon_2',
        'icon_build_at' => 'icon_3',
        'icon_refresh' => 'icon_4',
        'icon_vip' => 'icon_5',
        'icon_lowcost' => 'icon_6',
        'icon_mice' => 'icon_7',
        'icon_honey' => 'icon_8',
        'icon_family' => 'icon_9',
        'icon_active' => 'icon_10',
        'icon_relax' => 'icon_11',
        'icon_location_text' => 'icon_1_text',
        'icon_build_at_text' => 'icon_3_text',
        'icon_refresh_text' => 'icon_4_text',
        'check_in' =>'check_in',
        'check_out' =>'check_out',
        'order' => 'order'
    );

    $db = db::instance();
    $db->query("SELECT * FROM regions");
    while ($row = $db->fetch_row()) {
        $regions[$row['name']] = $row['id'];
    }

    $files = array_diff(scandir('files/onemore'), array('..', '.'));

    $order = 1;
foreach ($files as $filename) {
    $rn = trim(preg_replace(['/\d\d\sHOTEL\sBLOCK\s/', '/\.\w{3}\.\w{3}/', '/\dS/'], ['','', ''], $filename));

    $filepath = "files/onemore/$filename";
    $year = "2018";
    $block_id = $regions[$rn];
    $delimiter = "\t";

    $fh = fopen($filepath, 'r');
    $header = fgetcsv($fh, 0, $delimiter);

    $parsed = array();

    $db->query("SELECT * FROM sub_regions WHERE parent_region = $block_id");
    while ($row = $db->fetch_row()) {
        $areas[$row['id']] = $row['name'];
    }

    while ($hotel = fgetcsv($fh, 0, $delimiter)) {
    	// перекодируем
    	foreach ($hotel as $key => $value) {
    		$hotel[$key] = iconv('WINDOWS-1251', 'UTF-8', $value);
    	}
        foreach($hotel as $i => $data) {
            $key = trim(str_replace('-LEFT', '', $header[$i]), ' @');

            switch ($key) {
                case 'area':
                    $formatted_data = array_search(strtoupper(trim($data)), $areas);
                    break;

                case 'icon_video':
                case 'icon_location':
                case 'icon_location':
                case 'icon_center':
                case 'icon_build_at':
                case 'icon_refresh':
                case 'icon_vip':
                case 'icon_lowcost':
                case 'icon_mice':
                case 'icon_honey':
                case 'icon_family':
                case 'icon_active':
                case 'icon_relax':
                	$tmp = explode("\\", $data);
                    $formatted_data = $data ? (int) end($tmp/*explode("\\", $data)*/) : '99';
                    break;

                case 'icon_rate':
                	$tmp = explode("\\", $data);
                    $rate = end($tmp/*explode("\\", $data)*/);
                    $rate_parsed = $rate[0];
                    $rate_parsed .=  $rate[1] == '+' ? 's' : '';
                    $formatted_data = $rate_parsed;
                    break;

                case 'icon_check':
                    $formatted_data = '1';
                    break;

                case 'icon_location_text':
                    $formatted_data = (int) $data ? $data . 'м' : '';
                    break;

                case 'icon_build_at_text':
                case 'icon_refresh_text':
                    $formatted_data = (int) $data ? $data : '';
                    break;

                default:
                    $formatted_data = $data;
                    break;
            }

            $formatted[$key] = $formatted_data;
        }
        if(isset($parsed[$formatted['hotel_id']]) || !$formatted['hotel_id']) {
            $formatted['hotel_id'] = readline("FAIL WITH ID ".$formatted['hotel_id'] . " " . $formatted['name']);
        }
        $formatted['order'] = $order++;
        $formatted['block'] = $block_id;

        $parsed[$formatted['hotel_id']] = $formatted;
    }

    foreach($parsed as $hid => $hotel) {
        $forHotels['id']   = $hid;
        $forHotels['name'] = $hotel['name'];

        $forContents['hotel_id'] = $hid;
        $forContents['year']     = $year;
        $forContents['general']  = 1;
        $forContents['mice']     = $hotel['icon_mice'] == '99' ? 0 : 1;
        $forContents['luxury']   = $hotel['icon_vip']  == '99' ? 0 : 1;

        foreach ($map_catalogue as $parsedKey => $sqlKey)
        {
            switch ($sqlKey) {
                case 'room_services_left':
                case 'site_services_left':
                case 'pool_left':
                case 'children_services_left':
                case 'sports_ents_left':
                case 'beach_left':
                    $dataArr = explode('#', $hotel[$parsedKey]);
                    $count = count($dataArr);
                    if ($count  > 1) {
                        echo "COUNT $parsedKey : $count \n";
                        $sides = array_chunk($dataArr, (int) ($count/2)+1);
                        $forCatalogue[$sqlKey] = implode('#', $sides[0]);
                        if(isset($sides[1])) {
                            $forCatalogue[str_replace('_left', '_right', $sqlKey)] = implode('#', $sides[1]);
                        }
                        if(isset($sides[2][0])) {print_r($sides[2]); die ("SUKA");}
                        break;
                    }

                default:
                    $forCatalogue[$sqlKey] = $hotel[$parsedKey];
                    break;
            }
        }

        echo "SQLING {$hotel['name']} | {$hotel['area']} \n";
        //$db->replace($forHotels, 'hotels');
        //print_r($forHotels);
        //$db->replace($forContents, 'catalogue_contents');
        //print_r($forContents);
        $db->replace($forCatalogue, "catalogue_$year");
        //print_r($forCatalogue);

    }

}
