<?php

    include '../include/include.php';

    $db = db::instance();
    $db->query("SELECT * FROM regions");
    while ($row = $db->fetch_row()) {
        $regions[$row['name']] = $row['id'];
    }

    $fh = fopen('n/final_3.csv', 'r');

    while ($csv = fgetcsv($fh, 0, '#')) {
        $order = $csv[0];
        $name  = $csv[1];
        $rate  = $csv[2];
        $id    = (int) $csv[3];
        $block_id = $regions[$csv[4]];

        if (!$id) {
            $db->query("SELECT * FROM hotels WHERE name = '$name'");
            if ($row = $db->fetch_row()) {
                $id = $row['id'];
            } else {
                if ($rate) {
                    $id = $db->insert(array('name' => $name), 'hotels');
                } else {
                    $id = $db->insert(array('name' => $name . " " . $csv[4]), 'hotels');
                }
            }
        }

        echo "$order $name $rate $id {$csv[4]} \n";

        $cat = Catalogue2017::load($id, 2017);
        $cat->set('order', $order);
        $cat->set('rate', $rate);
        $cat->set('block', $block_id);
        $cat->save();

        $hot = Hotel::load($id);
        $hot->set('name', $name);
        $hot->save();

        $db->query("SELECT * FROM catalogue_contents WHERE hotel_id = $id AND year = 2017");
        if ($row = $db->fetch_row()) {
            if (!$row['general']) {
                $db->query("UPDATE catalogue_contents SET general = 1 WHERE hotel_id = $id AND year = 2017");
            }
        } else {
            $db->replace(array('hotel_id' => $id, 'year' => '2017', 'general' => 1), 'catalogue_contents');
        }
    }
