<?php
	
$map_luxury = [
	'hotel_id' =>'hotel_id',
	'basic_info' => 'base_info',
	'sign_info' => 'signature_info',
	'site_services' => 'site_services_left',
	'spa_info' => 'spa_info',
	'children_services' => 'children_services_left',
	'table_restaurants' => 'restaurants',
	'pool_head' => 'pool_top',
	'pool_body' => 'pool_left',
	'sports_ents' => 'sports_ents_left',
	'beach_head' => 'beach_top',
	'beach_body' => 'beach_left',
	'cards' => 'cards',
	'deposit' => 'deposit',
	
	'comments_sayama' => 'comments_sayama',
	'table_rooms' => 'rooms_table',
	'room_descr_title_1' => 'room_descr_title_1',
	'room_descr_1' => 'room_descr_1',
	'room_descr_title_2' => 'room_descr_title_2',
	'room_descr_2' => 'room_descr_2',
	'room_descr_title_3' => 'room_descr_title_3',
	'room_descr_3' => 'room_descr_3',
	'room_descr_title_4' => 'room_descr_title_4',
	'room_descr_4' => 'room_descr_4',
	'room_descr_title_5' => 'room_descr_title_5',
	'room_descr_5' => 'room_descr_5',
	'room_descr_title_6' => 'room_descr_title_6',
	'room_descr_6' => 'room_descr_6',
	'room_descr_title_7' => 'room_descr_title_7',
	'room_descr_7' => 'room_descr_7',
	'room_descr_title_8' => 'room_descr_title_8',
	'room_descr_8' => 'room_descr_8',
	'room_descr_title_9' => 'room_descr_title_9',
	'room_descr_9' => 'room_descr_9',
	'room_descr_title_10' => 'room_descr_title_10',
	'room_descr_10' => 'room_descr_10',
	'room_descr_title_11' => 'room_descr_title_11',
	'room_descr_11' => 'room_descr_11',
	'room_descr_title_12' => 'room_descr_title_12',
	'room_descr_12' => 'room_descr_12',
	'room_descr_title_13' => 'room_descr_title_13',
	'room_descr_13' => 'room_descr_13',
	'room_descr_title_14' => 'room_descr_title_14',
	'room_descr_14' => 'room_descr_14',
	'ap_distance' => 'ap_distance',
		
	'map' => 'map',
	'map_index' => 'map_index',
	'h_logo' => 'h_logo',
	'qr' => 'qr',
	'img1' => 'img_1',
	'img2' => 'img_2',
	'img3' => 'img_3',
	'img4' => 'img_4',
	'img5' => 'img_5',
	'img6' => 'img_6',
	'img7' => 'img_7',
	'img8' => 'img_8',
	'img9' => 'img_9',
	'img10' => 'img_10',
	'img11' => 'img_11',
	'img12' => 'img_12',
	'img13' => 'img_13',
	'img14' => 'img_14',
	'img15' => 'img_15',
	'img2_txt' => 'img_2_txt',
	'img3_txt' => 'img_3_txt',
	'img4_txt' => 'img_4_txt',
	'img5_txt' => 'img_5_txt',
	'img6_txt' => 'img_6_txt',
	'img7_txt' => 'img_7_txt',
	'img8_txt' => 'img_8_txt',
	'img9_txt' => 'img_9_txt',
	'img10_txt' => 'img_10_txt',
	'img11_txt' => 'img_11_txt',
	'img12_txt' => 'img_12_txt',
	'img13_txt' => 'img_13_txt',
	'img14_txt' => 'img_14_txt',
	'img15_txt' => 'img_15_txt',
	
];
	
$separetedFields = [
	'site_services_left',
	'children_services_left',
	'pool_left',
	'sports_ents_left',
	'beach_left',
];

$jsonPack = [
	'rooms_table'
];

$imageFields = [
	'map',
	'map_index',
	'h_logo',
	'qr',
	'img_1',
	'img_2',
	'img_3',
	'img_4',
	'img_5',
	'img_6',
	'img_7',
	'img_8',
	'img_9',
	'img_10',
	'img_11',
	'img_12',
	'img_13',
	'img_14',
	'img_15',
	'img_2_txt',
	'img_3_txt',
	'img_4_txt',
	'img_5_txt',
	'img_6_txt',
	'img_7_txt',
	'img_8_txt',
	'img_9_txt',
	'img_10_txt',
	'img_11_txt',
	'img_12_txt',
	'img_13_txt',
	'img_14_txt',
	'img_15_txt',
];

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<pre>
<?php
if (!empty($_POST)) {
	
	include '../include/include.php';
	
	$db = db::instance();
	
	$data = explode(PHP_EOL, trim($_POST['data']));
	
	$header = array_shift($data);
	$header = explode("\t", str_replace(["\n", "\r"], '', $header));
	
	print_r($header);
	
	foreach ($data as $row) {
		
		$cols = explode("\t", str_replace(["\n", "\r"], '', $row));
		
		$update = [];
			
		$unknownFields = [];
		
		$imageUpdate = [];
		
		$roomColCount = 0;
				
		foreach ($cols as $colIndex => $colValue) {
			if (array_key_exists($header[$colIndex], $map_luxury )) {
				$parceKey = $map_luxury[$header[$colIndex]];
				$colValue = str_replace("\"", '', $colValue);
				if (in_array($parceKey, $imageFields)) {
					$imageUpdate[$parceKey] = $colValue;
				} elseif (in_array($parceKey, $separetedFields)) {
					$colValue = explode('#', $colValue);
					$valueArray = array_chunk($colValue, ceil(count($colValue) / 2) );
					$update[$parceKey] = implode('#', array_shift($valueArray));
					if (count($valueArray)) {
						$update[str_replace('_left' , '_right', $parceKey)] = implode('#', array_shift($valueArray));
					} else {
						$update[str_replace('_left' , '_right', $parceKey)] = '';
					}
				} elseif (in_array($parceKey, $jsonPack)) {
					$array = [];
					$last_row = 1;
					$jsonRows = explode('#', $colValue);
					foreach ($jsonRows as $jsonRow) {
						$tmp = explode(';', $jsonRow);
						if (count($tmp) > 1) {
							//array_unshift($tmp, '');
							$roomColCount = count($tmp);
							$array[] = $tmp;
						} else {
							$update['room_table_last_row_' . $last_row] = array_shift($tmp);
							$last_row++;
						}
					}
					
					$update[$parceKey] = json_encode($array, JSON_UNESCAPED_UNICODE);
				} else {
					$update[$parceKey] = $colValue;
				}
			} else {
				$unknownFields[] = $header[$colIndex];
			}
		}
		
		$imageUpdate['hotel_id'] = $update['hotel_id'];
		print_r($update);
		print_r($imageUpdate);
		
		$db->update($update, 'luxury_2018', 'hotel_id = ' . $update['hotel_id'] . ' AND finished = 0');
		
		$db->update($imageUpdate, 'luxury_2018', 'hotel_id = ' . $imageUpdate['hotel_id']);
	}
	
	echo 'Done';
	
} else {
	?>
	<form action="update_luxury.php" method="post">
	<textarea name="data"></textarea>
	<br>
	<input type="submit" value="Обновить">
</form>
<?php } ?>
</pre>
</body>
</html>
